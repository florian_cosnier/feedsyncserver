package com.thales.feedsyncserver.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.abdera.ext.geo.GeoHelper;
import org.apache.abdera.ext.geo.Point;
import org.apache.abdera.ext.geo.Position;
import org.apache.abdera.model.Entry;
import org.apache.commons.httpclient.HttpException;
import org.tmatesoft.sqljet.core.SqlJetException;

import sun.misc.BASE64Decoder;

import com.thales.feedsyncserver.abdera.AbderaSupport;
import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.db.DBEntry;
import com.thales.feedsyncserver.geo.GeoFilter;

/**
 * This class represents the object Entry, it is implemented to manipulate
 * easily the ATOM entry
 * 
 * @author Florian COSNIER
 *
 */
public class EntryResource {
	private DB mDb;
	public InetAddress IP;
	public String httpAddress;
	public GeoFilter mGeoFilter;

	/**
	 * <p>
	 * Constructor </br>
	 * Initialize the IP address of the server and the database
	 * </p>
	 * 
	 * @throws SqlJetException
	 */
	public EntryResource() throws SqlJetException {
		this.mDb = new DB();
		this.mGeoFilter = new GeoFilter();

		this.IP = null;
		try {
			this.IP = InetAddress.getLocalHost();
			this.httpAddress = ("http://" + this.IP.getHostAddress() + ":8083");
		
			Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                    NetworkInterface ni = eni.nextElement();
                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                    while(inetAddresses.hasMoreElements()) {
                            InetAddress ia = inetAddresses.nextElement();
                            if(!ia.isLinkLocalAddress()) {
                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
                                    if(ni.getName().contains("wlan")){
                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
                                    }
                            }
                    }
            }
		
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * This function gets the entry from the database with the id (entryId) </br>
	 * It opens the db, uses a cursor to get the entry if the entryId matches
	 * </p>
	 * 
	 * @param feedId id of the feed where to get the entry
	 * @param entryId id of the entry to get
	 * @return Entry 
	 */
	public Entry getEntry(String feedId, String entryId) {
		Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		DBEntry mDbEntry = new DBEntry();
		try {
			mDbEntry = this.mDb.getDBEntry(Long.parseLong(entryId));
			if ((mDbEntry != null) && (mDbEntry.getFeed_id().equals(feedId))) {
				mEntry.setId(String.valueOf(mDbEntry.getId()));
				mEntry.setTitle(mDbEntry.getTitle());
				mEntry.setContent(mDbEntry.getContent());
				mEntry.setPublished(mDbEntry.getCreated());
				mEntry.setUpdated(new Date(mDbEntry.getUpdated().toGMTString()));
				if (mDbEntry.getAuthor() != null) {
					mEntry.addAuthor(mDbEntry.getAuthor());
				}
				mEntry.setSummaryAsHtml(mDbEntry.getSummary());
				if (!String.valueOf(mDbEntry.getLatitude()).isEmpty()) {
					GeoHelper.addPosition(mEntry,new Point(mDbEntry.getLatitude(), mDbEntry.getLongitude()));
				}
				mEntry.addLink(mDbEntry.getUrl(), "self","application/atom+xml", null, null, 0);
				mEntry.addLink(mDbEntry.getUrl(), "edit","application/atom+xml", null, null, 0);
				mEntry.addLink(mDbEntry.getUrl() + "/comments", "replies","application/atom+xml", null, null, 0);
			}
		} catch (SqlJetException e) {
			System.out.println("ERROR executing get entries");
			e.printStackTrace();
			return null;
		}
		return mEntry;
	}

	/**
	 * <p>
	 * This function does the same thing as the method getEntry but it returns a DBEntry
	 * </p>
	 * @param entryId id of the entry to get
	 * @return DBEntry
	 */
	public DBEntry getDBEntry(String entryId) {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		DBEntry mDbEntry = new DBEntry();
		try {
			mDbEntry = this.mDb.getDBEntry(Long.parseLong(entryId));
		} catch (SqlJetException e) {
			System.out.println("ERROR executing get entries");
			e.printStackTrace();
			return null;
		}
		return mDbEntry;
	}

	/**
	 * <p>
	 * This function adds an entry in the database </br>
	 * It opens the db and adds the entry then returns the id of the new entry </br>
	 * Once the entry is well added, the function returns the entry updated with the id and the link of the location of the entry
	 * </p>
	 * 
	 * @param feedId id of the feed where the entry will be added
	 * @param mEntry entry to add
	 * @return Entry entry updated once it is added in the database
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 */
	public Entry postEntry(String feedId, Entry mEntry)
			throws URISyntaxException, SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		long entryId = 0;

		DBEntry mDbEntry = new DBEntry();
		if(mEntry.getTitle()!=null){
			mDbEntry.setTitle(mEntry.getTitle());
		}else{
			mDbEntry.setTitle("");
		}
		if(mEntry.getContent()!=null){
			mDbEntry.setContent(mEntry.getContent());
		}else{
			mDbEntry.setContent("");
		}
		if(mEntry.getSummary()!=null){
			mDbEntry.setSummary(mEntry.getSummary());
		}else{
			mDbEntry.setSummary("");
		}
		
		mDbEntry.setFeed_id(feedId);
		if (mEntry.getUpdated() == null) {
			mDbEntry.setUpdated(new Date());
		} else {
			mDbEntry.setUpdated(new Date());
		}
		if (GeoHelper.getPositions(mEntry) != null) {
			Position[] position = GeoHelper.getPositions(mEntry);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		
		if (feedId.equals("3")) {
			List<DBEntry> mGeoUsers = this.mDb.getDBGeoUsers("4");
			String mUsers = this.mGeoFilter.checkUsersPositionsList(mGeoUsers,mDbEntry);
			Entry mNewEntry = AbderaSupport.getAbdera().newEntry();
			mNewEntry.addAuthor(mUsers);
			mDbEntry.setAuthor(mNewEntry.getAuthor());
		} else {
			if(mEntry.getAuthor()!=null){
				mDbEntry.setAuthor(mEntry.getAuthor());
			}else{
				mDbEntry.setAuthor(AbderaSupport.getAbdera().newEntry().addAuthor(""));
			}
			
		}
		
		try {
			entryId = this.mDb.addItemEntry(mDbEntry);
			this.mDb.updateEntryURL(entryId);
		} catch (SqlJetException e) {
			System.out.println("Error add entry in the database, SQL Busy");
			e.printStackTrace();
			return null;
		}
		mDbEntry = this.mDb.getDBEntry(entryId);

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		newEntry = mEntry;
		newEntry.setId(String.valueOf(entryId));
		newEntry.addLink(mDbEntry.getUrl(), "self", "application/atom+xml",null, null, 0);
		newEntry.addLink(mDbEntry.getUrl() + "/editEntry", "edit","application/atom+xml", null, null, 0);
		newEntry.addLink(mDbEntry.getUrl() + "/comments", "replies","application/atom+xml", null, null, 0);

		return newEntry;
	}
	/**
	 * <p>
	 * This function adds a media entry in the database </br>
	 * It opens the db and adds the entry then returns the id of the new entry </br>
	 * Once the entry is well added, the function returns the comment updated with the id, a link of the location of the new entry and a link to the media
	 * </p>
	 * 
	 * @param feedId id of the feed where the media entry will be added
	 * @param mMediaEntry media entry to add
	 * @return Entry comment updated once it is added in the database
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 */
	public Entry postMediaEntry(String feedId, Entry mMediaEntry)
			throws URISyntaxException, SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		long commentId = 0;

		DBEntry mDbEntry = new DBEntry();
		
		if(mMediaEntry.getTitle()!=null){
			mDbEntry.setTitle(mMediaEntry.getTitle());
		}else{
			mDbEntry.setTitle("");
		}
		if(mMediaEntry.getContent()!=null){
			mDbEntry.setContent(mMediaEntry.getContent());
		}else{
			mDbEntry.setContent("");
		}
		if(mMediaEntry.getSummary()!=null){
			mDbEntry.setSummary(mMediaEntry.getSummary());
		}else{
			mDbEntry.setSummary("");
		}
		if(mMediaEntry.getAuthor()!=null){
			mDbEntry.setAuthor(mMediaEntry.getAuthor());
		}else{
			mDbEntry.setAuthor(AbderaSupport.getAbdera().newEntry().addAuthor(""));
		}
		

		mDbEntry.setFeed_id(feedId);
		mDbEntry.setUpdated(new Date());
		if(mMediaEntry.getCategories()!=null){
			if(mMediaEntry.getCategories().get(0)!=null){
				mDbEntry.set_id(mMediaEntry.getCategories().get(0).getText().toString());
			}else{
				mDbEntry.set_id(new Date().toGMTString());
			}
		}else{
			mDbEntry.set_id(new Date().toGMTString());
		}
		
		BufferedImage image = null;

		System.out.println("item.content: " + mMediaEntry.getContent());
		if (!mMediaEntry.getContent().isEmpty()) {
			try {
				BASE64Decoder decoder = new BASE64Decoder();
				byte[] imageByte = decoder.decodeBuffer(mMediaEntry.getContent());
				ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
				image = ImageIO.read(bis);
				bis.close();
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		mDbEntry.setContent(this.httpAddress + "/feedsync/media/test.jpg");
		if (GeoHelper.getPositions(mMediaEntry) != null) {
			Position[] position = GeoHelper.getPositions(mMediaEntry);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		try {
			commentId = this.mDb.addItemEntry(mDbEntry);
			this.mDb.updateEntryURL(commentId);
		} catch (SqlJetException e) {
			System.out.println("Error add entry in the database, SQL Busy");
			e.printStackTrace();
			return null;
		}
		File outputfile = new File("webapps/feedsync/media/" + commentId+ ".jpg");

		System.out.println("output 1: " + outputfile.getAbsolutePath());
		if (image != null) {
			if (outputfile != null) {
				System.out.println("output 2: " + outputfile.getAbsolutePath());
				try {
					ImageIO.write(image, "jpg", outputfile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("output 3: " + outputfile.getAbsolutePath());
			}
		} else {
			System.out.println("error");
		}
		mDbEntry.setContent("<img src=\"" + this.httpAddress+ "/feedsync/media/" + commentId + ".jpg\">");
		mMediaEntry.setContent(this.httpAddress + "/feedsync/media/"+ commentId + ".jpg");
		this.mDb.updateContentEntry(mDbEntry);

		mDbEntry = this.mDb.getDBEntry(commentId);

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		newEntry = mMediaEntry;
		newEntry.setId(String.valueOf(commentId));
		newEntry.addLink(mDbEntry.getUrl(), "self", "application/atom+xml",null, null, 0);
		newEntry.addLink(mDbEntry.getUrl() + "/editEntry", "edit","application/atom+xml", null, null, 0);

		return newEntry;
	}

	/**
	 * <p>
	 * This function updates an entry in the database </br>
	 * It opens the db and edits the entry with the id "entryId" </br>
	 * Once the entry is well edited, the function returns a boolean to notify if the edition is done </br>
	 * </p>
	 * 
	 * @param feedId id of the feed 
	 * @param entryId if of the entry to edit
	 * @param mEntry entry edited
	 * @return Boolean true well edited, false not edited
	 */
	public Boolean putEntry(String feedId, String entryId, Entry mEntry) {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return Boolean.valueOf(false);
			}
		}
		DBEntry mDbTempEntry = null;
		try {
			mDbTempEntry = this.mDb.getDBEntry(Long.parseLong(entryId));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SqlJetException e1) {
			e1.printStackTrace();
		}
		DBEntry mDbEntry = new DBEntry();
		mDbEntry.setId(Long.parseLong(entryId));
		if (mEntry.getTitle() != null) {
			if (!mEntry.getTitle().isEmpty()) {
				mDbEntry.setTitle(mEntry.getTitle());
			} else {
				mDbEntry.setTitle(mDbTempEntry.getTitle());
			}
		} else {
			mDbEntry.setTitle(mDbTempEntry.getTitle());
		}
		if (mEntry.getAuthor() != null) {
			mDbEntry.setAuthor(mEntry.getAuthor());
		} else {
			mDbEntry.setAuthor(mDbTempEntry.getAuthor());
		}
		if (mEntry.getContent() != null) {
			if (!mEntry.getContent().isEmpty()) {
				mDbEntry.setContent(mEntry.getContent());
			} else {
				mDbEntry.setContent(mDbTempEntry.getContent());
			}
		} else {
			mDbEntry.setContent(mDbTempEntry.getContent());
		}
		if (mEntry.getSummary() != null) {
			if (!mEntry.getSummary().isEmpty()) {
				mDbEntry.setSummary(mEntry.getSummary());
			} else {
				mDbEntry.setSummary(mDbTempEntry.getSummary());
			}
		} else {
			mDbEntry.setSummary(mDbTempEntry.getSummary());
		}
		mDbEntry.setFeed_id(feedId);
		mDbEntry.setUpdated(new Date());
		if (GeoHelper.getPositions(mEntry) != null) {
			Position[] position = GeoHelper.getPositions(mEntry);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		
		try {
			this.mDb.updateEntry(mDbEntry);
		} catch (SqlJetException e) {
			System.out.println("ERROR edition entry");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * This function deletes an entry in the database </br>
	 * It opens the db, uses a cursor to delete the entry if the entryId and the feedId match
	 * </p>
	 * 
	 * @param entryId id of the entry to delete
	 * @return boolean true if the feed is deleted, false if not
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	public boolean deleteEntry(String entryId) throws NumberFormatException,SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return false;
			}
		}
		
		try {
			DBEntry mEntry = this.mDb.getDBEntry(Long.parseLong(entryId));
			if (mEntry != null) {
				this.mDb.removeEntry(mEntry.getId());
			} else {
				return false;
			}
		} finally {
			this.mDb.close();
		}
		this.mDb.close();

		return true;
	}
}