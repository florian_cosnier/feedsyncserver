package com.thales.feedsyncserver.resources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.abdera.ext.geo.GeoHelper;
import org.apache.abdera.ext.geo.Point;
import org.apache.abdera.ext.geo.Position;
import org.apache.abdera.model.Entry;
import org.apache.abdera.protocol.client.AbderaClient;
import org.apache.commons.httpclient.HttpException;
import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

import sun.misc.BASE64Decoder;

import com.thales.feedsyncserver.abdera.AbderaSupport;
import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.db.DBEntry;
import com.thales.feedsyncserver.db.DBFeed;

/**
 * This class represents the object Comment, it is implemented to manipulate
 * easily the ATOM entry
 * 
 * @author Florian COSNIER
 *
 */
public class CommentResource {
	private DB mDb;
	public InetAddress IP;
	public String httpAddress;
	
	/**
	 * POST -> 201 + Comment with modified ID
	 * PUT -> 200 OK
	 * DELETE -> 200 OK
	 */

	/**
	 * <p>
	 * Constructor </br>
	 * Initialize the IP address of the server and the database
	 * </p>
	 * 
	 * @throws SqlJetException
	 */
	public CommentResource() throws SqlJetException {
		this.mDb = new DB();

		this.IP = null;
		try {
			this.IP = InetAddress.getLocalHost();
			this.httpAddress = ("http://" + this.IP.getHostAddress() + ":8083");
		
			Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                    NetworkInterface ni = eni.nextElement();
                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                    while(inetAddresses.hasMoreElements()) {
                            InetAddress ia = inetAddresses.nextElement();
                            if(!ia.isLinkLocalAddress()) {
                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
                                    if(ni.getName().contains("wlan")){
                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
                                    }
                            }
                    }
            }
		
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * This function gets the comment from the database with the id (commentId) </br>
	 * It opens the db, uses a cursor to get the entry if the commentId matches
	 * </p>
	 * 
	 * @param feedId
	 * @param commentId
	 * @return Entry
	 */
	public Entry getComment(String commentId) {
		Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		DBEntry mDbEntry = new DBEntry();
		try {
			mDbEntry = this.mDb.getDBEntry(Long.parseLong(commentId));
			if (mDbEntry != null) {
				mEntry.setId(String.valueOf(mDbEntry.getId()));
				mEntry.setTitle(mDbEntry.getTitle());
				mEntry.setContent(mDbEntry.getContent());
				mEntry.setPublished(mDbEntry.getCreated());
				mEntry.setUpdated(new Date(mDbEntry.getUpdated().toGMTString()));
				if (mDbEntry.getAuthor() != null) {
					mEntry.addAuthor(mDbEntry.getAuthor());
				}
				mEntry.setSummaryAsHtml(mDbEntry.getSummary());
				if (!String.valueOf(mDbEntry.getLatitude()).isEmpty()) {
					GeoHelper.addPosition(mEntry,new Point(mDbEntry.getLatitude(), mDbEntry.getLongitude()));
				}
				mEntry.addLink(mDbEntry.getUrl(), "self","application/atom+xml", null, null, 0);
				mEntry.addLink(mDbEntry.getUrl(), "edit","application/atom+xml", null, null, 0);
			}
		} catch (SqlJetException e) {
			System.out.println("ERROR executing get entries");
			e.printStackTrace();
			return null;
		}
		return mEntry;
	}

	/**
	 * <p>
	 * This function does the same thing as the method getEntry but it returns a DBEntry
	 * </p>
	 * @param entryId id of the entry to get
	 * @return DBEntry
	 */
	public DBEntry getDBEntry(String entryId) {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		
		DBEntry mDbEntry = new DBEntry();
		try {
			mDbEntry = this.mDb.getDBEntry(Long.parseLong(entryId));
		} catch (SqlJetException e) {
			System.out.println("ERROR executing get entries");
			e.printStackTrace();
			return null;
		}
		return mDbEntry;
	}

	/**
	 * <p>
	 * This function adds a comment in the database </br>
	 * It opens the db and adds the comment then returns the id of the new comment </br>
	 * Once the comment is well added, the function returns the comment updated with the id and the link of the location of the comment
	 * </p>
	 * 
	 * @param feedId id of the feed where the comment will be added
	 * @param mComment comment to add
	 * @return Entry comment updated once it is added in the database
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 */
	public Entry postComment(String feedId, String entryId, Entry mComment)throws URISyntaxException, SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		
		long commentId = 0;

		DBEntry mDbEntry = new DBEntry();
		if(mComment.getTitle()!=null){
			mDbEntry.setTitle(mComment.getTitle());
		}else{
			mDbEntry.setTitle("");
		}
		if(mComment.getContent()!=null){
			mDbEntry.setContent(mComment.getContent());
		}else{
			mDbEntry.setContent("");
		}
		if(mComment.getSummary()!=null){
			mDbEntry.setSummary(mComment.getSummary());
		}else{
			mDbEntry.setSummary("");
		}
		
		mDbEntry.setFeed_id(feedId);
		if (mComment.getUpdated() == null) {
			mDbEntry.setUpdated(new Date());
		} else {
			mDbEntry.setUpdated(new Date());
		}
		
		if(mComment.getAuthor()!=null){
			mDbEntry.setAuthor(mComment.getAuthor());
		}else{
			mDbEntry.setAuthor(AbderaSupport.getAbdera().newEntry().addAuthor(""));
		}
		
		mDbEntry.setComment_to_entry(entryId);
		if(mComment.getCategories()!=null){
			if(mComment.getCategories().get(0)!=null){
				mDbEntry.set_id(mComment.getCategories().get(0).getText());
				System.out.println("mDbEntry.get_id() 2 "+mComment.getCategories().get(0).getText());
				System.out.println("mDbEntry.get_id() 3 "+mComment.getCategories().get(0).getLabel());
			}else{
				mDbEntry.set_id(new Date().toGMTString());
			}
		}else{
			mDbEntry.set_id(new Date().toGMTString());
		}
		System.out.println("mDbEntry.get_id() 1 "+mDbEntry.get_id());
		System.out.println("mDbEntry.get_id() 1 "+mDbEntry.get_id());
		
		if (GeoHelper.getPositions(mComment) != null) {
			Position[] position = GeoHelper.getPositions(mComment);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		
		
		long mIDFeed = 0;
		long mIDEntryFeed = 0;
		Boolean existedComment = false;
		
		if(this.mDb.getDBEntry(Long.parseLong(entryId))==null){
			return null;
		}
		
		try {
			commentId = this.mDb.addItemEntry(mDbEntry);
			this.mDb.updateCommentURL(commentId);
			
			DBEntry mDBCommentToEntry = this.getDBEntry(entryId);
			mDBCommentToEntry.setUpdated(new Date());
			this.mDb.updateEntry(mDBCommentToEntry);
			
			List<DBFeed> mFeedList =  this.mDb.getDBFeed();
			
			if(this.mDb.getDBFeed(Long.parseLong(mDBCommentToEntry.getFeed_id())).getTitle()!=null){
				if(this.mDb.getDBFeed(Long.parseLong(mDBCommentToEntry.getFeed_id())).getTitle().contains("SMS")){
				 	for(int k =0;k<mFeedList.size();k++){
						 DBFeed mDBFeed = new DBFeed();
						 mDBFeed = mFeedList.get(k);
						 System.out.println("mDBFeed.getTitle() "+mDBFeed.getTitle());
						 System.out.println("mDBCommentToEntry.getTitle() "+mDBCommentToEntry.getTitle());
						 if(mDBFeed.getTitle().contains(mDBCommentToEntry.getTitle())){
							 List<DBEntry> mList = this.mDb.getDBEntries(String.valueOf(mDBFeed.getId()));
							 for(int i=0;i<mList.size();i++){
								 if(mList.get(i).getTitle().contains(mComment.getAuthor().getName())){
									 System.out.println("mList.get(i).getTitle() "+mList.get(i).getTitle());
									 List<DBEntry> mListComment = this.mDb.getDBComment(String.valueOf(mList.get(i).getId()));
									 for(int j=0;j<mListComment.size();j++){
										 System.out.println("mListComment.get(i).get_id()) "+mListComment.get(i).get_id());
										 System.out.println("mDbEntry.get_id() "+mDbEntry.get_id());
										 if(mListComment.get(i).get_id() == mDbEntry.get_id()){
											 existedComment = true;
											 //mComment.getCategories().get(0).getText().toString()
										 }
									 }
									 if(existedComment == false){
										 mIDFeed = mDBFeed.getId();
										 mIDEntryFeed = mList.get(i).getId();
										 System.out.println("existedComment false");
									 }
									 break;
								 }
							 }
							 break;
						 }
				 	}
				}
			}
			
		} catch (SqlJetException e) {
			System.out.println("Error add entry in the database, SQL Busy");
			e.printStackTrace();
			return null;
		}
		mDbEntry = this.mDb.getDBEntry(commentId);
		
		if(existedComment==false){
			AbderaClient mAbderaClient = new AbderaClient();
			mAbderaClient.post(httpAddress+"/feedsync/rest/myfeeds/"+mIDFeed+"/"+mIDEntryFeed+"/addComment",mComment); 
		}
		
		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		newEntry = mComment;
		newEntry.setId(String.valueOf(commentId));
		newEntry.addLink(mDbEntry.getUrl(), "self", "application/atom+xml",null, null, 0);
		newEntry.addLink(mDbEntry.getUrl() + "/editComment", "edit","application/atom+xml", null, null, 0);

		return newEntry;
	}

	/**
	 * <p>
	 * This function adds a media comment in the database </br>
	 * It opens the db and adds the media comment then returns the id of the new comment </br>
	 * Once the comment is well added, the function returns the comment updated with the id, a link of the location of the comment and a link to the media
	 * </p>
	 * 
	 * @param feedId id of the feed where the entry will be added
	 * @param entryId id of the entry in which the media comment will be linked
	 * @param mComment comment to add
	 * @return Entry comment updated once it is added in the database
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 */
	public Entry postMediaComment(String feedId, String entryId, Entry mComment)throws URISyntaxException, SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		
		long commentId = 0;

		DBEntry mDbEntry = new DBEntry();
		if(mComment.getTitle()!=null){
			mDbEntry.setTitle(mComment.getTitle());
		}else{
			mDbEntry.setTitle("");
		}
		
		if(mComment.getSummary()!=null){
			mDbEntry.setSummary(mComment.getSummary());
		}else{
			mDbEntry.setSummary("");
		}
		
		mDbEntry.setFeed_id(feedId);
		if (mComment.getUpdated() == null) {
			mDbEntry.setUpdated(new Date());
		} else {
			mDbEntry.setUpdated(new Date());
		}
		
		if(mComment.getAuthor()!=null){
			mDbEntry.setAuthor(mComment.getAuthor());
		}else{
			mDbEntry.setAuthor(AbderaSupport.getAbdera().newEntry().addAuthor(""));
		}
		
		mDbEntry.setComment_to_entry(entryId);
		if(mComment.getCategories()!=null){
			if(mComment.getCategories().get(0)!=null){
				mDbEntry.set_id(mComment.getCategories().get(0).getText().toString());
			}else{
				mDbEntry.set_id(new Date().toGMTString());
			}
		}else{
			mDbEntry.set_id(new Date().toGMTString());
		}

		BufferedImage image = null;

		System.out.println("item.content: " + mComment.getContent());
		if (mComment.getContent() != null) {
			try {
				BASE64Decoder decoder = new BASE64Decoder();
				byte[] imageByte = decoder.decodeBuffer(mComment.getContent());
				ByteArrayInputStream bis = new ByteArrayInputStream(imageByte);
				image = ImageIO.read(bis);
				bis.close();
			} catch (Exception error) {
				error.printStackTrace();
			}
		}
		mDbEntry.setContent(this.httpAddress + "/feedsync/media/test.jpg");
		if (GeoHelper.getPositions(mComment) != null) {
			Position[] position = GeoHelper.getPositions(mComment);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		try {
			commentId = this.mDb.addItemEntry(mDbEntry);
			this.mDb.updateCommentURL(commentId);
			
			DBEntry mDBCommentToEntry = this.getDBEntry(entryId);
			mDBCommentToEntry.setUpdated(new Date());
			this.mDb.updateEntry(mDBCommentToEntry);
			
		} catch (SqlJetException e) {
			System.out.println("Error add entry in the database, SQL Busy");
			e.printStackTrace();
			return null;
		}
//		File outputfile = new File("./webapps/feedsync/media/" + commentId+ ".jpg");
		File outputfile = new File("/home/tai/tomcat7/webapps/feedsync/media/" + commentId+ ".jpg");

		System.out.println("this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath() "+this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
		
		System.out.println("output 1: " + outputfile.getAbsolutePath());
		if (image != null) {
			if (outputfile != null) {
				System.out.println("output 2: " + outputfile.getAbsolutePath());
				try {
					ImageIO.write(image, "jpg", outputfile);
				} catch (IOException e) {
					e.printStackTrace();
				}
				System.out.println("output 3: " + outputfile.getAbsolutePath());
			}
		} else {
			System.out.println("error");
		}
		mDbEntry.setContent("<img src=\"" + this.httpAddress+ "/feedsync/media/" + commentId + ".jpg\">");
		mComment.setContent(this.httpAddress + "/feedsync/media/" + commentId+ ".jpg");
		this.mDb.updateContentEntry(mDbEntry);

		mDbEntry = this.mDb.getDBEntry(commentId);

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		newEntry = mComment;
		newEntry.setId(String.valueOf(commentId));
		newEntry.addLink(mDbEntry.getUrl(), "self", "application/atom+xml",null, null, 0);
		newEntry.addLink(mDbEntry.getUrl() + "/editComment", "edit","application/atom+xml", null, null, 0);

		return newEntry;
	}

	/**
	 * <p>
	 * This function updates a comment in the database </br>
	 * It opens the db and edits the comment with the id (commentId) </br>
	 * Once the comment is well edited, the function returns a boolean to notify if the edition is done </br>
	 * </p>
	 * 
	 * @param feedId id of the feed 
	 * @param commentId id of the entry to edit
	 * @param mComment comment edited
	 * @return Boolean true well edited, false not edited
	 */
	public Boolean putComment(String feedId, String commentId, Entry mComment) {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return Boolean.valueOf(false);
			}
		}
		
		DBEntry mDbTempEntry = new DBEntry();
		
		try {
			mDbTempEntry = this.mDb.getDBEntry(Long.parseLong(commentId));
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		} catch (SqlJetException e1) {
			e1.printStackTrace();
		}
		DBEntry mDbEntry = new DBEntry();
		mDbEntry.setId(Long.parseLong(commentId));
		if (mComment.getTitle() != null) {
			if (!mComment.getTitle().isEmpty()) {
				mDbEntry.setTitle(mComment.getTitle());
			} else {
				mDbEntry.setTitle(mDbTempEntry.getTitle());
			}
		} else {
			mDbEntry.setTitle(mDbTempEntry.getTitle());
		}
		if (mComment.getAuthor() != null) {
			mDbEntry.setAuthor(mComment.getAuthor());
		} else {
			mDbEntry.setAuthor(mDbTempEntry.getAuthor());
		}
		if (mComment.getContent() != null) {
			if (!mComment.getContent().isEmpty()) {
				mDbEntry.setContent(mComment.getContent());
			} else {
				mDbEntry.setContent(mDbTempEntry.getContent());
			}
		} else {
			mDbEntry.setContent(mDbTempEntry.getContent());
		}
		if (mComment.getSummary() != null) {
			if (!mComment.getSummary().isEmpty()) {
				mDbEntry.setSummary(mComment.getSummary());
			} else {
				mDbEntry.setSummary(mDbTempEntry.getSummary());
			}
		} else {
			mDbEntry.setSummary(mDbTempEntry.getSummary());
		}
		mDbEntry.setUpdated(new Date());
		if (GeoHelper.getPositions(mComment) != null) {
			Position[] position = GeoHelper.getPositions(mComment);
			Position[] arrayOfPosition1;
			int j = (arrayOfPosition1 = position).length;
			for (int i = 0; i < j; i++) {
				Position pos = arrayOfPosition1[i];
				if ((pos instanceof Point)) {
					Point p = (Point) pos;
					System.out.println(p.getCoordinate());
					mDbEntry.setLatitude(p.getCoordinate().getLatitude());
					mDbEntry.setLongitude(p.getCoordinate().getLongitude());
				}
			}
		}
		try {
			this.mDb.updateEntry(mDbEntry);
			
			DBEntry mDBCommentToEntry = this.getDBEntry(mDbTempEntry.getComment_to_entry());
			mDBCommentToEntry.setUpdated(new Date());
			this.mDb.updateEntry(mDBCommentToEntry);
			
		} catch (SqlJetException e) {
			System.out.println("ERROR edition entry");
			e.printStackTrace();
			return false;
		}
		return true;
	}

	/**
	 * <p>
	 * This function deletes a comment in the database </br>
	 * It opens the db, uses a cursor to delete the comment if the commentId and the feedId match
	 * </p>
	 * 
	 * @param commentId id of the comment to delete
	 * @return boolean true if the feed is deleted, false if not
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	public boolean deleteComment(String commentId)throws NumberFormatException, SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return false;
			}
		}
		try {
			DBEntry mEntry = this.mDb.getDBEntry(Long.parseLong(commentId));
			if (mEntry != null) {
				this.mDb.removeEntry(mEntry.getId());
			} else {
				return false;
			}
		} finally {
			this.mDb.close();
		}
		this.mDb.close();

		return true;
	}
}