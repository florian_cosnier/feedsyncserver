package com.thales.feedsyncserver.resources;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import org.apache.abdera.ext.geo.GeoHelper;
import org.apache.abdera.ext.geo.Point;
import org.apache.abdera.model.Category;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.tmatesoft.sqljet.core.SqlJetErrorCode;
import org.tmatesoft.sqljet.core.SqlJetException;

import com.thales.feedsyncserver.abdera.AbderaSupport;
import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.db.DBEntry;
import com.thales.feedsyncserver.db.DBFeed;

/**
 * This class represents the object Feed, it is implemented to manipulate easily
 * the ATOM feed
 * 
 * @author Florian COSNIER
 *
 */
public class FeedResource {
	private DB mDb;
	public InetAddress IP;
	public String httpAddress;

	/**
	 * <p>
	 * Constructor</br>
	 * Initialize the IP address of the server and the database
	 * </p>
	 * 
	 * @throws SqlJetException
	 */
	public FeedResource() throws SqlJetException {
		this.mDb = new DB();

		this.IP = null;
		try {
			this.IP = InetAddress.getLocalHost();
			this.httpAddress = ("http://" + this.IP.getHostAddress() + ":8083");
		
			Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                    NetworkInterface ni = eni.nextElement();
                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                    while(inetAddresses.hasMoreElements()) {
                            InetAddress ia = inetAddresses.nextElement();
                            if(!ia.isLinkLocalAddress()) {
                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
                                    if(ni.getName().contains("wlan")){
                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
                                    }
                            }
                    }
            }
			
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * <p>
	 * This function creates a feed </br>
	 * The author corresponds to the argument "userId" </br>
	 * At first, it creates an object DBFeed to add the informations in the database </br>
	 * then it creates an Atom Feed corresponding to the DBFeed, and it returns it.
	 * </p>
	 * 
	 * @param userId id of the user 
	 * @return the feed created with his id
	 * @throws SqlJetException
	 */
	public Feed createFeedWithTitle(String userId, String title) {
		Feed mFeed = AbderaSupport.getAbdera().getFactory().newFeed();
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}

		DBFeed mDbFeed = new DBFeed();
		try {
			long id = this.mDb.addEmptyFeedWithTitle(userId, title);
			this.mDb.updateFeedURL(id);
			mDbFeed = this.mDb.getDBFeed(id);
			mFeed.setTitle(mDbFeed.getTitle());
			mFeed.addLink("");
			mFeed.addLink(mDbFeed.getUrl() + "/entries", "self");
			mFeed.setUpdated(new Date());
			mFeed.addAuthor(userId);
			mFeed.setId(String.valueOf(id));
		} catch (SqlJetException e) {
			if (e.getErrorCode() == SqlJetErrorCode.BUSY) {
				mFeed = null;
				System.out.println("ERROR SQL");
			} else if (e.getErrorCode() == SqlJetErrorCode.IOERR) {
				mFeed = null;
				System.out.println("ERROR SQL");
			}
			e.printStackTrace();
		}
		return mFeed;
	}

	/**
	 * <p>
	 * This function creates a feed </br>
	 * The author corresponds to the argument "userId" </br>
	 * At first, it creates an object DBFeed to add the informations in the database </br>
	 * then it creates an Atom Feed corresponding to the DBFeed, and it returns it.
	 * </p>
	 * 
	 * @param userId id of the user 
	 * @return the feed created with his id
	 * @throws SqlJetException
	 */
	public Feed createFeed(String userId) {
		Feed mFeed = AbderaSupport.getAbdera().getFactory().newFeed();
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
				return null;
			}
		}
		
		DBFeed mDbFeed = new DBFeed();
		try {
			long id = this.mDb.addEmptyFeed(userId);
			this.mDb.updateFeedURL(id);
			mDbFeed = this.mDb.getDBFeed(id);
			mFeed.addLink("");
			mFeed.addLink(mDbFeed.getUrl(), "self");
			mFeed.setUpdated(new Date());
			mFeed.addAuthor(userId);
			mFeed.setId(String.valueOf(id));
		} catch (SqlJetException e) {
			if (e.getErrorCode() == SqlJetErrorCode.BUSY) {
				mFeed = null;
				System.out.println("ERROR SQL");
			} else if (e.getErrorCode() == SqlJetErrorCode.IOERR) {
				mFeed = null;
				System.out.println("ERROR SQL");
			}
			e.printStackTrace();
		}
		return mFeed;
	}

	/**
	 * <p>
	 * This functions gets the entries of the feed with the id (feedId) </br>
	 * The user can get a certain number of entries (nbEntry) from this feed
	 * </p>
	 * 
	 * @param feedId id of the feed where the entries are gotten
	 * @param userId id of the user to check for the entries specially for the 
	 * @param nbEntry number of entries to get
	 * @return the feed with the entries
	 * @throws SqlJetException 
	 */
	@SuppressWarnings("deprecation")
	public Feed getEntries(String feedId, String userId, int nbEntry, int sec)throws SqlJetException {
		Feed mFeed = AbderaSupport.getAbdera().getFactory().newFeed();
		Feed returnedFeed = AbderaSupport.getAbdera().getFactory().newFeed();

		Boolean checkFeed = Boolean.valueOf(false);
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
			}
		}
		
		DBFeed mDBFeed = new DBFeed();
		if (!feedId.isEmpty()) {
			mDBFeed = this.mDb.getDBFeed(Long.parseLong(feedId));
			if (feedId.equals(String.valueOf(mDBFeed.id))) {
				mFeed.setTitle(mDBFeed.title);
				mFeed.setId(String.valueOf(mDBFeed.id));
				mFeed.addLink("");
				mFeed.addLink(mDBFeed.getUrl(), "self");
				mFeed.setUpdated(new Date());
				mFeed.addAuthor(mDBFeed.author);
				checkFeed = Boolean.valueOf(true);
			}
		}
		
		List<DBEntry> mListDbEntries = new ArrayList<DBEntry>();
		mListDbEntries = this.mDb.getDBEntries(feedId);
		if (nbEntry == -1) {
			nbEntry = mListDbEntries.size();
		}
		Date actualTime = new Date();
		if (!feedId.equals("3")) {
			if (!mListDbEntries.isEmpty()) {
				for (int i = mListDbEntries.size() - nbEntry; i < mListDbEntries.size(); i++) {
					Date time = new Date(mListDbEntries.get(i).getUpdated().toGMTString());
					double diff = actualTime.getTime() - time.getTime();
					int diff1 = (int) (diff / 1000);
					if (sec != 0) {
						if (diff1 <= sec) {
							Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
							mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
							mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
							if (mListDbEntries.get(i).getAuthor() != null) {
								mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
							}
							mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
							mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
							if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
								GeoHelper.addPosition(mEntry,new Point(((DBEntry) mListDbEntries.get(i)).getLatitude(),((DBEntry) mListDbEntries.get(i)).getLongitude()));
							}
							mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
							mEntry.addLink(mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null,0);
							mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editEntry", "edit","application/atom+xml", null, null, 0);
							mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/comments", "replies","application/atom+xml", null, null, 0);

							mFeed.addEntry(mEntry);
						}
					} else {
						Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
						mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
						mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
						if (mListDbEntries.get(i).getAuthor() != null) {
							mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
						}
						mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
						mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
						if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
							GeoHelper.addPosition(mEntry,new Point(((DBEntry) mListDbEntries.get(i)).getLatitude(),((DBEntry) mListDbEntries.get(i)).getLongitude()));
						}
						mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
						mEntry.addLink(mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null, 0);
						mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editEntry", "edit","application/atom+xml", null, null, 0);
						mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/comments", "replies","application/atom+xml", null, null, 0);

						mFeed.addEntry(mEntry);
					}
				}
			}
		} else if ((feedId.equals("3")) && (!mListDbEntries.isEmpty())) {
			for (int i = 0; i < mListDbEntries.size(); i++) {
				if (mListDbEntries.get(i).getAuthor().getName().contains(userId)) {
					Date time = new Date(mListDbEntries.get(i).getUpdated().toGMTString());
					double diff = actualTime.getTime() - time.getTime();
					int diff1 = (int) (diff / 1000);
					if (sec != 0) {
						if (diff1 <= sec) {
							Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
							mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
							mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
							if (mListDbEntries.get(i).getAuthor() != null) {
								mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
							}
							mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
							mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
							if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
								GeoHelper.addPosition(mEntry,new Point(mListDbEntries.get(i).getLatitude(),mListDbEntries.get(i).getLongitude()));
							}
							mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
							mEntry.addLink(mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null,0);
							mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editEntry", "edit","application/atom+xml", null, null, 0);
							mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/comments", "replies","application/atom+xml", null, null, 0);

							mFeed.addEntry(mEntry);
						}
					} else {
						Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
						mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
						mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
						if (mListDbEntries.get(i).getAuthor() != null) {
							mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
						}
						mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
						mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
						if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
							GeoHelper.addPosition(mEntry,new Point(mListDbEntries.get(i).getLatitude(),mListDbEntries.get(i).getLongitude()));
						}
						mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
						mEntry.addLink( mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null, 0);
						mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editEntry", "edit","application/atom+xml", null, null, 0);
						mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/comments", "replies","application/atom+xml", null, null, 0);

						mFeed.addEntry(mEntry);
					}
				}
			}
		}
		if (checkFeed.booleanValue()) {
			returnedFeed = mFeed;
		} else {
			returnedFeed = null;
		}
		return returnedFeed;
	}

	/**
	 * <p>
	 * This functions gets the comments of the entry with the id (entryId) </br>
	 * The user can get a certain number of comments (nbEntry) from this entry
	 * </p>
	 * 
	 * @param feedId id of the feed where the entries are gotten
	 * @param nbEntry number of entries to get
	 * @return the feed with the entries
	 * @throws SqlJetException 
	 */
	@SuppressWarnings("deprecation")
	public Feed getComments(String feedId, String entryId, int nbEntry, int sec)throws SqlJetException {
		Feed mFeed = AbderaSupport.getAbdera().getFactory().newFeed();
		Feed returnedFeed = AbderaSupport.getAbdera().getFactory().newFeed();

		Boolean checkFeed = Boolean.valueOf(false);
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
			}
		}
		DBFeed mDBFeed = new DBFeed();
		if (!feedId.isEmpty()) {
			mDBFeed = this.mDb.getDBFeed(Long.parseLong(feedId));
			if (feedId.equals(String.valueOf(mDBFeed.id))) {
				mFeed.setTitle(mDBFeed.title + "-Comments");
				String[] mAddress = mDBFeed.getUrl().split("/entries");
				mFeed.setId(mAddress[0] + "/" + entryId + "/comments");
				mFeed.addLink("");
				mFeed.addLink(mDBFeed.getUrl() + "/entries", "self");
				mFeed.setUpdated(new Date());
				mFeed.addAuthor(mDBFeed.author);
				checkFeed = Boolean.valueOf(true);
			}
		}
		Date actualTime = new Date();

		List<DBEntry> mListDbEntries = new ArrayList<DBEntry>();
		mListDbEntries = this.mDb.getDBComment(entryId);
		System.out.println("mListDbEntries.size() " + mListDbEntries.size());
		if (nbEntry == -1) {
			nbEntry = mListDbEntries.size();
		}
		if (!mListDbEntries.isEmpty()) {
			for (int i = mListDbEntries.size() - nbEntry; i < mListDbEntries.size(); i++) {
				Date time = new Date(mListDbEntries.get(i).getUpdated().toGMTString());
				double diff = actualTime.getTime() - time.getTime();
				System.out.println("diff " + diff);
				int diff1 = (int) (diff / 1000);
				System.out.println("diff1 " + diff1);
				if (sec != 0) {
					System.out.println("sec " + sec);
					if (diff1 <= sec) {
						Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
						mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
						System.out.println("mListDbEntries.get(i).getContent() "+ mListDbEntries.get(i).getContent());
						mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
						if (mListDbEntries.get(i).getAuthor() != null) {
							mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
						}
						mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
						mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
						if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
							GeoHelper.addPosition(mEntry,new Point(((DBEntry) mListDbEntries.get(i)).getLatitude(),mListDbEntries.get(i).getLongitude()));
						}
						Category cat = AbderaSupport.getAbdera().getFactory().newCategory();
						cat.setLabel(mListDbEntries.get(i).get_id());
						cat.setText(mListDbEntries.get(i).get_id());
						mEntry.addCategory(cat);

						mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
						mEntry.addLink(mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null, 0);
						mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editComment", "edit","application/atom+xml", null, null, 0);

						mFeed.addEntry(mEntry);
					}
				} else {
					Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
					mEntry.setTitleAsHtml(mListDbEntries.get(i).getTitle());
					System.out.println("mListDbEntries.get(i).getContent() "+ mListDbEntries.get(i).getContent());
					mEntry.setContentAsHtml(mListDbEntries.get(i).getContent());
					if (mListDbEntries.get(i).getAuthor() != null) {
						mEntry.addAuthor(mListDbEntries.get(i).getAuthor());
					}
					mEntry.setSummaryAsHtml(mListDbEntries.get(i).getSummary());
					mEntry.setId(String.valueOf(mListDbEntries.get(i).getId()));
					if (!String.valueOf(mListDbEntries.get(i).getLatitude()).isEmpty()) {
						GeoHelper.addPosition(mEntry,new Point(((DBEntry) mListDbEntries.get(i)).getLatitude(),mListDbEntries.get(i).getLongitude()));
					}
					Category cat = AbderaSupport.getAbdera().getFactory().newCategory();
					cat.setLabel(mListDbEntries.get(i).get_id());
					cat.setText(mListDbEntries.get(i).get_id());
					mEntry.addCategory(cat);

					mEntry.setUpdated(new Date(mListDbEntries.get(i).getUpdated().toGMTString()));
					mEntry.addLink(mListDbEntries.get(i).getUrl(),"self", "application/atom+xml", null, null, 0);
					mEntry.addLink(mListDbEntries.get(i).getUrl()+ "/editComment", "edit","application/atom+xml", null, null, 0);

					mFeed.addEntry(mEntry);
				}
			}
		}
		if (checkFeed.booleanValue()) {
			returnedFeed = mFeed;
		} else {
			returnedFeed = null;
		}
		return returnedFeed;
	}

	/**
	 * <p>
	 * This function edits a feed in the database </br>
	 * It opens the db and edits the feed with the id (feedId) </br>
	 * Once the feed is well edited, the function returns a boolean to notify if the edition is done
	 * </p>
	 * 
	 * @param feedId id of the feed to edit
	 * @param mFeed feed edited
	 * @return boolean true well edited, false not edited
	 */
	public boolean putFeed(String feedId, Feed mFeed) {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				e.printStackTrace();
				System.out.println("ERROR opening DB");
			}
		}
		DBFeed mDbFeed = new DBFeed();
		mDbFeed.setId(Integer.parseInt(feedId));
		mDbFeed.setTitle(mFeed.getTitle());
		mDbFeed.setAuthor(mFeed.getAuthor().getName());
		try {
			this.mDb.updateFeeditem(mDbFeed);
		} catch (SqlJetException e) {
			System.out.println("ERROR edition feed");
			e.printStackTrace();
		}
		return true;
	}

	/**
	 * This function deletes a feed in the database 
	 * 
	 * @param feedId id of the feed to delete
	 * @return a boolean true if the feed is deleted, false if not
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	public boolean deleteFeed(String feedId) throws NumberFormatException,
			SqlJetException {
		if (this.mDb == null) {
			try {
				this.mDb = new DB();
			} catch (SqlJetException e) {
				System.out.println("ERROR opening DB");
				e.printStackTrace();
			}
		}
		try {
			DBFeed mFeed = this.mDb.getDBFeed(Long.parseLong(feedId));
			if (mFeed != null) {
				this.mDb.removeFeed(mFeed.id);
			} else {
				return false;
			}
		} finally {
			this.mDb.close();
		}
		this.mDb.close();

		return true;
	}
}