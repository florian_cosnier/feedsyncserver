package com.thales.feedsyncserver.geo;

import java.util.ArrayList;
import java.util.List;

import org.apache.abdera.ext.geo.Point;

import com.thales.feedsyncserver.db.DBEntry;

/**
 * <p>
 * This class provides functions to check, to test if a position like the user position 
 * is in a geofence.</br> A geofence is an area delimited by points, this area is represented with a list of points.
 * </p>
 * 
 * @author Florian COSNIER
 *
 */
public class GeoFilter {
	
	public GeoFilter(){
		
	}
	
	/**
	 * This function checks if the position (mPosition) is in a geofence (List of a points, mPoints)
	 * 
	 * @param mPosition position
	 * @param mPoints list of the points of the geofence
	 * @return true if the user is in the geofence, false if not
	 */	
	public boolean checkPosition(Point mPosition, ArrayList<Point> mPoints) {
	      int i;
	      int j;
	      Point test = new Point(mPosition.getCoordinate().getLatitude(),mPosition.getCoordinate().getLongitude());
	      boolean result = false;
	      for (i = 0, j = mPoints.size() - 1; i < mPoints.size(); j = i++) {
	        if ((mPoints.get(i).getCoordinate().getLatitude() > test.getCoordinate().getLatitude()) != (mPoints.get(j).getCoordinate().getLatitude() > test.getCoordinate().getLatitude()) &&
	            (test.getCoordinate().getLongitude() < (mPoints.get(j).getCoordinate().getLongitude() - mPoints.get(i).getCoordinate().getLongitude()) * (test.getCoordinate().getLatitude() - mPoints.get(i).getCoordinate().getLatitude()) / (mPoints.get(j).getCoordinate().getLatitude()-mPoints.get(i).getCoordinate().getLatitude()) + mPoints.get(i).getCoordinate().getLongitude())) {
	          result = !result;
	         }
	      }
	      return result;
	 }
	
	/**
	 * <p>
	 * This function checks if some users from the list passed in arguments are in a geofence </br>
	 * The geofence is dedicated to an entry passed in arguments </br>
	 * It checks the positions and returns the positions which are in the geofence
	 * </p>
	 * 
	 * @param mPositionsList list of positions to check
	 * @param mDbEntry entry with the fence
	 * @return List the list of the users
	 */
	public List<String> checkPositionsList(List<Point> mPositionsList, DBEntry mDbEntry){
		ArrayList<String> mList = new ArrayList<String>();
		ArrayList<Point> mPoints = new ArrayList<Point>();
		String mFence[]= mDbEntry.getContent().split("///"); //the content is used for the geofence
		if(mFence.length % 2 == 0){
			for(int i=0; i<mFence.length;i+=2){
				Point mGeoPoint = new Point(Double.parseDouble(mFence[i]),Double.parseDouble(mFence[i+1]));
				mPoints.add(mGeoPoint);
			}
		}
		
		for(int j=0;j<mPositionsList.size();j++){
			if(checkPosition(mPositionsList.get(j),mPoints)){
				mList.add("j");
			}
		}
		
		return mList;
	}
	
	/**
	 * <p>
	 * This function checks if some users from the list passed in arguments are in a geofence </br>
	 * The geofence is dedicated to an entry passed in arguments </br>
	 * It checks the positions and returns the positions which are in the geofence
	 * </p>
	 * 
	 * @param mPositionsList list of positions to check
	 * @param mDbEntry entry with the fence
	 * @return String the list of the users
	 */
	public String checkUsersPositionsList(List<DBEntry> mPositionsList, DBEntry mDbEntry){
		String mList = "";
		ArrayList<Point> mPoints = new ArrayList<Point>();
		String mFence[]= mDbEntry.getContent().split("///"); //the content is used for the geofence
		if(mFence.length % 2 == 0){
			for(int i=0; i<mFence.length;i+=2){
				Point mGeoPoint = new Point(Double.parseDouble(mFence[i]),Double.parseDouble(mFence[i+1]));
				mPoints.add(mGeoPoint);
			}
		}
		
		for(int j=0;j<mPositionsList.size();j++){
			if(checkPosition(new Point(mPositionsList.get(j).getLatitude(),mPositionsList.get(j).getLongitude()),mPoints)){
				mList+=mPositionsList.get(j).getTitle();
				mList+=",";
			}
		}
		
		return mList;
	}
}
