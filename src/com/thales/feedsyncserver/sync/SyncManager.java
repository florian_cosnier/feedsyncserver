package com.thales.feedsyncserver.sync;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.core.Response;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.apache.commons.httpclient.HttpException;
import org.tmatesoft.sqljet.core.SqlJetException;

import com.thales.feedsyncserver.resources.CommentResource;
import com.thales.feedsyncserver.resources.EntryResource;
import com.thales.feedsyncserver.resources.FeedResource;

/**
 * This Class manages the synchronization between the server and the user. It
 * calls the different modules for the creation, the add , the management of the
 * feeds/entries etc...
 * 
 * @author Florian COSNIER
 *
 */
public class SyncManager {
	private FeedResource mFeedResource;
	private EntryResource mEntryResource;
	private CommentResource mCommentResource;

	public SyncManager() throws SqlJetException {
		this.mFeedResource = new FeedResource();
		this.mEntryResource = new EntryResource();
		this.mCommentResource = new CommentResource();
	}

	/**
	 * This function checks if the user with the id (userId) has the good rights
	 * to create a feed
	 * 
	 * @param userId
	 *            id of the user
	 * @return true if it has the rights, false if not
	 */
	public boolean checkUserFeed(String userId) {
		if (userId == null) {
			return false;
		}
		return true;
	}

	/**
	 * This function checks if the user with the id (userId) has the good rights to read/write in the feed with the id (feedId)
	 * 
	 * @param feedId the id of the feed to check
	 * @param userId id of the user
	 * @return true if it has the rights, false if not
	 */
	public boolean checkUserEntries(String feedId, String userId) {
		return true;
	}

	/**
	 * <p>
	 * This function creates a new Feed with the call of the class FeedResource</br>
	 * The return is a response:
	 * <ul>
	 * 	<li>Creation OK -> Status 200 and the new feed created</li>
	 * 	<li>if the user has not the good rights -> Status 401</li>
	 * 	<li>if the user has the rights but the feed does not have created -> Status 409</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @return Response
	 * @throws SqlJetException
	 * @throws URISyntaxException 
	 */
	public Response createFeed(String userId) throws SqlJetException,URISyntaxException {
		if (checkUserFeed(userId)) {
			Feed mFeed = this.mFeedResource.createFeed(userId);
			if (mFeed != null) {
				return Response
						.created(mFeed.getLink("self").getHref().toURI())
						.type("application/atom+xml;type=feed").entity(mFeed)
						.build();
			}
			return Response.status(409).entity("Server too busy").build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function creates a new Feed with the call of the class FeedResource</br>
	 * The return is a response:
	 * <ul>
	 * 	<li>Creation OK -> Status 200 and the new feed created</li>
	 * 	<li>if the user has not the good rights -> Status 401</li>
	 * 	<li>if the user has the rights but the feed does not have created -> Status 409</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @return Response
	 * @throws SqlJetException
	 * @throws URISyntaxException 
	 */
	public Response createFeed(String userId, String title)throws SqlJetException, URISyntaxException {
		if (checkUserFeed(userId)) {
			Feed mFeed = this.mFeedResource.createFeedWithTitle(userId, title);
			if (mFeed != null) {
				return Response
						.created(mFeed.getLink("self").getHref().toURI())
						.type("application/atom+xml;type=feed").entity(mFeed)
						.build();
			}
			return Response.status(409).entity("Server too busy").build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function returns a feed with the corresponded list of entries from the feed </br>
	 * with the id (feedId), before getting the entries, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>get OK -> Status 200 and the feed with entries</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not have got-> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to get entries
	 * @param nbEntry number of entries to get
	 * @return Response
	 * @throws SqlJetException 
	 */
	public Response getEntries(String userId, String feedId, int nbEntry,int sec) throws SqlJetException {
		if (checkUserEntries(feedId, userId)) {
			Feed mFeed = this.mFeedResource.getEntries(feedId, userId, nbEntry,sec);
			if (mFeed != null) { //Normal Case
				return Response.status(200).type("application/atom+xml").entity(mFeed)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	public Response getUpdatedContent(String userId, String feedId, int sec)throws SqlJetException {
		return null;
	}

	/**
	 * <p>
	 * This function returns a feed with the corresponded list of comments from the entry </br>
	 * with the id (entryId), before getting the comments, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>get OK -> Status 200 and the feed with comments</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not have got-> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to get comments
	 * @param entryId id of the entry in which the comments are linked
	 * @param nbEntry number of comments to get
	 * @return Response
	 * @throws SqlJetException 
	 */
	public Response getComments(String userId, String feedId, String entryId,int nbEntry, int sec) throws SqlJetException {
		if (checkUserEntries(feedId, userId)) {
			Feed mFeed = this.mFeedResource.getComments(feedId, entryId,nbEntry, sec);
			if (mFeed != null) {
				return Response.status(200).type("application/atom+xml").entity(mFeed)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).entity("No entry available, check if the entry exists").build();
		}
		return Response.status(401).entity("No entry available, check if the entry exists").build();
	}

	/**
	 * <p>
	 * This function returns a entry with the id (entryId) and with the feedId (feedId) </br>
	 * before getting the entry, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>get OK -> Status 200 and the entry</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the entry does not have got-> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to get entries
	 * @param entryId id of the entry to get
	 * @return Response
	 * @throws SqlJetException
	 */
	public Response getEntry(String userId, String feedId, String entryId)throws SqlJetException {
		if (checkUserEntries(feedId, userId)) {
			Entry mEntry = this.mEntryResource.getEntry(feedId, entryId);
			if (mEntry != null) {
				return Response.status(200).type("application/atom+xml").entity(mEntry)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).entity("No entry available").build();
	}

	/**
	 * <p>
	 * This function adds the entry with the id (entryId) in the feed with the id (feedId) </br>
	 * before adding, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>add OK -> Status 201 with the location link of the entry and the entry itself</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the fed does not exist or the entry can not be added -> Status 206</li>
	 * <ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to post the entry
	 * @param mEntry entry to post
	 * @return Response
	 * @throws URISyntaxException
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 */
	public Response postEntry(String userId, String feedId, Entry mEntry)throws URISyntaxException, SqlJetException, HttpException,IOException {
		if (checkUserEntries(feedId, userId)) {
			Entry newEntry = this.mEntryResource.postEntry(feedId, mEntry);
			if (newEntry != null) {
				return Response.created(newEntry.getLink("self").getHref().toURI())
						.type("application/atom+xml").entity(newEntry)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(206).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function adds the entry with a media in the feed with the id (feedId)</br>
	 * The media entry will be parsed and added with a link to the media
	 * before adding, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>add OK -> Status 201 with the location link of the entry and the entry itself</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not exist or the entry can not be added -> Status 206</li>
	 * <ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to post the media entry
	 * @param mEntry media entry to post
	 * @return Response
	 * @throws URISyntaxException
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 */
	public Response postMediaEntry(String userId, String feedId, Entry mEntry)throws URISyntaxException, SqlJetException, HttpException,IOException {
		if (checkUserEntries(feedId, userId)) {
			Entry newEntry = this.mEntryResource.postMediaEntry(feedId, mEntry);
			if (newEntry != null) {
				return Response.created(newEntry.getLink("self").getHref().toURI()).type("application/atom+xml").entity(newEntry)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(206).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function adds the comment in the feed with the id (feedId), this comment is linked to the entry with the id (entryId) </br>
	 * before adding, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>add OK -> Status 201 with the location link of the comment and the comment itself</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not exist or the entry can not be added -> Status 206</li>
	 * <ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to post the entry
	 * @param entryId id of the entry in which the comment is linked
	 * @param mEntry comment to post
	 * @return Response
	 * @throws URISyntaxException
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 */
	public Response postComment(String userId, String feedId, String entryId,Entry mEntry) throws URISyntaxException, SqlJetException,HttpException, IOException {
		if (checkUserEntries(feedId, userId)) {
			Entry newEntry = this.mCommentResource.postComment(feedId, entryId,mEntry);
			if (newEntry != null) {
				return Response.created(newEntry.getLink("self").getHref().toURI()).type("application/atom+xml").entity(newEntry)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(206).entity("No entry available, check if the entry exists").build();
		}
		return Response.status(401).entity("No entry available, check if the entry exists").build();
	}

	/**
	 * <p>
	 * This function adds the comment with a media in the feed with the id (feedId) linked to the entry with the id (entryId) </br>
	 * before adding, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>add OK -> Status 201 with the location link of the comment and the comment itself</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not exist or the entry can not be added -> Status 206</li>
	 * <ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to post the entry
	 * @param entryId id of the entry in which the comment is linked
	 * @param mEntry comment to post
	 * @return Response
	 * @throws URISyntaxException
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 */
	public Response postMediaComment(String userId, String feedId,String entryId, Entry mEntry) throws URISyntaxException,SqlJetException, HttpException, IOException {
		if (checkUserEntries(feedId, userId)) {
			Entry newEntry = this.mCommentResource.postMediaComment(feedId,entryId, mEntry);
			if (newEntry != null) {
				return Response.created(newEntry.getLink("self").getHref().toURI()).type("application/atom+xml").entity(newEntry)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(206).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function edits the entry with the id (entryId) </br>
	 * before editing, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>Edition OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the entry does not exist or can not be edited -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to edit one entry
	 * @param entryId id of the entry to edit
	 * @param mEntry entry edited
	 * @return Response
	 */
	public Response putEntry(String userId, String feedId, String entryId,
			Entry mEntry) {
		if (checkUserEntries(feedId, userId)) {
			if (mEntryResource.putEntry(feedId, entryId, mEntry)) {
				System.out.println("status(200)");
				return Response.status(200)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			System.out.println("status(204)");
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function edits the comment with the id (entryId) </br>
	 * before editing, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>Edition OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the comment does not exist or can not be edited -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to edit one entry
	 * @param entryId id of the entry to edit
	 * @param mEntry entry edited
	 * @return Response
	 */
	public Response putComment(String userId, String feedId, String entryId,Entry mEntry) {
		if (checkUserEntries(feedId, userId)) {
			if (this.mCommentResource.putComment(feedId, entryId, mEntry)) {
				return Response.status(200)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function edits the feed with the id (feedId) </br>
	 * before editing, it checks if the user has the good rights </br>
	 * The return is a response: 
	 * <ul>
	 * 		<li>Edition OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 *		<li>if the user has the rights but the feed does not exist or can not be edited -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to edit
	 * @param mFeed feed edited
	 * @return Response
	 */
	public Response putFeed(String userId, String feedId, Feed mFeed) {
		if (checkUserEntries(feedId, userId)) {
			if (this.mFeedResource.putFeed(feedId, mFeed)) {
				return Response.status(200)
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function deletes the feed with the id (feedId) </br>
	 * before deleting, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>Delete OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the feed does not exist -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param feedId id of the feed to delete
	 * @return Response
	 * @throws SqlJetException 
	 * @throws NumberFormatException 
	 */
	public Response deleteFeed(String userId, String feedId)throws NumberFormatException, SqlJetException {
		if (checkUserEntries(feedId, userId)) {
			if (this.mFeedResource.deleteFeed(feedId)) {
				return Response.ok().build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function deletes the entry with the id (entryId) </br>
	 * before deleting, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>Delete OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the entry does not exist -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param entryId id of the entry to delete
	 * @return Response
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	public Response deleteEntry(String userId, String entryId)throws NumberFormatException, SqlJetException {
		if (checkUserEntries(entryId, userId)) {
			if (this.mEntryResource.deleteEntry(entryId)) {
				return Response.ok()
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}

	/**
	 * <p>
	 * This function deletes the comment with the id (commentId) </br>
	 * before deleting, it checks if the user has the good rights </br>
	 * The return is a response:
	 * <ul>
	 * 		<li>Delete OK -> Status 200</li>
	 * 		<li>if the user has not the good rights -> Status 401</li>
	 * 		<li>if the user has the rights but the comment does not exist -> Status 204</li>
	 * </ul>
	 * </p>
	 * 
	 * @param userId id of the user
	 * @param entryId id of the entry to delete
	 * @return Response
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	public Response deleteComment(String userId, String commentId)throws NumberFormatException, SqlJetException {
		if (checkUserEntries(commentId, userId)) {
			if (this.mCommentResource.deleteComment(commentId)) {
				return Response.ok()
						.header("Access-Control-Allow-Origin", "*")
						.build();
			}
			return Response.status(204).build();
		}
		return Response.status(401).build();
	}
}