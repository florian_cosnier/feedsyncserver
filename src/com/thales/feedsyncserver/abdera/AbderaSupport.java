package com.thales.feedsyncserver.abdera;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.MessageBodyReader;
import javax.ws.rs.ext.MessageBodyWriter;
import javax.ws.rs.ext.Provider;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.abdera.Abdera;
import org.apache.abdera.model.Document;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * This class is an Abdera Support, it lets the server to read and write the ATOM Document 
 * 
 * @author Florian COSNIER
 *
 */
@Provider
@Produces("application/atom+xml")
@Consumes("application/atom+xml")
public class AbderaSupport implements MessageBodyWriter<Object>, 
       MessageBodyReader<Object> {

   private final static Abdera abdera = new Abdera();

   public static Abdera getAbdera() {
       return abdera;
   }

	@Override
	public boolean isReadable(Class<?> type, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (Feed.class.isAssignableFrom(type) || Entry.class.isAssignableFrom(type));
	}
	
	@Override
	public Object readFrom(Class<Object> feedOrEntry, Type arg1, Annotation[] arg2,
			MediaType arg3, MultivaluedMap<String, String> arg4, InputStream inputStream)
			throws IOException, WebApplicationException {

		Document<Element> doc = getAbdera().getParser().parse(inputStream);
	    Element el = doc.getRoot();

	    if (feedOrEntry.isAssignableFrom(el.getClass())) {
	        return el;
	    } 
	    else {
	        throw new IOException("Unexpected payload, expected "+feedOrEntry.getName()+
	            ", received "+el.getClass().getName());
	    }    
	}
	
	@Override
	public long getSize(Object arg0, Class<?> arg1, Type arg2, Annotation[] arg3,
			MediaType arg4) {
		return -1;
	}
	
	@Override
	public boolean isWriteable(Class<?> type, Type arg1, Annotation[] arg2,
			MediaType arg3) {
		return (Feed.class.isAssignableFrom(type) || Entry.class.isAssignableFrom(type));
	}
	
	@Override
	public void writeTo(Object feedOrEntry, Class<?> arg1, Type arg2, Annotation[] arg3,
			MediaType arg4, MultivaluedMap<String, Object> headers, OutputStream outputStream)
			throws IOException, WebApplicationException {
		if (feedOrEntry instanceof Feed) {
	        Feed feed = (Feed)feedOrEntry;
	        Document<Feed> doc = feed.getDocument();
	        doc.writeTo(outputStream);
	    } else {
	        Entry entry = (Entry)feedOrEntry;
	        Document<Entry> doc = entry.getDocument();
	        doc.writeTo(outputStream);
	    }
	}
	
	public org.w3c.dom.Document readXml(InputStream is) throws SAXException, IOException,
	    ParserConfigurationException {
	    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
	
	    dbf.setValidating(false);
	    dbf.setIgnoringComments(false);
	    dbf.setIgnoringElementContentWhitespace(true);
	    dbf.setNamespaceAware(true);

	    DocumentBuilder db = null;
	    db = dbf.newDocumentBuilder();
	    db.setEntityResolver(new NullResolver());

	    return db.parse(is);
	}

	class NullResolver implements EntityResolver {
		public InputSource resolveEntity(String publicId, String systemId) throws SAXException,
	    IOException {
			return new InputSource(new StringReader(""));
		}
	}
}