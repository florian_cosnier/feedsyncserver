package com.thales.feedsyncserver.db;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.SqlJetTransactionMode;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;
import org.tmatesoft.sqljet.core.table.ISqlJetTransaction;
import org.tmatesoft.sqljet.core.table.SqlJetDb;

/**
 * This class implements the database of the server
 * @author Florian COSNIER
 *
 */
public class DB {
	
	private static final int VERSION = 2;
	
	private static final String FILE_NAME = "databaseFeeds.db";

	private SqlJetDb db;
	
	public InetAddress IP;
	
	public String httpAddress;

	/**
	 * Constructor
	 * @throws SqlJetException
	 */
	public DB() throws SqlJetException {
		db = SqlJetDb.open(new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath()+"/../"+FILE_NAME), true);
		IP = null;
		   try {
			   IP = InetAddress.getLocalHost();
			   httpAddress = "http://"+IP.getHostAddress()+":8083";
			   
			   Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

	            while (eni.hasMoreElements()) {
	                    NetworkInterface ni = eni.nextElement();
	                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


	                    while(inetAddresses.hasMoreElements()) {
	                            InetAddress ia = inetAddresses.nextElement();
	                            if(!ia.isLinkLocalAddress()) {
	                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
	                                    if(ni.getName().contains("wlan")){
	                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
	                                    }
	                            }
	                    }
	            }
			   
		   } catch (UnknownHostException e1) {
			   // TODO Auto-generated catch block
			   e1.printStackTrace();
		   } catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   
		upgrade(VERSION);
		
	}
	
	/**
	 * This function close the database
	 * @throws SqlJetException
	 */
	public void close() throws SqlJetException {
		db.close();
		db = null;
	}

	public void beginTransaction(boolean write) throws SqlJetException {
		db.beginTransaction( write ? SqlJetTransactionMode.WRITE : SqlJetTransactionMode.READ_ONLY);
	}
	
	/**
	 * This function ends a transaction
	 * @throws SqlJetException
	 */
	public void commit() throws SqlJetException {
		db.commit();
	}
	
	public int getVersion() throws SqlJetException {
		return db.getOptions().getUserVersion();
	}
	
	/**
	 * Initialization of the tables
	 * @param targetVersion version of the database
	 * @throws SqlJetException
	 */
	private void upgrade(int targetVersion) throws SqlJetException {
		if (targetVersion < 1) {
			return;
		}
		if (getVersion() < 1) {
			db.runWriteTransaction(new ISqlJetTransaction() {

				public Object run(SqlJetDb db) throws SqlJetException {
					db.createTable("CREATE TABLE DBENTRY " +
				                   "(ID INTEGER PRIMARY KEY     ," +
				                   " _ID        		TEXT , " +
				                   " FEED_ID        	TEXT    , " +
				                   " COMMENT_ID        	TEXT    , " +
				                   " URL        		TEXT    , " +
				                   " NAME           	TEXT    , " + 
				                   " TITLE          	TEXT    NOT NULL, " + 
				                   " CONTENT        	TEXT	, " +
				                   " SUMMARY        	TEXT	, " +
				                   " CREATED        	TEXT	, " +
				                   " UPDATED        	INTEGER	, " +
				                   " DESCRIPTION    	TEXT	, " +
				                   " MODE        		TEXT	, " +
				                   " COMMENT_TO_ENTRY   TEXT	, " +
				                   " LATITUDE        	TEXT	, " +
				                   " LONGITUDE        	TEXT	, " +
				                   " AUTHOR        		TEXT) ");
					
					db.createTable("CREATE TABLE DBFEED " +
				                   "(ID INTEGER PRIMARY KEY     ," +
				                   " URL        		TEXT    , " +
				                   " AUTHOR        		TEXT    , " +
				                   " TITLE          	TEXT    NOT NULL, " + 
				                   " CREATED        	TEXT) ");
					
					db.createTable("CREATE TABLE DBUSER " +
			                   	   "(ID INTEGER PRIMARY KEY     ," +
			                       " NAME        		TEXT    , " +
			                       " FEEDLIST        	TEXT    , " +
			                       " UPDATED        	TEXT) ");
					
					db.createTable("CREATE TABLE DBNOTIF " +
			                   	   "(ID INTEGER PRIMARY KEY     , " +
			                       " FEEDID        		TEXT    , " +
			                       " USERID        		TEXT) ");
					
					db.createIndex("CREATE INDEX DATE_INDEX ON DBENTRY (UPDATED)");
					db.getOptions().setUserVersion(1);
					return null;
				}
			});
		}
		if (targetVersion < 2) {
			return;
		}
		if (targetVersion > 2) {
			throw new IllegalArgumentException("Unsupported version: " + targetVersion);
		}
		
		db.commit();
	}

	/**
	 * This function returns a cursor for all entries
	 * 
	 * @return ISqlJetCursor cursor for all entries
	 * @throws SqlJetException
	 */
	public ISqlJetCursor getAllItemsEntry() throws SqlJetException {
		return db.getTable("dbentry").open();
	}
	
	/**
	 * This function returns a cursor for all feeds
	 * 
	 * @return ISqlJetCursor cursor for all feeds
	 * @throws SqlJetException
	 */
	public ISqlJetCursor getAllItemsFeed() throws SqlJetException {
		return db.getTable("dbfeed").open();
	}
	
	/**
	 * This function returns a cursor for all notifications
	 * 
	 * @return ISqlJetCursor cursor for all notifications
	 * @throws SqlJetException
	 */
	public ISqlJetCursor getAllItemsNotif() throws SqlJetException {
		return db.getTable("dbnotif").open();
	}

	/**
	 * This function returns the entry corresponding of the id in parameters
	 * 
	 * @param id id of the entry to get
	 * @return DBEntry entry gotten
	 * @throws SqlJetException
	 */
	public DBEntry getDBEntry(final long id) throws SqlJetException {
		return (DBEntry) db.runReadTransaction(new ISqlJetTransaction() {
			
			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(id)) {
						DBEntry item = new DBEntry();
						item.read(cursor);
						return item;
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
	}
	
	/**
	 * This function returns the feed corresponding of the id in parameters
	 *  
	 * @param id id of the feed to get
	 * @return DBFeed feed gotten
	 * @throws SqlJetException
	 */
	public DBFeed getDBFeed(final long id) throws SqlJetException {
		return (DBFeed) db.runReadTransaction(new ISqlJetTransaction() {
			
			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbfeed").open();
				try {
					if (cursor.goTo(id)) {
						DBFeed item = new DBFeed();
						item.read(cursor);
						return item;
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
	}
	
	/**
	 * This function returns all entries of a specific feed with it id (id)
	 * @param id id of the feed
	 * @return List<DBEntry> list of entries
	 * @throws SqlJetException
	 */
	@SuppressWarnings("unchecked")
	public List<DBEntry> getDBEntries(final String id) throws SqlJetException{
		return (List<DBEntry>) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor dateCursor = db.getTable("dbentry").order("date_index");
				List<DBEntry> mList = new ArrayList<DBEntry>();
				try {
					 while (!dateCursor.eof()) {
						 DBEntry mDBEntry = new DBEntry();
						 mDBEntry.read(dateCursor);
						 if(id.equals(mDBEntry.getFeed_id()) && mDBEntry.getComment_to_entry()==null){
							 mList.add(mDBEntry);
						 }
						 dateCursor.next();
					 }
					 return mList;
				}finally {
					dateCursor.close();
				}
			}
		});
	}
	
	@SuppressWarnings("unchecked")
	public List<DBFeed> getDBFeed() throws SqlJetException{
		return (List<DBFeed>) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor dateCursor = db.getTable("dbfeed").open();
				List<DBFeed> mList = new ArrayList<DBFeed>();
				try {
					 while (!dateCursor.eof()) {
						 DBFeed mDBEntry = new DBFeed();
						 mDBEntry.read(dateCursor);
//						 if(id.equals(mDBEntry.getFeed_id()) && mDBEntry.getComment_to_entry()==null){
							 mList.add(mDBEntry);
//						 }
						 dateCursor.next();
					 }
					 return mList;
				}finally {
					dateCursor.close();
				}
			}
		});
	}
	
	/**
	 * This function returns all entries of a specific feed with it id "id"
	 * @param id id of the feed
	 * @return List<DBEntry> list of entries
	 * @throws SqlJetException
	 */
	@SuppressWarnings("unchecked")
	public List<DBEntry> getDBGeoUsers(final String id) throws SqlJetException{
		return (List<DBEntry>) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor dateCursor = db.getTable("dbentry").order("date_index");
				List<DBEntry> mList = new ArrayList<DBEntry>();
				try {
					 while (!dateCursor.eof()) {
						 DBEntry mDBEntry = new DBEntry();
						 mDBEntry.read(dateCursor);
						 if(id.equals(mDBEntry.getFeed_id()) && mDBEntry.getComment_to_entry()==null){
							 mList.add(mDBEntry);
						 }
						 dateCursor.next();
					 }
					 return mList;
				}finally {
					dateCursor.close();
				}
			}
		});
	}
	
	/**
	 * This function returns all comments of a specific entry with it id (entryId)
	 * @param id id of the feed
	 * @return List<DBEntry> list of entries
	 * @throws SqlJetException
	 */
	@SuppressWarnings("unchecked")
	public List<DBEntry> getDBComment(final String entryId) throws SqlJetException{
		System.out.println("getDBComment "+entryId);
		return (List<DBEntry>) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor dateCursor = db.getTable("dbentry").order("date_index");
				List<DBEntry> mList = new ArrayList<DBEntry>();
				try {
					 while (!dateCursor.eof()) {
						 DBEntry mDBEntry = new DBEntry();
						 mDBEntry.read(dateCursor);
						 System.out.println("mDBEntry.getComment_to_entry "+mDBEntry.getComment_to_entry());
						 System.out.println("mDBEntry.getComment_to_entry entryId "+entryId);
						 if(mDBEntry.getComment_to_entry()!=null){
							 if(mDBEntry.getComment_to_entry().equals(entryId)){
								 mList.add(mDBEntry);
								 System.out.println("add");
							 }
						 }
						 dateCursor.next();
					 }
					 return mList;
				}finally {
					dateCursor.close();
				}
			}
		});
	}

	/**
	 * <p>
	 * This function adds an entry in the database
	 * </p>
	 * @param item entry to add
	 * @return long id of the entry once it is added in the database
	 * @throws SqlJetException
	 */
	public long addItemEntry(final DBEntry item) throws SqlJetException {
		long mId;
		if(db.isInTransaction()){
			System.out.println("addItemEntry: close DB");
			db.commit();
		}
		mId = (Long) db.runWriteTransaction(new ISqlJetTransaction() {

			@SuppressWarnings("deprecation")
			public Object run(SqlJetDb db) throws SqlJetException {
				byte[] blobContent = null;
				byte[] blobName = null;
				byte[] blobAuthor = null;
				byte[] blobSummary = null;
				byte[] blobDescription = null;
//				byte[] blobUpdated = null;
				byte[] blob_id = null;

				if (item.getContent() != null) {
					try {
						blobContent = item.getContent().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if (item.getName() != null) {
					try {
						blobName = item.getName().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if (item.getSummary() != null) {
					try {
						blobSummary = item.getSummary().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if (item.getDescription() != null) {
					try {
						blobDescription = item.getDescription().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
//				if (item.getUpdated() != null) {
//					try {
//						blobUpdated = item.getUpdated().toGMTString().getBytes("UTF-8");
//					} catch (UnsupportedEncodingException e) {
//						throw new IllegalArgumentException(e);
//					}
//				}
				
				if (item.getAuthor() != null) {
					try {
						blobAuthor = item.getAuthor().getName().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if (item.get_id() != null) {
					try {
						blob_id = item.get_id().getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
//				if(!item.getAuthor().getName().equals("origin")){
//					Boolean mNotif = false;
//					for(int i=1;i<=10;i++){
//						if(String.valueOf(i).equals(item.getAuthor().getName())==false){
//							ISqlJetCursor cursorNotif = getAllItemsNotif();
//							try {
//								 while (!cursorNotif.eof()) {
//
//									 if(cursorNotif.getString("feedid").equals(item.getFeed_id()) && cursorNotif.getString("userid").equals(i)){
//										 mNotif = true;
//									 }
//									 cursorNotif.next();
//								 }
//							}finally {
//								cursorNotif.close();
//							}
//							if(mNotif==true){
//								db.getTable("dbnotif").insert(null,item.getFeed_id(),i);
//							}
//						}
//					}
//				}
				
				
				return db.getTable("dbentry").insert(null, blob_id, item.getFeed_id(), item.getComment_id(), item.getUrl(), blobName, item.getTitle(), blobContent, blobSummary, null, item.getUpdated().getTime(), blobDescription, null, item.getComment_to_entry(), String.valueOf(item.getLatitude()), String.valueOf(item.getLongitude()), blobAuthor);
			}
		});
		item.setId(mId);
		db.commit();
		return mId;
	}

	/**
	 * <p>
	 * This function adds a notification in the database
	 * </p>
	 * @param userId id of the user for the notification
	 * @param feedId if of the feed to notify
	 * @return long id of the notification once it is added in the database
	 */
	public long addItemNotif(final String userId, final String feedId){
		if(db.isInTransaction()){
			
			try {
				db.commit();
				System.out.println("addItemNotif: close DB");
			} catch (SqlJetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		long mId = 0;
		try {
			db.beginTransaction(SqlJetTransactionMode.WRITE);
			mId =  (Long) db.getTable("dbnotif").insert(null,feedId,userId);
			db.commit();
		} catch (SqlJetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mId;
	}
	
	/**
	 * <p>
	 * This function adds an empty feed in the database
	 * </p>
	 * @param userId id of the user who adds the fed
	 * @return long id of the feed once it is added in the database
	 * @throws SqlJetException
	 */
	public long addEmptyFeed(final String userId) throws SqlJetException {
		return (Long) db.runWriteTransaction(new ISqlJetTransaction() {
			@SuppressWarnings("deprecation")
			public Object run(SqlJetDb db){
				DBFeed mDBFeed = new DBFeed();
				mDBFeed.author = userId;
				mDBFeed.created = new Date().toGMTString();
				mDBFeed.title = "newFeed";
				byte[] blobDate = null;
				if(mDBFeed.url == null){
					mDBFeed.url = "/feedsync/rest/myfeeds/";
				}
				
				if (mDBFeed.created != null) {
					try {
						blobDate = mDBFeed.created.getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if(mDBFeed.author.isEmpty()){
					mDBFeed.author = "empty";
				}
				
				try {
					return db.getTable("dbfeed").insert(null,  mDBFeed.url, mDBFeed.author, mDBFeed.title, blobDate);
				} catch (SqlJetException e) {
					// TODO Auto-generated catch block
					System.out.println("ERROR creation Feed");
					e.printStackTrace();
					return null;
				}
			}
		});
	}
	
	/**
	 * <p>
	 * This function adds an empty feed in the database
	 * </p>
	 * @param userId id of the user who adds the fed
	 * @return long id of the feed once it is added in the database
	 * @throws SqlJetException
	 */
	public long addEmptyFeedWithTitle(final String userId, final String title) throws SqlJetException {
		return (Long) db.runWriteTransaction(new ISqlJetTransaction() {
			@SuppressWarnings("deprecation")
			public Object run(SqlJetDb db){
				DBFeed mDBFeed = new DBFeed();
				mDBFeed.author = userId;
				mDBFeed.created = new Date().toGMTString();
				mDBFeed.title = title;
				byte[] blobDate = null;
				if(mDBFeed.url == null){
					mDBFeed.url = "/feedsync/rest/myfeeds/";
				}
				
				if (mDBFeed.created != null) {
					try {
						blobDate = mDBFeed.created.getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if(mDBFeed.author.isEmpty()){
					mDBFeed.author = "empty";
				}
				
				try {
					return db.getTable("dbfeed").insert(null,  mDBFeed.url, mDBFeed.author, mDBFeed.title, blobDate);
				} catch (SqlJetException e) {
					// TODO Auto-generated catch block
					System.out.println("ERROR creation Feed");
					e.printStackTrace();
					return null;
				}
			}
		});
	}
	
	/**		
	 * <p>
	 * This function adds an feed in the database
	 * </p>
	 * @param item feed to add in the database
	 * @return long id of the feed once it is added in the database
	 * @throws SqlJetException
	 */
	public long addFeed(final DBFeed item) throws SqlJetException {
		return item.id = (Long) db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				byte[] blobDate = null;
				if(item.url == null){
					item.url = "/feedsync/rest/myfeeds/";
				}
				
				if (item.created != null) {
					try {
						blobDate = item.created.getBytes("UTF-8");
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
				
				if(item.title == null){
					item.title = "empty";
				}
				
				return db.getTable("dbfeed").insert(null,  item.url, item.author, item.title, blobDate);
			}
		});
	}
	
	/**
	 * This function update the url of each feed once it is added in the database
	 * @param article id of the feed
	 * @throws SqlJetException
	 */
	public void updateFeedURL(final long article) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();
		values.put("url", httpAddress+"/feedsync/rest/myfeeds/"+article+"/entries");
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbfeed").open();
				try {
					if (cursor.goTo(article)) {
						cursor.update(cursor.getRowId(),values.get("url"),cursor.getValue("author"),cursor.getValue("title"),cursor.getValue("created"));
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function updates the data of a feed with the feed received in parameter "item"  
	 * @param item feed updated
	 * @throws SqlJetException
	 */
	public void updateFeeditem(final DBFeed item) throws SqlJetException {

		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbfeed").open();
				try {
					if (cursor.goTo(item.id)) {
						if(item.author!=null){
							cursor.update(cursor.getRowId(),cursor.getValue("url"),item.author,item.title,item.created);
						}else{
							cursor.update(cursor.getRowId(),cursor.getValue("url"),cursor.getValue("author"),item.title,item.created);
						}
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function update the url of each entry once it is added in the database
	 * @param article id of the entry
	 * @throws SqlJetException
	 */
	public void updateEntryURL(final long article) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();

		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(article)) {
						values.put("url",  httpAddress+"/feedsync/rest/myfeeds/"+cursor.getValue("feed_id")+"/"+article);
						cursor.update(cursor.getRowId(),cursor.getValue("_id"),cursor.getValue("feed_id"),cursor.getValue("comment_id"),values.get("url"),cursor.getValue("name"),cursor.getValue("title"),cursor.getValue("content"),cursor.getValue("summary"),cursor.getValue("created"),cursor.getValue("updated"),cursor.getValue("description"),cursor.getValue("mode"),cursor.getValue("comment_to_entry"),cursor.getValue("latitude"),cursor.getValue("longitude"),cursor.getValue("author"));
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function update the url of each comment once it is added in the database
	 * @param article id of the comment
	 * @throws SqlJetException
	 */
	public void updateCommentURL(final long article) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();

		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(article)) {
						values.put("url",  httpAddress+"/feedsync/rest/myfeeds/"+cursor.getValue("feed_id")+"/"+cursor.getValue("comment_to_entry")+"/"+article);
						cursor.update(cursor.getRowId(),cursor.getValue("_id"),cursor.getValue("feed_id"),cursor.getValue("comment_id"),values.get("url"),cursor.getValue("name"),cursor.getValue("title"),cursor.getValue("content"),cursor.getValue("summary"),cursor.getValue("created"),cursor.getValue("updated"),cursor.getValue("description"),cursor.getValue("mode"),cursor.getValue("comment_to_entry"),cursor.getValue("latitude"),cursor.getValue("longitude"),cursor.getValue("author"));
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function updates the geo data of an entry with the entry received in parameter (entry)
	 * @param entry entry updated
	 * @throws SqlJetException
	 */
	@SuppressWarnings("deprecation")
	public void updateBFTEntry(final DBEntry entry) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();
		values.put("latitude", entry.getLatitude());
		values.put("longitude", entry.getLongitude());

		values.put("updated", entry.getUpdated().getTime());
		
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(entry.getId())) {
						cursor.update(cursor.getRowId(),cursor.getValue("_id"),cursor.getValue("feed_id"),cursor.getValue("comment_id"),cursor.getValue("url"),cursor.getValue("name"),cursor.getValue("title"),cursor.getValue("content"),cursor.getValue("summary"),cursor.getValue("created"),values.get("updated"),cursor.getValue("description"),cursor.getValue("mode"),cursor.getValue("comment_to_entry"),values.get("latitude"),values.get("longitude"),cursor.getValue("author"));
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function updates the data of an entry with the entry received in parameter (entry)
	 * @param entry entry updated
	 * @throws SqlJetException
	 */
	@SuppressWarnings("deprecation")
	public void updateEntry(final DBEntry entry) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();
		values.put("latitude", entry.getLatitude());
		values.put("longitude", entry.getLongitude());

		values.put("updated", entry.getUpdated().getTime());
		
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(entry.getId())) {
						cursor.update(cursor.getRowId(),cursor.getValue("_id"),cursor.getValue("feed_id"),cursor.getValue("comment_id"),cursor.getValue("url"),cursor.getValue("name"),entry.getTitle(),entry.getContent(),entry.getSummary(),cursor.getValue("created"),values.get("updated"),cursor.getValue("description"),cursor.getValue("mode"),cursor.getValue("comment_to_entry"),values.get("latitude"),values.get("longitude"),entry.getAuthor().getName());
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function updates the content data of an entry with the entry received in parameter (entry)
	 * @param entry entry updated
	 * @throws SqlJetException
	 */
	@SuppressWarnings("deprecation")
	public void updateContentEntry(final DBEntry entry) throws SqlJetException {
		final Map<String, Object> values = new HashMap<String, Object>();
		values.put("latitude", entry.getLatitude());
		values.put("longitude", entry.getLongitude());

		values.put("updated", entry.getUpdated().getTime());
		
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(entry.getId())) {
						cursor.update(cursor.getRowId(),cursor.getValue("_id"),cursor.getValue("feed_id"),cursor.getValue("comment_id"),cursor.getValue("url"),cursor.getValue("name"),cursor.getValue("title"),entry.getContent(),cursor.getValue("summary"),cursor.getValue("created"),values.get("updated"),cursor.getValue("description"),cursor.getValue("mode"),cursor.getValue("comment_to_entry"),values.get("latitude"),values.get("longitude"),cursor.getValue("author"));
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}

	/**
	 * This function removes the feed with the id (id) in the database
	 * @param id id of the feed to remove
	 * @throws SqlJetException
	 */
	public void removeFeed(final long id) throws SqlJetException {
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbfeed").open();
				try {
					if (cursor.goTo(id)) {
						cursor.delete();
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function removes the entry with the id (id) in the database
	 * @param id id of the entry to remove
	 * @throws SqlJetException
	 */
	public void removeEntry(final long id) throws SqlJetException {
		db.runWriteTransaction(new ISqlJetTransaction() {

			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbentry").open();
				try {
					if (cursor.goTo(id)) {
						cursor.delete();
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
	
	/**
	 * This function removes the notification with the id (id) in the database
	 * @param id id of the notification to remove
	 */
	public void removeNotifUser(final long id) {
		try {
			db.beginTransaction(SqlJetTransactionMode.WRITE);
			ISqlJetCursor cursor = getAllItemsNotif();
			try {
				if (cursor.goTo(id)) {
					cursor.delete();
				}
			}finally {
				cursor.close();
			}
			
			try {
				db.commit();
			} catch (SqlJetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SqlJetException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
	
	/**
	 * <p>
	 * This function checks if the database contains a notification for a specific feed with the id (feedId) and for the user (userId)</br>
	 * It returns a boolean, TRUE if it exits, FALSE if not
	 * </p>
	 * @param feedId id of the feed 
	 * @param userId id of the user
	 * @return boolean 
	 * @throws SqlJetException
	 */
	public Boolean containsNotif(final String feedId, final String userId) throws SqlJetException{
		Boolean mBool = false;
		
		if(db.isInTransaction()){
			System.out.println("containsNotif: close DB");
			db.commit();
		}
		
		mBool = (Boolean) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = getAllItemsNotif();
				try {
					 while (!cursor.eof()) {
						 if(cursor.getString("feedid").equals(feedId) && cursor.getString("userid").equals(userId)){
							 return true;
						 }
						 cursor.next();
					 }
					 return false;
				}finally {
					cursor.close();
				}
			}
		});
		db.commit();
		return mBool;
	}
	
	/**
	 * <p>
	 * This function checks if the database contains a notification for a specific feed with the id (feedId) and for the user (userId)
	 * </p>
	 * 
	 * @param feedId id of the feed 
	 * @param userId id of the user
	 * @return long id of the notification if it exists
	 * @throws SqlJetException
	 */
	public long containsNotifLong(final String feedId, final String userId) throws SqlJetException{
		long mLong = 0;
		mLong = (Long) db.runReadTransaction(new ISqlJetTransaction() {
			public Object run(final SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = getAllItemsNotif();
				try {
					 while (!cursor.eof()) {

						 if(cursor.getString("feedid").equals(feedId) && cursor.getString("userid").equals(userId)){
							 return cursor.getRowId();
						 }
						 cursor.next();
					 }
					 return false;
				}finally {
					cursor.close();
				}
			}
		});

		System.out.println("mLong "+mLong);
		db.commit();
		return mLong;
	}
	
	/**
	 * This function removes the notification with the id "id" in the database
	 * 
	 * @param id id of the notification to remove
	 * @throws SqlJetException
	 */
	public void removeNotif(final long id) throws SqlJetException {
		if(db.isInTransaction()){
			System.out.println("removeNotif: close DB");
			db.commit();
		}
		db.runWriteTransaction(new ISqlJetTransaction() {
			public Object run(SqlJetDb db) throws SqlJetException {
				ISqlJetCursor cursor = db.getTable("dbnotif").open();
				try {
					if (cursor.goTo(id)) {
						cursor.delete();
					}
				} finally {
					cursor.close();
				}
				return null;
			}
		});
		db.commit();
	}
}