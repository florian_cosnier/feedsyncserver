package com.thales.feedsyncserver.db;

import java.io.UnsupportedEncodingException;
import java.util.Date;

import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Person;
import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

import com.thales.feedsyncserver.abdera.AbderaSupport;

/**
 * <p>
 * This class provides an object corresponding of the Atom Entry with several variables </br>
 * It has some getters and setters to read and write the data </br>
 * It provides different functions to manipulate entry objects with the database
 * </p>
 * 
 * @author Florian COSNIER
 */
public class DBEntry {
	public DBEntry(){
		
	}

	public DBEntry(String _id, int id, String feed_id, String url, Date updated, Date created, String title,
			String content, String summary, String name, String text) {
		super();
		this.id = id;
		this.feed_id = feed_id;
		this.comment_id = feed_id;
		this.url = url;
		this.updated = updated;
		this.created = created;
		this.title = title;
		this.content = content;
		this.summary = summary;
		this.name = name;
		this.text = text;
	}

	private String _id;
	private long id;
	private String feed_id;
	private String comment_id;
	private String comment_to_entry;
	private String url;
	private Date updated;
	private Date created;
	private String title;
	private String content;
	private String summary;
	private String name;
	private String text;
	private String mode;
	private String description;
	private Person author;
	private double latitude;
	private double longitude;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getUpdated() {
		return updated;
	}

	public void setUpdated(Date updated) {
		this.updated = updated;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String toString(){
		return(new String("id:"+id+"updated_at:"+updated+"title:"+title));
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Person getAuthor() {
		return author;
	}

	public void setAuthor(Person author) {
		this.author = author;
	}
	
	public String getFeed_id() {
		return feed_id;
	}

	public void setFeed_id(String feed_id) {
		this.feed_id = feed_id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public String getComment_to_entry() {
		return comment_to_entry;
	}

	public void setComment_to_entry(String comment_to_entry) {
		this.comment_to_entry = comment_to_entry;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getComment_id() {
		return comment_id;
	}

	public void setComment_id(String comment_id) {
		this.comment_id = comment_id;
	}
	
	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	@SuppressWarnings("deprecation")
	public void read(ISqlJetCursor cursor) throws SqlJetException {
		this.id = cursor.getInteger("id");
		this.title = cursor.getString("title");
		this.url = cursor.getString("url");
		this.feed_id = cursor.getString("feed_id");
		this.comment_id = cursor.getString("comment_id");
		this.comment_to_entry = cursor.getString("comment_to_entry");
		
		if(cursor.getString("latitude")!=null){
			this.latitude = Double.parseDouble(cursor.getString("latitude"));
		}
		
		if(cursor.getString("longitude")!=null){
			this.longitude = Double.parseDouble(cursor.getString("longitude"));
		}
		
		byte[] blobContent = cursor.getBlobAsArray("content");
		if (blobContent != null) {
			try {
				this.content = new String(blobContent, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.content = null;
		}
		
		byte[] blobSummary = cursor.getBlobAsArray("summary");
		if (blobSummary != null) {
			try {
				this.summary = new String(blobSummary, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.summary = null;
		}
		
		byte[] blobDescription = cursor.getBlobAsArray("description");
		if (blobDescription != null) {
			try {
				this.description = new String(blobDescription, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.description = null;
		}
		
		byte[] blobName = cursor.getBlobAsArray("name");
		if (blobName != null) {
			try {
				this.name = new String(blobName, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.name = null;
		}
		
		long blobUpdated = cursor.getInteger("updated");
		this.updated = new Date(blobUpdated);

		byte[] blobAuthor = cursor.getBlobAsArray("author");
		if (blobAuthor != null) {
			try {
				Entry newEntry = AbderaSupport.getAbdera().newEntry();
				newEntry.addAuthor(new String(blobAuthor, "UTF-8"));
				this.author = newEntry.getAuthor();
				
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.author = null;
		}
		
		byte[] blob_id = cursor.getBlobAsArray("_id");
		if (blob_id != null) {
			try {
				this._id = new String(blob_id, "UTF-8");
				
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this._id = null;
		}
	}
}
