package com.thales.feedsyncserver.db;

import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

/**
 * <p>
 * This class provides an object corresponding of the notification with several variables </br>
 * It has some getters and setters to read and write the data </br>
 * It provides different functions to manipulate notifications with the database
 * </p>
 * 
 * @author Florian COSNIER
 */
public class DBNotif {
	
	private long id;
	private String feedId;
	private String userId;
	
	public DBNotif(){
		
	}

	public DBNotif(int id, String feedId, String userId) {
		super();
		this.id = id;
		this.feedId = feedId;
		this.userId = userId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFeedId() {
		return feedId;
	}

	public void setFeedId(String feedId) {
		this.feedId = feedId;
	}
	
	public String getFeedList(){
		return userId;
	}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	
	public String toString(){
		return(new String("id:"+id+"userId:"+userId+"feedId:"+feedId));
	}
	
	public void read(ISqlJetCursor cursor) throws SqlJetException {
		this.id = cursor.getInteger("id");
		this.feedId = cursor.getString("feedId");
		this.userId = cursor.getString("userId");
	}

}
