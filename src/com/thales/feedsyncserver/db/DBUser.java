package com.thales.feedsyncserver.db;

import java.io.UnsupportedEncodingException;

import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

/**
 * <p>
 * This class provides an object corresponding of the User with several variables </br>
 * It has some getters and setters to read and write the data </br>
 * It provides different functions to manipulate users with the database </br>
 * </p>
 * 
 * @author Florian COSNIER
 */
public class DBUser {
	
	private long id;
	private String name;
	private String feedList;
	private String updated;
	
	public DBUser(){
		
	}

	public DBUser(int id, String name, String feedList, String updated) {
		super();
		this.id = id;
		this.name = name;
		this.feedList = feedList;
		this.updated = updated;
	}
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getFeedList(){
		return feedList;
	}
	
	public void setFeedList(String feedList){
		this.feedList = feedList;
	}
	
	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}
	
	public String toString(){
		return(new String("id:"+id+"name:"+name));
	}
	
	public void read(ISqlJetCursor cursor) throws SqlJetException {
		this.id = cursor.getInteger("id");
		this.name = cursor.getString("name");
	
		byte[] blobDate = cursor.getBlobAsArray("updated");
		if (blobDate != null) {
			try {
				this.updated = new String(blobDate, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.updated = null;
		}
	}

}
