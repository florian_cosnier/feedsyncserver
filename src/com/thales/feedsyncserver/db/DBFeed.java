package com.thales.feedsyncserver.db;

import java.io.UnsupportedEncodingException;

import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

/**
 * <p>
 * This class provides an object corresponding of the Atom Feed with several variables </br>
 * It has some getters and setters to read and write the data </br>
 * It provides different functions to manipulate feed objects with the database
 * </p>
 * 
 * @author Florian COSNIER
 */
public class DBFeed {
	
	public DBFeed(){
			
	}

	public DBFeed(int id, String url, String author, String created, String title) {
		super();
		this.id = id;
		this.url = url;
		this.created = created;
		this.title = title;
		this.author = author;
	}
	
	public long id;
	public String url;
	public String author;
	public String created;
	public String title;
	public int nbEntries = 0;
	
	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String toString(){
		return(new String("id:"+id+"created_at:"+created+"title:"+title+"author:"+author));
	}

	public String getCreated() {
		return created;
	}

	public void setCreated(String created) {
		this.created = created;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	
	public void read(ISqlJetCursor cursor) throws SqlJetException {
		this.id = cursor.getInteger("id");
		this.title = cursor.getString("title");
		this.url = cursor.getString("url");
		this.author = cursor.getString("author");
		
		byte[] blobDate = cursor.getBlobAsArray("created");
		if (blobDate != null) {
			try {
				this.created = new String(blobDate, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.created = null;
		}
		/*byte[] blobText = cursor.getBlobAsArray("text");
		if (blobText != null) {
			try {
				this.text = new String(blobText, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new IllegalArgumentException(e);
			}
		} else {
			this.text = null;
		}
		String dateUpdated = this.updated.toGMTString();
		dateUpdated = cursor.getString("updated");
		String dateCreated = this.created.toGMTString();*/
	}

}
