package com.thales.feedsyncserver.api;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.abdera.model.Document;
import org.apache.abdera.model.Element;
import org.apache.abdera.model.Entry;
import org.apache.abdera.model.Feed;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.io.input.XmlStreamReader;
import org.tmatesoft.sqljet.core.SqlJetException;

import com.thales.feedsyncserver.abdera.AbderaSupport;
import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.resources.EntryResource;
import com.thales.feedsyncserver.sync.SyncManager;

/**
 * <p>
 * This class provides a <b>REST API</b>
 * </p>
 * 
 * @author Florian COSNIER
 *
 */
@Produces("application/atom+xml")
@Consumes("application/atom+xml")
@Path("/myfeeds")
public class ContentAPI {
	
	@Context
	private UriInfo uriInfo;
	public InetAddress IP;
	public String httpAddress;
	public ExecutorService pool;
	public ExecutorCompletionService<Response> threadService;
	public static int HTTP_POST_ENTRY = 0;
	public static int HTTP_POST_FEED = 1;
	public static int HTTP_POST_COMMENT = 2;
	public static int HTTP_PUT_ENTRY = 3;
	public static int HTTP_PUT_FEED = 4;
	public static int HTTP_PUT_COMMENT = 5;
	public static int HTTP_GET_ENTRIES = 6;
	public static int HTTP_GET_ENTRY = 7;
	public static int HTTP_GET_COMMENTS = 8;
	public static int HTTP_GET_COMMENT = 9;
	public static int HTTP_DELETE_USER = 10;
	public static int HTTP_DELETE_FEED = 11;
	public static int HTTP_DELETE_ENTRY = 12;
	public static int HTTP_DELETE_COMMENT = 13;
	public static int HTTP_NOTIFY_USER = 14;
	public static int HTTP_POST_COMMENT_MEDIA = 15;
	public static int HTTP_POST_ENTRY_MEDIA = 16;
	public static int HTTP_GET_UPDATED_CONTENT = 17;
	
	private SyncManager mSyncManager;
	private String corsHeaders;
	boolean debug = true; //TRUE to show the logs

	public ContentAPI() throws SqlJetException, SocketException {
		IP = null;
		try {
			IP = InetAddress.getLocalHost();
			httpAddress = "http://" + IP.getHostAddress() + ":8083";
			
			Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                    NetworkInterface ni = eni.nextElement();
                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                    while(inetAddresses.hasMoreElements()) {
                            InetAddress ia = inetAddresses.nextElement();
                            if(!ia.isLinkLocalAddress()) {
                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
                                    if(ni.getName().contains("wlan")){
                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
                                    }
                            }
                    }
            }
			
		} catch (UnknownHostException e1) {
			e1.printStackTrace();
		}
		
		//Initialize the sync manager
		mSyncManager = new SyncManager();

		//Launch the Executors and the ExecutorCompletionService with a pool
		//They execute sequentially the different requests received throw the API
		pool = Executors.newSingleThreadExecutor();
		threadService = new ExecutorCompletionService<Response>(pool);
	}

	private Response.ResponseBuilder makeCORS(Response.ResponseBuilder responseBuilder, String returnMethod) {
		
		Response.ResponseBuilder rb = responseBuilder.header("Access-Control-Allow-Origin", null);
		String reqHead = returnMethod;
		
		if ((reqHead != null) && (!reqHead.equals(""))) {
			rb.header("Access-Control-Allow-Headers", returnMethod);
		}
		
		return rb;
	}

	/**
	 * This function creates a new Feed </br>
	 * The user gets the address then the API calls the SyncManager for the Feed creation
	 * 
	 * @return Response with status and if it is ok, the Feed and its id
	 * @throws SqlJetException
	 * @throws URISyntaxException
	 */
	@POST
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/addFeed")
	public Response createFeed(String mFeed) throws SqlJetException, URISyntaxException {
		System.out.println("createFeed()");

//		String title = uriInfo.getQueryParameters().getFirst("title");
//		System.out.println("title " + uriInfo.getQueryParameters().isEmpty());
//		String userId = uriInfo.getQueryParameters().getFirst("userId");
//		System.out.println("userId " + userId);
		
		Feed newFeed = AbderaSupport.getAbdera().newFeed();
		try {
			InputStream stream = new ByteArrayInputStream(mFeed.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newFeed = (Feed) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		

		return mSyncManager.createFeed("Admin", newFeed.getTitle());
	}

	@OPTIONS
	@Path("/{feedId}/entries")
	public Response getEntriesOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "GET").build();
	}

	/**
	 * <p>
	 * This function gets the entries from the feed with the id (feedId) </br>
	 * The user gets the address then the API calls the SyncManager to get the list of entries corresponding of the feed id (feedId) </br>
	 * It gets the entries throw a callable
	 * </p>
	 * 
	 * @param feedId id of the feed to get entries
	 * @return Response with the status and if it is OK, it returns the feed
	 * @throws SqlJetException
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@GET
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/entries")
	public Response getEntries(@PathParam("feedId") String feedId)
			throws SqlJetException, InterruptedException, ExecutionException {
		System.out.println("getEntries()");
		String userId = uriInfo.getQueryParameters().getFirst("userId");
		String nbEntry = uriInfo.getQueryParameters().getFirst("nbEntry");
		String nbSecondes = uriInfo.getQueryParameters().getFirst("nbSec");
		
		if (debug) {
			System.out.println("userId " + userId);
			System.out.println("nbEntry " + nbEntry);
		}
		
		//In the case where the user doesn't specify the number of entries 
		//The number of entries is negative so the server will response with the totality of the feed
		if ((nbEntry == null) || (nbEntry.isEmpty())) {
			nbEntry = "-1";
		}
		
		if ((nbSecondes == null) || (nbSecondes.isEmpty())) {
			nbSecondes = "0";
		}

		threadService.submit(new EntryCallable(HTTP_GET_ENTRIES,userId, feedId, null, null, nbEntry, Integer.parseInt(nbSecondes)));
		Future<Response> future = threadService.take();
		Response result = future.get();
		if (result == null) {
			result = Response.status(400).build();
		}
		
		//the pool is shutdown once everything is done
		pool.shutdown();

		//We return the result of the GET
		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}")
	public Response getEntryOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "GET").build();
	}

	/**
	 * <p>
	 * This function gets the entry with the id (entryId) from the feed with the id (feedId) </br>
	 * The user gets the address then the API calls the SyncManager to get the entry with the id (entryId) corresponding of the feed id (feedId)</br>
	 * It gets one entry throw a callable
	 * </p>
	 * 
	 * @param feedId id of the feed to get one entry
	 * @param entryId id of the entry gotten
	 * @return Response with the status and if it is OK, it returns the entry
	 * @throws SqlJetException
	 */
	@GET
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/{entryId}")
	public Response getEntry(@PathParam("feedId") String feedId,@PathParam("entryId") String entryId) throws SqlJetException {
		System.out.println("getEntry()");
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		//The threadService is launched 
		//It submits a new callable, to get one entry
		Response result = null;
		try {
			threadService.submit(new EntryCallable(HTTP_GET_ENTRY,userId, feedId, entryId, null, "1", 0));
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		pool.shutdown();

		//We return the result of the GET
		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/comments")
	public Response getCommentsOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "GET").build();
	}

	/**
	 * <p>
	 * This function gets the comments linked to the entry with the id (entryId) </br>
	 * The user gets the address then the API calls the SyncManager to get the list of comments corresponding of the entry id (entryId) </br>
	 * It gets the comments throw a callable
	 * </p>
	 * 
	 * @param feedId id of the feed to get entries
	 * @param entryId id of the entry in which the comments are linked
	 * @return Response with the status and if it is OK, it returns the comments
	 * @throws SqlJetException
	 */
	@GET
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/{entryId}/comments")
	public Response getComments(@PathParam("feedId") String feedId,@PathParam("entryId") String entryId) throws SqlJetException {
		System.out.println("getComments()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");
		String nbEntry = uriInfo.getQueryParameters().getFirst("nbEntry");
		String nbSecondes = uriInfo.getQueryParameters().getFirst("nbSec");
		
		if (debug) {
			System.out.println("userId " + userId);
			System.out.println("nbEntry " + nbEntry);
		}
		
		//In the case where the user doesn't specify the number of entries 
		//The number of entries is negative so the server will response with the totality of the feed
		if((nbEntry==null) || (nbEntry.isEmpty())){
			nbEntry = "-1";
		}
		
		if ((nbSecondes == null) || (nbSecondes.isEmpty())) {
			nbSecondes = "0";
		}

		threadService.submit(new EntryCallable(HTTP_GET_COMMENTS,userId, feedId, entryId, null, nbEntry, Integer.parseInt(nbSecondes)));
		Response result = null;
		Future<Response> future;
		try {
			future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		if (result == null) {
			result = Response.status(400).build();
		}
		
		//the pool is shutdown once everything is done
		pool.shutdown();

		//We return the result of the GET
		return result;
	}

	@OPTIONS
	@Path("/{feedId}/editFeed")
	public Response putFeedOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "PUT").build();
	}

	/**
	 * <p>
	 * This function edits the feed with the id (feedId) </br>
	 * The user puts on the address a feed as parameter, then the API calls the SyncManager to edit the feed</br>
	 * At first, it gets the feed passed in argument and parses it. Then it sends throw the SyncManager the feed to edit.
	 * </p>
	 * 
	 * @param mFeed the feed
	 * @param feedId id of the feed to edit
	 * @return Response with the status and if it is OK, it returns the feed updated
	 */
	@PUT
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/editFeed")
	public Response putFeed(String mFeed, @PathParam("feedId") String feedId) {
		System.out.println("putFeed()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");
		System.out.println("userId " + userId);

		Feed newFeed = AbderaSupport.getAbdera().newFeed();
		try {
			InputStream stream = new ByteArrayInputStream(mFeed.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newFeed = (Feed) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		return mSyncManager.putFeed(userId, feedId, newFeed);
	}

	@OPTIONS
	@Path("/{feedId}/addEntry")
	public Response postEntryOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "POST").build();
	}

	/**
	 * <p>
	 * This function adds an entry in a specific feed (feedId)</br>
	 * The user posts an entry in parameter to the address.</br>
	 * At first, the entry is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry entry to post
	 * @param feedId id of the feed where the entry will be posted
	 * @return Response with the status and if it is 201 CREATED, it returns the entry added with its id
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@POST
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/addEntry")
	public Response postEntry(String mEntry, @PathParam("feedId") String feedId)throws URISyntaxException, SqlJetException, HttpException,IOException, InterruptedException, ExecutionException {
		System.out.println("postEntry()");
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_POST_ENTRY,userId, feedId, "", newEntry, "", 0));
		Future<Response> future = threadService.take();
		Response result = future.get();
		if (result == null) {
			result = Response.status(400).build();
		}

		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/media/{feedId}/addMedia")
	public Response postMediaEntryOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "POST").build();
	}

	/**
	 * <p>
	 * This function adds a media entry in a specific feed (feedId)</br>
	 * The user posts a media entry in parameter to the address. It can be an image, a video, a song for example.</br>
	 * At first, the entry is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry media entry to post
	 * @param feedId id of the feed where the media entry will be posted
	 * @return Response with the status and if it is 201 CREATED, it returns the entry added with its id and the link of the media
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@POST
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/media/{feedId}/addMedia")
	public Response postMediaEntry(String mEntry,@PathParam("feedId") String feedId) throws URISyntaxException,SqlJetException, HttpException, IOException, InterruptedException,ExecutionException {
		System.out.println("postMediaEntry()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_POST_ENTRY_MEDIA, userId, feedId, "", newEntry, "", 0));
		Future<Response> future = threadService.take();
		Response result = future.get();
		if (result == null) {
			result = Response.status(400).build();
		}

		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/addComment")
	public Response postCommentOptions(
			@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		return rb.header("Access-Control-Allow-Methods", "POST").build();
	}

	/**
	 * <p>
	 * This function adds a comment linked to a specific entry (entryId)</br>
	 * The user posts a comment in parameter to the address.</br>
	 * At first, the comment is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry comment to post
	 * @param feedId id of the feed where the comment will be posted
	 * @param entryId id of the entry in which the comment is linked
	 * @return Response with the status and if it is 201 CREATED, it returns the comment added with its id
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@POST
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/{entryId}/addComment")
	public Response postComment(String mEntry,@PathParam("feedId") String feedId,@PathParam("entryId") String entryId) throws URISyntaxException,SqlJetException, HttpException, IOException, InterruptedException,ExecutionException {
		System.out.println("postComments()");
		
		String userId =  uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_POST_COMMENT,userId, feedId, entryId, newEntry, "", 0));
		Future<Response> future = threadService.take();
		Response result = future.get();
		if (result == null) {
			result = Response.status(400).build();
		}

		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/media/{feedId}/{entryId}/addMedia")
	public Response postMediaCommentOptions(
			@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		return rb.header("Access-Control-Allow-Methods", "POST").build();
	}

	/**
	 * <p>
	 * This function adds a media comment linked to an entry (entryId)</br>
	 * The user posts a media comment in parameter to the address.</br>
	 * At first, the entry is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry media comment to post
	 * @param feedId id of the feed where the media comment will be posted
	 * @param entryId id of the entry in which the media comment is linked
	 * @return Response with the status and if it is 201 CREATED, it returns the entry added with its id
	 * @throws URISyntaxException 
	 * @throws SqlJetException 
	 * @throws IOException 
	 * @throws HttpException 
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	@POST
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/media/{feedId}/{entryId}/addMedia")
	public Response postMediaComment(String mEntry,@PathParam("feedId") String feedId,@PathParam("entryId") String entryId) throws URISyntaxException,SqlJetException, HttpException, IOException, InterruptedException,ExecutionException {
		System.out.println("postMediaComments()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			Element el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_POST_COMMENT_MEDIA, userId, feedId, entryId, newEntry, "",0));
		Future<Response> future = threadService.take();
		Response result = future.get();
		if (result == null) {
			result = Response.status(400).build();
		}
		
		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/editEntry")
	public Response putEntryOptions(@HeaderParam("Access-Control-Allow-Headers") String requestH,@HeaderParam("Content-Type") String test) {
		System.out.println("requestH " + requestH);
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);
		
		return rb.header("Access-Control-Allow-Methods", "PUT").build();
	}

	/**
	 * <p>
	 * This function edits an entry in a specific feed (feedId)</br>
	 * The user puts an entry in parameter to the address.</br>
	 * At first, the entry is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry the entry edited
	 * @param feedId id of the feed where the entry to edit is
	 * @param entryId id of the entry to edit
	 * @return Response with the status and if it is 200 OK, it returns the entry updated 
	 */
	@PUT
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/{entryId}/editEntry")
	public Response putEntry(String mEntry, @PathParam("feedId") String feedId,@PathParam("entryId") String entryId) {
		System.out.println("putEntry()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		Element el;
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		threadService.submit(new EntryCallable(HTTP_PUT_ENTRY,userId, feedId, entryId, newEntry, "", 0));

		Response result = null;
		try {
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_NOTIFY_USER,userId, feedId, "", newEntry, "", 0));
		try {
			Future<Response> future1 = threadService.take();
			result = future1.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/{commentId}/editComment")
	public Response putCommentOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);

		return rb.header("Access-Control-Allow-Methods", "PUT").build();
	}

	/**
	 * <p>
	 * This function edits a comment with the id (commentId)  linked to an entry (entryId)</br>
	 * The user puts an entry in parameter to the address.</br>
	 * At first, the entry is deocoded and parsed to the object org.apache.abdera.model.Entry then passed in arguments to the SyncManager
	 * </p>
	 * 
	 * @param mEntry the comment edited
	 * @param feedId id of the feed
	 * @param entryId id of the entry 
	 * @param commentId id of the comment to edit
	 * @return Response with the status and if it is 200 OK, it returns the entry updated 
	 */
	@PUT
	@Produces(MediaType.APPLICATION_ATOM_XML)
	@Path("/{feedId}/{entryId}/{commentId}/editComment")
	public Response putComment(String mEntry,@PathParam("feedId") String feedId,@PathParam("entryId") String entryId,@PathParam("commentId") String commentId) {
		System.out.println("putComment()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		Entry newEntry = AbderaSupport.getAbdera().newEntry();
		Element el;
		try {
			InputStream stream = new ByteArrayInputStream(mEntry.getBytes("UTF-8"));
			XmlStreamReader mXmlStreamReader = new XmlStreamReader(stream);
			Document<Element> doc = AbderaSupport.getAbdera().getParser().parse(mXmlStreamReader);
			el = doc.getRoot();
			newEntry = (Entry) el;
		} catch (IOException e) {
			System.out.println("Error Encoding Atom Document");
			e.printStackTrace();
		}
		
		threadService.submit(new EntryCallable(HTTP_PUT_COMMENT,userId, feedId, commentId, newEntry, "", 0));

		Response result = null;
		try {
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		
		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/deleteFeed")
	public Response deleteFeedOptions(@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);

		return rb.header("Access-Control-Allow-Methods", "DELETE").build();
	}

	/**
	 * <p>
	 * This function deletes the feed with the id (feedId)</br>
	 * The user gets the address then the API calls the SyncManager to delete the feed with the id (feedId) corresponding of the feed id (feedId)
	 * </p>
	 * 
	 * @param feedId id of the feed to delete
	 * @return Response with the status and if it is OK, it returns an OK Response
	 * @throws URISyntaxException
	 * @throws IOException
	 * @throws SqlJetException
	 */
	@DELETE
	@Path("/{feedId}/deleteFeed")
	@Consumes("application/atom+xml")
	public Response deleteFeed(@PathParam("feedId") String feedId)throws URISyntaxException, IOException, SqlJetException {
		System.out.println("deleteFeed()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		threadService.submit(new EntryCallable(HTTP_DELETE_FEED,userId, feedId, "", null, "", 0));

		Response result = null;
		try {
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/deleteEntry")
	public Response deleteEntryOptions(
			@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);

		return rb.header("Access-Control-Allow-Methods", "DELETE").build();
	}

	/**
	 * <p>
	 * This function deletes the entry with the id (entryId) </br>
	 * The user gets the address then the API calls the SyncManager to delete the entry with the id (entryId) corresponding of the entry id (entryId)
	 * </p>
	 * 
	 * @param entryId id of the entry to delete
	 * @return Response with the status and if it is OK, it returns an OK Response
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	@DELETE
	@Path("/{feedId}/{entryId}/deleteEntry")
	@Consumes("application/atom+xml")
	public Response deleteEntry(@PathParam("entryId") String entryId)throws NumberFormatException, SqlJetException {
		System.out.println("deleteEntry()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		threadService.submit(new EntryCallable(HTTP_DELETE_ENTRY,userId, "", entryId, null, "", 0));

		Response result = null;
		try {
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		pool.shutdown();

		return result;
	}

	@OPTIONS
	@Path("/{feedId}/{entryId}/{commentId}/deleteComment")
	public Response deleteCommentOptions(
			@HeaderParam("Access-Control-Request-Headers") String requestH) {
		Response.ResponseBuilder rb = null;
		rb = makeCORS(Response.ok(), requestH);

		return rb.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "DELETE").build();
	}

	/**
	 * <p>
	 * This function deletes the comment with the id (commentId) </br>
	 * The user gets the address then the API calls the SyncManager to delete the comment with the id (commentId)
	 * </p>
	 * 
	 * @param commentId id of the comment to delete
	 * @return Response with the status and if it is OK, it returns an OK Response
	 * @throws NumberFormatException
	 * @throws SqlJetException
	 */
	@DELETE
	@Path("/{feedId}/{entryId}/{commentId}/deleteComment")
	@Consumes("application/atom+xml")
	public Response deleteComment(@PathParam("commentId") String commentId)throws NumberFormatException, SqlJetException {
		System.out.println("commentId()");
		
		String userId = uriInfo.getQueryParameters().getFirst("userId");

		threadService.submit(new EntryCallable(HTTP_DELETE_COMMENT,userId, "", commentId, null, "", 0));

		Response result = null;
		try {
			Future<Response> future = threadService.take();
			result = future.get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		pool.shutdown();

		return result;
	}

	public class EntryCallable implements Callable<Response> {
		
		private int type;
		private String userId;
		private String feedId;
		private String entryId;
		private Entry entry;
		private String nbEntry;
		private int nbSec;
		
		public static final int HTTP_POST_ENTRY = 0;
		public static final int HTTP_POST_FEED = 1;
		public static final int HTTP_POST_COMMENT = 2;
		public static final int HTTP_PUT_ENTRY = 3;
		public static final int HTTP_PUT_FEED = 4;
		public static final int HTTP_PUT_COMMENT = 5;
		public static final int HTTP_GET_ENTRIES = 6;
		public static final int HTTP_GET_ENTRY = 7;
		public static final int HTTP_GET_COMMENTS = 8;
		public static final int HTTP_GET_COMMENT = 9;
		public static final int HTTP_DELETE_USER = 10;
		public static final int HTTP_DELETE_FEED = 11;
		public static final int HTTP_DELETE_ENTRY = 12;
		public static final int HTTP_DELETE_COMMENT = 13;
		public static final int HTTP_NOTIFY_USER = 14;
		public static final int HTTP_POST_COMMENT_MEDIA = 15;
		public static final int HTTP_POST_ENTRY_MEDIA = 16;
		public static final int HTTP_GET_UPDATED_CONTENT = 17;

		public EntryCallable(int type,String userId, String feedId, String entryId, Entry newEntry,String nbEntry, int nbSec) {
			this.type = type;
			this.userId = userId;
			this.feedId = feedId;
			this.entryId = entryId;
			this.entry = newEntry;
			this.nbEntry = nbEntry;
			this.nbSec = nbSec;
		}

		public Response call() {
			try {
				return processCommand();
			} catch (HttpException e) {
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} catch (SqlJetException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		}

		private Response processCommand() throws HttpException,URISyntaxException, SqlJetException, IOException {
			//Depends on the HTTP Request, it calls the SyncManager to do a specific action
			switch (type) {
				case HTTP_GET_ENTRIES: //entryId here is the number of seconds 
					return mSyncManager.getEntries(userId, feedId,Integer.parseInt(nbEntry), nbSec);
				case  HTTP_GET_ENTRY:
					return mSyncManager.getEntry(userId, feedId,entryId);
				case HTTP_GET_COMMENTS:
					return mSyncManager.getComments(userId, feedId,entryId, Integer.parseInt(nbEntry),nbSec);
				case HTTP_GET_UPDATED_CONTENT:
					return mSyncManager.getUpdatedContent(userId, feedId,Integer.parseInt(nbEntry));
				case HTTP_POST_FEED:
					break;
				case HTTP_POST_ENTRY:
					return mSyncManager.postEntry(userId, feedId,entry);
				case HTTP_POST_COMMENT:
					return mSyncManager.postComment(userId, feedId,entryId, entry);
				case HTTP_POST_COMMENT_MEDIA:
					return mSyncManager.postMediaComment(userId, feedId,entryId, entry);
				case HTTP_POST_ENTRY_MEDIA:
					return mSyncManager.postMediaEntry(userId, feedId,entry);
				case HTTP_PUT_FEED:
					break;
				case HTTP_PUT_ENTRY:
					return mSyncManager.putEntry(userId, feedId,entryId, entry);
				case HTTP_PUT_COMMENT:
					return mSyncManager.putComment(userId, feedId,entryId, entry);
				case HTTP_DELETE_FEED:
					return mSyncManager.deleteFeed(userId, feedId);
				case HTTP_DELETE_ENTRY:
					return mSyncManager.deleteEntry(userId, entryId);
				case HTTP_DELETE_COMMENT:
					return mSyncManager.deleteComment(userId, entryId);
				default:
					return null;
				}
			return null;
		}
	}

}