package com.thales.feedsyncserver.gui;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Map;

import org.apache.abdera.ext.geo.GeoHelper;
import org.apache.abdera.ext.geo.Point;
import org.apache.abdera.model.Category;
import org.apache.abdera.model.Entry;
import org.apache.abdera.protocol.client.AbderaClient;
import org.tmatesoft.sqljet.core.SqlJetException;
import org.tmatesoft.sqljet.core.table.ISqlJetCursor;

import com.thales.feedsyncserver.abdera.AbderaSupport;
import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.db.DBEntry;
import com.thales.feedsyncserver.db.DBFeed;

/**
 * <p>
 * It provides different functions to show Feeds, Entries in a Web Apps. </br>
 * Each function has some HTML codes to show the data and some Javascript functions to add dynamic view
 * </p>
 * 
 * @author Florian COSNIER
 *
 */
public class GuiResponder {
	
	/*** Date variables ***/
	public static final long MINUTE=60;
	public static final long HOUR=3600;
	public static final long DAY=24*HOUR;
	public static final long WEEK=7*DAY;
	
	public InetAddress IP;
	public String httpAddress;
	
	/**
	 * <p>
	 * Constructor </br>
	 * Initialize the IP address of the server and the database
	 * </p>
	 */
	public GuiResponder(){
		
		IP = null;
		try {
			IP = InetAddress.getLocalHost();
			httpAddress = "http://"+IP.getHostAddress()+":8083";
			
			Enumeration<NetworkInterface> eni = NetworkInterface.getNetworkInterfaces();

            while (eni.hasMoreElements()) {
                    NetworkInterface ni = eni.nextElement();
                    Enumeration<InetAddress> inetAddresses =  ni.getInetAddresses();


                    while(inetAddresses.hasMoreElements()) {
                            InetAddress ia = inetAddresses.nextElement();
                            if(!ia.isLinkLocalAddress()) {
                                    System.out.println("Interface: " + ni.getName() + "   IP: " + ia.getHostAddress());
                                    if(ni.getName().contains("wlan")){
                                    	httpAddress = "http://" + ia.getHostAddress() + ":8083";
                                    }
                            }
                    }
            }
			
		} catch (UnknownHostException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * <p>
	 * This function shows feeds in a HTML web page in a tab </br>
	 * To show in a tab, it calls the function showTabFeeds calling the data from the database
	 * </p>
	 * 
	 * @param buffer web page with a header
	 * @throws SqlJetException
	 */
	public void showFeeds(StringBuffer buffer) throws SqlJetException {
		buffer.append(
				  "<link href='../css/bootstrap.css' rel='stylesheet'>"
				+ "<div id='header'>"
					+ "<div class='navbar navbar-inverse navbar-fixed-top' role='navigation'>"
						+ "<div class='container'>"
							+ "<div class='navbar-header'>"
								+ "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>"
									+ "<span class='sr-only'>Toggle navigation</span><span class='icon-bar'></span>"
									+ "<span class='icon-bar'></span>"
									+ "<span class='icon-bar'></span>"
								+ "</button>"
								+ "<a class='navbar-brand' href='../'>Home</a>"
								+ "<a class='navbar-brand' href='#'>Administration</a>"
								+ "<a class='navbar-brand' href='#'>DB</a>"
							+ "</div>"
						+ "</div>"
					+"</div>"
				+ "</div>");
		DB db = new DB();
		try {
			db.beginTransaction(false);
			try {
				ISqlJetCursor cursor;
				cursor = db.getAllItemsFeed();
				
				try {
					showTabFeeds(db, buffer, cursor);
				} finally {
					cursor.close();
				}
			} finally {
				db.commit();
			}
		} finally {
			db.close();
		}
	}
	
	/**
	 * <p>
	 * This function shows entries in a HTML web page in a tab </br>
	 * To show in a tab, it calls the function showTabEntries calling the data from the database </br>
	 * The param id corresponds to the feed id, the entries are linked to this feed
	 * </p>
	 * 
	 * @param buffer web page with a header
	 * @param id id of the feed
	 * @throws SqlJetException
	 */
	public void showEntries(StringBuffer buffer, String id) throws SqlJetException {
		buffer.append(
			"<html><head><title>Admin Management</title>"
				+ "<link href='../../css/bootstrap.css' rel='stylesheet'>"
				+ "<link href='../../carousel.css' rel='stylesheet'></head>"
				+ "<div class='navbar-wrapper'>"
					+ "<div class='container'>"
						+ "<div class='navbar navbar-inverse navbar-static-top' role='navigation'>"
							+ "<div class='container'>"
								+ "<div class='navbar-header'>"
									+ "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>"
										+ "<span class='sr-only'>Toggle navigation</span>"
										+ "<span class='icon-bar'></span>"
										+ "<span class='icon-bar'></span>"
										+ "<span class='icon-bar'></span>"
									+ "</button>"
									+ "<a class='navbar-brand' href='#'><img src='../../img/thaleslogo.png' style='margin-right:5px;'></a>"
								+ "</div>"
								+ "<div class='navbar-collapse collapse'>"
									+ "<ul class='nav navbar-nav'>"
										+ "<li><a href='../'>Home</a></li>"
										+ "<li><a href='../dragAndDrop.html'>Users/Group</a></li>"
										+ "<li><a href='../about.html'>About</a></li>"
										+ "<li><a href='../contact.html'>Contact</a></li>"
										+ "<li class='dropdown'>"
											+ "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Users/Group <b class='caret'></b></a>"
//											+ "<ul class='nav navbar-nav'>"
//												+ "<li><a href='#'>Action</a></li>"
//												+ "<li><a href='#'>Another action</a></li>"
//												+ "<li><a href='#'>Something else here</a></li>"
//												+ "<li class='divider'></li>"
//												+ "<li class='dropdown-header'>Nav header</li>"
//												+ "<li><a href='#'>Separated link</a></li>"
//												+ "<li><a href='#'>One more separated link</a></li>"
//											+ "</ul>"
										+ "</li>"
										+ "<li class='dropdown active' >"
											+ "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Content <b class='caret'></b></a>"
											+ "<ul class='dropdown-menu'>"
												+ "<li><a href='#'><span class='glyphicon glyphicon-envelope' style='padding-right : 20px;'></span><span class='badge pull-right'>3</span>SMS</a></li>"
												+ "<li><a href='3'><span class='glyphicon glyphicon-user' style='padding-right : 20px;'></span>Group Messaging</a></li>"
												+ "<li><a href='1'><span class='glyphicon glyphicon-star' style='padding-right : 20px;'></span>Interventions</a></li>"
												+ "<li><a href='2'><span class='glyphicon glyphicon-warning-sign' style='padding-right : 20px;'></span>Interventions Management</a></li>"
												+ "<li><a href='4'><span class='glyphicon glyphicon-map-marker' style='padding-right : 20px;'></span>Tracking</a></li>"
											+ "</ul>"
										+ "</li>"
//	                					+ "<form class='navbar-form navbar-right' role='form' style='margin-left: 250px;'>"
//			            					+ "<div class='form-group' style='padding-right: 4px;'>"
//			              						+ "<input type='text' placeholder='Login' class='form-control'>"
//			            					+ "</div>"
//			            					+ "<div class='form-group' style='padding-right: 4px;''>"
//			              						+ "<input type='password' placeholder='Password' class='form-control'>"
//			            					+ "</div>"
//			            					+ "<!--  <button type='submit' class='btn btn-success' href='/rest/admin'>Sign in</button> -->"
//			            					+ "<a role='button' class='btn btn-success' href='#'>Sign in</a>"
//		          						+ "</form>"
              						+ "</ul>"
              					+ "</div>"
          					+ "</div>"
      					+ "</div>"
  					+ "</div>"
				+ "</div>");
		
		DB db = new DB();
		try {
			db.beginTransaction(false);
			try {
				ISqlJetCursor cursor;
				cursor = db.getAllItemsEntry();
				try {
					showTabEntries(db, buffer, id, cursor);
				} finally {
					cursor.close();
				}
			} finally {
				db.commit();
			}
		} finally {
			db.close();
		}
		
		buffer.append("</body></html>");
	}
	
	/**
	 * <p>
	 * This function shows comments in a HTML web page in a tab </br>
	 * To show in a tab, it calls the function showTabEntries calling the data from the database </br>
	 * The param id corresponds to the entry id, the comments are linked to this feed
	 * </p>
	 * 
	 * @param buffer web page with a header
	 * @param id id of the feed
	 * @throws SqlJetException
	 */
	public void showComments(StringBuffer buffer, String entryId) throws SqlJetException {
		buffer.append(
			"<html><head><title>Admin Management</title>"
				+ "<link href='../../../css/bootstrap.css' rel='stylesheet'>"
				+ "<link href='../../../carousel.css' rel='stylesheet'></head>"
				+ "<div class='navbar-wrapper'>"
					+ "<div class='container'>"
						+ "<div class='navbar navbar-inverse navbar-static-top' role='navigation'>"
							+ "<div class='container'>"
								+ "<div class='navbar-header'>"
									+ "<button type='button' class='navbar-toggle' data-toggle='collapse' data-target='.navbar-collapse'>"
										+ "<span class='sr-only'>Toggle navigation</span>"
										+ "<span class='icon-bar'></span>"
										+ "<span class='icon-bar'></span>"
										+ "<span class='icon-bar'></span>"
									+ "</button>"
									+ "<a class='navbar-brand' href='#'><img src='../../../img/thaleslogo.png' style='margin-right:5px;'></a>"
								+ "</div>"
								+ "<div class='navbar-collapse collapse'>"
									+ "<ul class='nav navbar-nav'>"
										+ "<li><a href='../'>Home</a></li>"
										+ "<li><a href='../dragAndDrop.html'>Users/Group</a></li>"
										+ "<li><a href='../about.html'>About</a></li>"
										+ "<li><a href='../contact.html'>Contact</a></li>"
										+ "<li class='dropdown'>"
											+ "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Users/Group <b class='caret'></b></a>"
//											+ "<ul class='nav navbar-nav'>"
//												+ "<li><a href='#'>Action</a></li>"
//												+ "<li><a href='#'>Another action</a></li>"
//												+ "<li><a href='#'>Something else here</a></li>"
//												+ "<li class='divider'></li>"
//												+ "<li class='dropdown-header'>Nav header</li>"
//												+ "<li><a href='#'>Separated link</a></li>"
//												+ "<li><a href='#'>One more separated link</a></li>"
//											+ "</ul>"
										+ "</li>"
										+ "<li class='dropdown active' >"
											+ "<a href='#' class='dropdown-toggle' data-toggle='dropdown'>Content <b class='caret'></b></a>"
											+ "<ul class='dropdown-menu'>"
												+ "<li><a href='#'><span class='glyphicon glyphicon-envelope' style='padding-right : 20px;'></span><span class='badge pull-right'>3</span>SMS</a></li>"
												+ "<li><a href='3'><span class='glyphicon glyphicon-user' style='padding-right : 20px;'></span>Group Messaging</a></li>"
												+ "<li><a href='1'><span class='glyphicon glyphicon-star' style='padding-right : 20px;'></span>Interventions</a></li>"
												+ "<li><a href='2'><span class='glyphicon glyphicon-warning-sign' style='padding-right : 20px;'></span>Interventions Management</a></li>"
												+ "<li><a href='4'><span class='glyphicon glyphicon-map-marker' style='padding-right : 20px;'></span>Tracking</a></li>"
											+ "</ul>"
										+ "</li>"
//	                					+ "<form class='navbar-form navbar-right' role='form' style='margin-left: 250px;'>"
//			            					+ "<div class='form-group' style='padding-right: 4px;'>"
//			              						+ "<input type='text' placeholder='Login' class='form-control'>"
//			            					+ "</div>"
//			            					+ "<div class='form-group' style='padding-right: 4px;''>"
//			              						+ "<input type='password' placeholder='Password' class='form-control'>"
//			            					+ "</div>"
//			            					+ "<!--  <button type='submit' class='btn btn-success' href='/rest/admin'>Sign in</button> -->"
//			            					+ "<a role='button' class='btn btn-success' href='#'>Sign in</a>"
//		          						+ "</form>"
              						+ "</ul>"
              					+ "</div>"
          					+ "</div>"
      					+ "</div>"
  					+ "</div>"
				+ "</div>");
		
		DB db = new DB();
		try {
			db.beginTransaction(false);
			try {
				ISqlJetCursor cursor;
				cursor = db.getAllItemsEntry();
				try {
					showTabComments(db, buffer, entryId, cursor);
				} finally {
					cursor.close();
				}
			} finally {
				db.commit();
			}
		} finally {
			db.close();
		}
		
		buffer.append("</body></html>");
	}
	
	/**
	 * <p>
	 * This function shows feeds in a tab </br>
	 * The data come from the database directly
	 * </p>
	 * 
	 * @param db database 
	 * @param buffer web page
	 * @param cursor cursor to navigate in the database
	 * @throws SqlJetException
	 */
	private void showTabFeeds(DB db, StringBuffer buffer, ISqlJetCursor cursor) throws SqlJetException {
		buffer.append("<div class='container'><h2 style='margin-top: 60px;'>Feed List Database</h2><table class='table table-striped' style='margin-top: 10px;'><tr><th>ID</th><th>Title</th><th>Url</th><th>Created</th>");
		int extraColumns = 0;
		buffer.append("<th colspan='2'>Action</th><tr>");
		DBFeed item = new DBFeed();
		while (!cursor.eof()) {
			item.read(cursor);
			//if(!item.title.contains("Comment")){
				buffer.append("<tr>");
				printValue(buffer, item.id);
				printValue(buffer, "<a href='/feedsync/rest/webapps/"+item.id+"'>"+item.title+"</a>");
				printValue(buffer, item.url);
				printValue(buffer, item.created);
				buffer.append("<td><a type='button' class='btn btn-primary' href='/feedsync/rest/webapps/editFeed?id=");
				buffer.append(item.id);
				buffer.append("'>Edit</a></td>");
				buffer.append("<td><a type='button' class='btn btn-danger'  href='/feedsync/rest/webapps/removeFeed?id=");
				buffer.append(item.id);
				buffer.append("' onclick='return confirm(");
				buffer.append("&#x27 Are you sure ?&#x27");
				buffer.append(");");
				buffer.append("'>Remove</a></td>");
				buffer.append("</tr>");
			cursor.next();
		}
		buffer.append("<tr><td colspan='");
		buffer.append(5 + extraColumns);
		buffer.append("'></td><td colspan='2'><a type='button' class='btn btn-success' href='/feedsync/rest/webapps/addFeed'>Add Feed</a></td></tr>");
		buffer.append("<tr><form><td colspan='");
		buffer.append(5 + extraColumns);
		buffer.append("</table></div>  <footer><p>&copy; Thales Communications & Security 2013-2014</p></footer>");
	}
	
	/**
	 * <p>
	 * This function shows Comments in a tab </br>
	 * The data come from the database directly </br>
	 * The param id corresponds to the entry id, the comments are linked to this entry
	 * </p>
	 * 
	 * @param db database
	 * @param buffer web page
	 * @param id id of the feed
	 * @param cursor cursor to navigate in the database
	 * @throws SqlJetException
	 */
	private void showTabComments(DB db, StringBuffer buffer, String id, ISqlJetCursor cursor) throws SqlJetException {
		int extraColumns = 0;
		int type = 0;
		ISqlJetCursor cursorBFT = cursor;
		
		DBFeed mFeed = new DBFeed();
		DBFeed mFeedBFT = new DBFeed();
		DB mdb = new DB();
		mFeed = mdb.getDBFeed(Long.parseLong(id));
		String idBFT = "4";
		mFeedBFT = mdb.getDBFeed(Long.parseLong("4"));
		
		if(mFeed.title.contains("News")){
			type = 1;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>Interventions Comments</h2>"
	        	+ "</div>"
        	+ "</div>");
		}else if(mFeed.title.contains("OT")){
			type = 4;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>Operations Tracking Comments</h2>"
	        	+ "</div>"
		    + "</div>");
		}else if(mFeed.title.contains("GroupMessaging")){
			type = 3;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>GroupMessaging Comments</h2>"
	        	+ "</div>"
        	+ "</div>");
		}else if(mFeed.title.contains("Alert")){
			type = 2;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
			        + "<h2>Interventions Comments</h2>"
			      + "</div>"
		    + "</div>");
		}
		
		buffer.append(
		  "<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=true&libraries=drawing'></script>"
		+ "<script>"
			+ "var coordinates;"
			+ "var myCoordinates;"
			+ "var drawingManager;"
			+ "var objCheckbox;"
			+ "var onclickCBGeofence = 0;"
			+ "var onclickCBGeolocalized = 0;"
			+ "var formGeofence;"
			+ "var formGeolocalized;"
			+ "var tempLocation;"
			+ "var map;"
			+ "var markers = [];"
			+ "var allPolygones = [];"
			+ "var infowindow;"
			+ "var latitude;"
			+ "var longitude;"
			+ "var content;"
			+ "var showHide = 0;"
			+ "var showEdit = 0;"
			+ "var showValidate = 0;"
			+ "var editDraggable = 0;"
			+ "var editPutId;"
			+ "var refreshScript;"
			+ "function showDrawingManagerGeofence() {"
				+ "if (onclickCBGeofence == 0){"
					+ "console.log('onclickCBGeofence');"
					+ "onclickCBGeofence = 1;"
				+ "}else if (onclickCBGeofence == 1){"
					+ "console.log('onclickCBGeofence');"
					+ "onclickCBGeofence = 0;"
				+ "}"	
				+ "showDrawingManager();"
	        + "}"
	        + "function showDrawingManagerGeolocalized() {"
		        + "if (onclickCBGeolocalized == 0){"
					+ "console.log('onclickCBGeolocalized');"
					+ "onclickCBGeolocalized = 1;"
				+ "}else if (onclickCBGeolocalized == 1){"
					+ "console.log('onclickCBGeolocalized');"
					+ "onclickCBGeolocalized = 0;"
				+ "}"
				+ "showDrawingManager();"
			+ "}"
			+ "function showDrawingManager() {"
				+ "if (onclickCBGeofence == 0 && onclickCBGeolocalized == 0){"
					+ "drawingManager.setOptions({drawingControl: false});"
				+ "}else {"
					+ "drawingManager.setOptions({drawingControl: true});"
				+ "}"		
			+ "}"
			+ "function changeFieldGeofence(coordinates) {"
				+ "content = coordinates;"	
				+ "console.log(coordinates);"
			+ "}"
			+ "function changeFieldGeolocalizedLat(tempLocation) {"
				+ "formGeolocalized = tempLocation;"	
				+ "console.log(tempLocation);"
			    + "document.getElementById('inputLat3').value=formGeolocalized;"
		    + "}"
		    + "function changeFieldGeolocalizedLng(tempLocation) {"
				+ "formGeolocalized = tempLocation;"	
				+ "console.log(tempLocation);"
			    + "document.getElementById('inputLng3').value=formGeolocalized;"
		    + "}"
		    + "function postDataForm() {"
			    + "var title = document.getElementById('inputTitle3').value;"
			    + "var author = document.getElementById('inputAuthor3').value;"
			    + "var summary = document.getElementById('inputSummary3').value;"
			    + "if(content == null){"
			    	+ "content=1.0;"
			    + "}"
			    + "if(showValidate == 0){"
					+ "location.href ='/feedsync/rest/webapps/"+id+"/addEntry?title='+title+'&author='+author+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude;"
				+ "}else if(showValidate == 1){"
					+ "showValidate = 0;"
					+ "location.href ='/feedsync/rest/webapps/"+id+"/editEntry?title='+title+'&author='+author+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude+'&id='+editPutId;"
			    + "}"
			+ "}"			    
			+ "function resetmDB() {"
				+ "location.href ='/feedsync/rest/webapps/2/reset';"
			+ "}"
		    + "function editDataForm(editId,editTitle,editAuthor,editSummary,editContent,editLatitude,editLongitude) {"
		    	+ "showEdit = 1;"
		    	+ "editPutId = editId;"
			    + "console.log(editId,editTitle,editAuthor,editSummary);"
			    + "if (infowindow) infowindow.close();"
			    + "content = editContent;"
			    + "latitude = editLatitude;"
			    + "longitude = editLongitude;"
			    + "if(showValidate == 0){"
			    	+ "document.getElementById('addItem').innerHTML = 'Edit Item';"
			    	+ "document.getElementById('inputTitle3').value = editTitle;"
				    + "document.getElementById('inputAuthor3').value = editAuthor;"
				    + "document.getElementById('inputSummary3').value= editSummary;"
			    	+ "showValidate = 1;"
		    	+ "}else if(showValidate == 1){"
		    		+ "showValidate = 0;"
			    	+ "var titleEdit = document.getElementById('inputTitle3').value;"
			    	+ "var authorEdit = document.getElementById('inputAuthor3').value;"
			    	+ "var summary = document.getElementById('inputSummary3').value;"
			    	+ "console.log(titleEdit);"
			    	+ "document.getElementById('addItem').innerHTML = 'Add Item';"
			    	+ "location.href ='/feedsync/rest/webapps/"+id+ "/editEntry?title='+titleEdit+'&author='+authorEdit+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude+'&id='+editId;"
			    + "}"
		    + "}"
		    + "function addPoylygon(polyCoordinates) {"
		      + "myCoordinates = polyCoordinates.split('///');"
		      + "var allLatLng = [];"
		      + "for(var i=myCoordinates.length;i>1;i-=2){"
	      		+ "var myLatlng = new google.maps.LatLng(myCoordinates[i-2],myCoordinates[i-1]);"
	      		+ "allLatLng.push(myLatlng);"
			  + "}"
			  + "allPolygones.push(createPolygones(allLatLng));"
			+ "}"
			+ "function addMarker(latLocation,lngLocation,name) {"
				+ "var myLatlng = new google.maps.LatLng(latLocation,lngLocation);"
				+ "var contentString = name;"
				+ "var myName = name;"
				+ "console.log(myName);"
				+ "markers.push(createMarker(myName,myLatlng));"
			+ "}"
			+ "function showAll() {"
				+ "if(showHide == 0){"
					+ "showHide = 1;"
					+ "document.getElementById('showHide').innerHTML = 'Hide markers';"
					+ "showPoly();"
					+ "showMarkers();"
				+ "}else if(showHide == 1){"
					+ "showHide = 0;"
					+ "document.getElementById('showHide').innerHTML = 'Show markers';"
					+ "hideAll();"
				+ "}"
			+ "}"
		    + "function hideAll() {"
		  		+ "setAllPolyMap(null);"
		  		+ "setAllMarkersMap(null);"
			+ "}"	
			+ "function showPoly() {"
			  	+ "setAllPolyMap(map);"
			+ "}"
			+ "function showMarkers() {"
	  		  	+ "setAllMarkersMap(map);"
		    + "}"
			+ "function setAllMarkersMap(map) {"
			  	+ "for (var i = 0; i < markers.length; i++) {"
			      	+ "markers[i].setMap(map);"
		      	+ "}"
			+ "}"
			+ "function setAllPolyMap(map) {"
			  	+ "for (var i = 0; i < allPolygones.length; i++) {"
			      	+ "allPolygones[i].setMap(map);"
		      	+ "}"
			+ "}"
			+ "function createPolygones(latlng) {"
				+ "console.log(latlng);"
				+ "var myPolygon = new google.maps.Polygon({map: map,paths: latlng});"
				+ "google.maps.event.addListener(myPolygon, 'click', function(event) {"
					+ "if(showEdit == 1){"
						+ "console.log(editDraggable);"
						+ "if(editDraggable == 0){"
							+ "this.setDraggable(true);"
							+ "this.setEditable(true);"
							+ "editDraggable = 1;"
						+ "}"
						+ "else if(editDraggable == 1){"
							+ "this.setDraggable(false);"
							+ "this.setEditable(false);"
							+ "editDraggable = 0;"
					    	+ "var polygonBounds = this.getPath();"
							+ "coordinates = [];"
							+ "for(var i = 0 ; i < polygonBounds.length ; i++){"
					    		+ "coordinates.push(polygonBounds.getAt(i).lat(),polygonBounds.getAt(i).lng());"
				    		+ "}"
				    		+ "changeFieldGeofence(coordinates.join('///'));"
						+ "}"
					+ "}"
	    		+ "});"
		    	+ "return myPolygon;"
		    + "}"  
			+ "function createMarker(name, latlng) {"
				+ "var image;"
				+ "console.log(name.indexOf('Fire'));"
		    	+ "if(name.indexOf('Fire') != -1){"
		    		+ "image = '../../img/fire.png';"
		    	+ "}else if(name.indexOf('Bomb') != -1 ){"
		    		+ "image = '../../img/bomb.png';"
	    		+ "}else if(name.indexOf('Flood') != -1 ){"
		    		+ "image = '../../img/flood.png';"
	    		+ "}else if(name.indexOf('Radiation') != -1 ){"
		    		+ "image = '../../img/radiation.png';"
	    		+ "}else if(name.indexOf('Treedown') != -1 ){"
		    		+ "image = '../../img/treedown.png';"
		    	+ "}else if(name.indexOf('Group') != -1 ){"
	    			+ "image = '../../img/group.png';"
    			+ "}else if(name.indexOf('Offline') != -1 ){"
	    			+ "image = '../../img/male-red.png';"
    			+ "}else if(name.indexOf('Available') != -1 ){"
	    			+ "image = '../../img/male-2.png';"
    			+ "}else if(name.indexOf('Missing') != -1 ){"
	    			+ "image = '../../img/male-yellow.png';"
    			+ "}else if(name.indexOf('tetra') != -1 ){"
	    			+ "image = '../../img/flaticon.png';"
    			+ "}else{"
	    			+ "image = '../../img/male-2.png';"
	    		+ "}"
			    + "var myMarker = new google.maps.Marker({position: latlng, map: map,icon : image, draggable:true});"
			    + "google.maps.event.addListener(myMarker, 'click', function() {"
			      + "if (infowindow) infowindow.close();"
			      + "infowindow = new google.maps.InfoWindow({content: name});"
			      + "infowindow.open(map, myMarker);"
			    + "});"
			    + "google.maps.event.addListener(myMarker, 'dragend', function() {"
					+ "if(showEdit == 1){"
						+ "var markerBounds = myMarker.getPosition();"
						+ "latitude = markerBounds.lat();"
						+ "longitude = markerBounds.lng();"
					+ "}"
				+ "});"
			    + "return myMarker;"
		    + "}"  
			+ "function initialize(){"
			    + "map = new google.maps.Map(document.getElementById('map'), {zoom: 14, center: new google.maps.LatLng(48.941361,2.30689)});"
			    + "drawingManager = new google.maps.drawing.DrawingManager("
			    + "{drawingMode: null,drawingControlOptions: {drawingModes: ["
	    		+ "google.maps.drawing.OverlayType.MARKER,google.maps.drawing.OverlayType.POLYGON]}});"
	    		+ "drawingManager.setMap(map);"
	    		+ "drawingManager.setOptions({drawingControl: true});"
			    + "polygons = [];"
				+ "google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function(polygon) {"
			    	+ "polygons.push(polygon);"
			    	+ "console.log(polygon.getPath().getArray());"
			    	+ "var polygonBounds = polygon.getPath();"
					+ "coordinates = [];"
					+ "for(var i = 0 ; i < polygonBounds.length ; i++){"
			    		+ "coordinates.push(polygonBounds.getAt(i).lat(),polygonBounds.getAt(i).lng());"
		    		+ "}"
		    		+ "changeFieldGeofence(coordinates.join('///'));"
				+ "});"
				+ "google.maps.event.addDomListener(drawingManager, 'markercomplete', function(marker) {"
			    	+ "console.log(marker.getPosition());"
			    	+ "var markerBounds = marker.getPosition();"
		    		+ "latitude = markerBounds.lat();"
		    		+ "longitude = markerBounds.lng();"
	    		+ "});"
	    		+ "google.maps.event.addListenerOnce(map, 'tilesloaded', showAll);"	
			+ "}"
				+ "google.maps.event.addDomListener(window, 'load', initialize);"
				+ "function refreshTable() {"
					+ "setInterval(function () {"
						+ "var pathtopage = window.location.href;"
						+ "var myDiv = document.getElementById('rowTab');"
						+ "var arr = myDiv.getElementsByTagName('script');"
						+ "refreshScript = arr;"
						+ "$('#rowTab').load(pathtopage + ' #rowTab');"
					+ "}, 2000);"
				+ "}"
				+ "function refreshMap() {"
				+ "setInterval(function () {"
					+ "console.log('refreshMap');"
					+ "console.log(refreshScript);"
				    + "eval(refreshScript[0].innerHTML);" 
				+ "}, 2000);"
			+ "}"
		+ "</script>");
		
		buffer.append(
		"<div class='container'>"
			+ "<div class='row'>"
				+ "<div class='col-md-2' style='height:40px;'>"
					+ "<button type='button' class='btn btn-info' id='showHide' onclick='showAll()' style='margin-bottom:20px;'>Show markers</button>"
				+ "</div>"
			+ "</div>"
			+ "<div class='row'>"
			    + "<div class='col-md-8'>"
					+ "<div style='width:900px; height:680px;' id='map'></div>"
				+ "</div>"
				+ "<div class='col-md-4'>"
					+ "<div class='row-fluid' style='margin-top: 50px;'>"
						+ "<div class='col-md-12' style='margin-left:30%;'>"
							+ "<form role='post' class='form-horizontal'>");
							if(type==1){
								buffer.append("<div class='form-group'>"
									+ "<label for='inputTitle3' class='col-sm-4 control-label'>Title</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' class='form-control' id='inputTitle3' placeholder='Title'>"
									+ "</div>"
								+ "</div>");
							}
							if(type==2 || type==1){
									buffer.append(
								  "<div class='form-group'>"
									+ "<label for='inputAuthor3' class='col-sm-4 control-label'>Author</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' value='PMR' class='form-control' id='inputAuthor3' placeholder='Author'>"
									+ "</div>"
								+ "</div>"
								+ "<div class='form-group'>"
									+ "<label for='inputSummary3' class='col-sm-4 control-label'>Summary</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' value='Alert incident' class='form-control' id='inputSummary3' placeholder='Summary'>"
									+ "</div>"
								+ "</div>"
								);}
							    buffer.append("<div class='form-group'>");
									//+ "<a href='/feedsync/rest/admin/"+id+ "/add_item'><input type='submit' id='button' class='btn btn-success' value='Add item'></input></a>"
									//+ "<a role='button' class='btn btn-success' href='/feedsync/rest/admin/"+id+ "/add_item?title='>Add Item</a>"
							    if(type == 2 || type==1){
							    	buffer.append("<button type='button' class='btn btn-success' id ='addItem' style='margin-left:200px' onclick='postDataForm()'>Add Item</button>");
							    	//buffer.append("<button type='button' class='btn btn-success' onclick='resetmDB'>Reset DB</button>");
							    }
							    buffer.append("</div>"
							+ "</form>"
						+ "</div>"
					+ "</div>"
				+ "</div>"
		+ "</div>");
		buffer.append(""
				+ "<style type='text/css'>"
				+ "td { text-align:center;}"
				+ "th { text-align:center;}"
				+ "</style>"
				+ "<div class='row' id='rowTab' style='margin-top:20px;'>"
					+ "<table class='table table-striped table-hover table-condensed' id='tabDB'>"
					+ "<tr>"
						+ "<th>ID</th><th>Title</th><th>Author</th><th>Created</th>");
		buffer.append("<th colspan='2'>Action</th><tr>");
		DBEntry item = new DBEntry();
		while (!cursor.eof()) {
			item.read(cursor);
			if(id.equals(item.getComment_to_entry())){
				buffer.append("<tr>");
				printValue(buffer, item.getId());
				printValue(buffer, "<a href='/feedsync/rest/webapps/"+item.getFeed_id()+"/"+item.getId()+ "'>"+item.getTitle()+ "</a>");
				printValue(buffer, item.getAuthor().getName());
				printValue(buffer, item.getUpdated());
				String mTmp = "<h3>"+item.getTitle()+ "</h3>"
								+ "<div><p>"+item.getContent()+ "</p></div>"
										+ "<div>"
										//+ "<button type=\"button\" class=\"btn btn-primary\" id=\"showEdit\" onclick=\"editDataForm("+item.id+ ",\\'"+item.title+ "\\',\\'"+item.author.getName()+ "\\',\\'"+item.summary+ "\\',\\'"+item.content+ "\\',\\'"+item.latitude+ "\\',\\'"+item.longitude+ "\\')\">Edit</button>"
										//+ "<a role=\"button\" class=\"btn btn-primary\" href=\"/feedsync/rest/admin/"+id+ "/edit_item?id="+item.id+ "\">Edit</a>"
										+ "<a role=\"button\"  style=\"margin-left:10px;\" class=\"btn btn-danger\" href=\"/feedsync/rest/webapps/"+id+ "/removeEntry?id="+item.getId()+ "\" onclick=\"return confirm(&#x27 Are you sure ?&#x27);\">Remove</a><div>"
										
								+ "</div>";
				buffer.append("<script type='text/javascript'>"
						+ "addMarker("+item.getLatitude()+ ","+item.getLongitude()+ ",'"+mTmp+ "');");
//						if(item.getContent()==null){
//							
//						}else{
//							buffer.append("addPoylygon('"+item.getContent()+ "');");
//						}
						
						buffer.append("</script>");
				
				
				buffer.append("<td><a role='button' class='btn btn-primary' href='/feedsync/rest/webapps/"+id+ "/editComment?id=");
				buffer.append(item.getId());
				buffer.append("'>Edit</a></td>");
				buffer.append("<td><a role='button' class='btn btn-danger' href='/feedsync/rest/webapps/"+id+ "/removeComment?id=");
				buffer.append(item.getId());
				buffer.append("' onclick='return confirm(");
				buffer.append("&#x27 Are you sure ?&#x27");
				buffer.append(");");
				buffer.append("'>Remove</a></td>");
				buffer.append("</tr>");
			}
			cursor.next();
		}
		buffer.append("</table></div>");
		printFooterComments(buffer);
	}
	
	/**
	 * <p>
	 * This function shows entris in a tab </br>
	 * The data come from the database directly </br>
	 * The param id corresponds to the feed id, the entries are linked to this feed
	 * </p>
	 * 
	 * @param db database
	 * @param buffer web page
	 * @param id id of the entry
	 * @param cursor cursor to navigate in the database
	 * @throws SqlJetException
	 */
	private void showTabEntries(DB db, StringBuffer buffer, String id, ISqlJetCursor cursor) throws SqlJetException {
		int extraColumns = 0;
		int type = 0;
		ISqlJetCursor cursorBFT = cursor;
		
		DBFeed mFeed = new DBFeed();
		DBFeed mFeedBFT = new DBFeed();
		DB mdb = new DB();
		mFeed = mdb.getDBFeed(Long.parseLong(id));
		String idBFT = "4";
		mFeedBFT = mdb.getDBFeed(Long.parseLong("4"));
		
		if(mFeed.title.contains("News")){
			type = 1;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>Interventions</h2>"
	        	+ "</div>"
        	+ "</div>");
		}else if(mFeed.title.contains("OT")){
			type = 4;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>Operations Tracking</h2>"
	        	+ "</div>"
		    + "</div>");
		}else if(mFeed.title.contains("GroupMessaging")){
			type = 3;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
		        	+ "<h2>GroupMessaging</h2>"
	        	+ "</div>"
        	+ "</div>");
		}else if(mFeed.title.contains("Alert")){
			type = 2;
			buffer.append(
			  "<div class='jumbotron' style='padding-top:20px; margin:5px'>"
				+ "<div class='container' style='position:relative; top:45px;'>"
			        + "<h2>Interventions Management</h2>"
			      + "</div>"
		    + "</div>");
		}
		
		buffer.append(
		  "<script type='text/javascript' src='http://maps.google.com/maps/api/js?sensor=true&libraries=drawing'></script>"
		+ "<script>"
			+ "var coordinates;"
			+ "var myCoordinates;"
			+ "var drawingManager;"
			+ "var objCheckbox;"
			+ "var onclickCBGeofence = 0;"
			+ "var onclickCBGeolocalized = 0;"
			+ "var formGeofence;"
			+ "var formGeolocalized;"
			+ "var tempLocation;"
			+ "var map;"
			+ "var markers = [];"
			+ "var allPolygones = [];"
			+ "var infowindow;"
			+ "var latitude;"
			+ "var longitude;"
			+ "var content;"
			+ "var showHide = 0;"
			+ "var showEdit = 0;"
			+ "var showValidate = 0;"
			+ "var editDraggable = 0;"
			+ "var editPutId;"
			+ "var refreshScript;"
			+ "function showDrawingManagerGeofence() {"
				+ "if (onclickCBGeofence == 0){"
					+ "console.log('onclickCBGeofence');"
					+ "onclickCBGeofence = 1;"
				+ "}else if (onclickCBGeofence == 1){"
					+ "console.log('onclickCBGeofence');"
					+ "onclickCBGeofence = 0;"
				+ "}"	
				+ "showDrawingManager();"
	        + "}"
	        + "function showDrawingManagerGeolocalized() {"
		        + "if (onclickCBGeolocalized == 0){"
					+ "console.log('onclickCBGeolocalized');"
					+ "onclickCBGeolocalized = 1;"
				+ "}else if (onclickCBGeolocalized == 1){"
					+ "console.log('onclickCBGeolocalized');"
					+ "onclickCBGeolocalized = 0;"
				+ "}"
				+ "showDrawingManager();"
			+ "}"
			+ "function showDrawingManager() {"
				+ "if (onclickCBGeofence == 0 && onclickCBGeolocalized == 0){"
					+ "drawingManager.setOptions({drawingControl: false});"
				+ "}else {"
					+ "drawingManager.setOptions({drawingControl: true});"
				+ "}"		
			+ "}"
			+ "function changeFieldGeofence(coordinates) {"
				+ "content = coordinates;"	
				+ "console.log(coordinates);"
			+ "}"
			+ "function changeFieldGeolocalizedLat(tempLocation) {"
				+ "formGeolocalized = tempLocation;"	
				+ "console.log(tempLocation);"
			    + "document.getElementById('inputLat3').value=formGeolocalized;"
		    + "}"
		    + "function changeFieldGeolocalizedLng(tempLocation) {"
				+ "formGeolocalized = tempLocation;"	
				+ "console.log(tempLocation);"
			    + "document.getElementById('inputLng3').value=formGeolocalized;"
		    + "}"
		    + "function postDataForm() {"
			    + "var title = document.getElementById('inputTitle3').value;"
			    + "var author = document.getElementById('inputAuthor3').value;"
			    + "var summary = document.getElementById('inputSummary3').value;"
			    + "if(content == null){"
			    	+ "content=1.0;"
			    + "}"
			    + "if(showValidate == 0){"
					+ "location.href ='/feedsync/rest/webapps/"+id+"/addEntry?title='+title+'&author='+author+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude;"
				+ "}else if(showValidate == 1){"
					+ "showValidate = 0;"
					+ "location.href ='/feedsync/rest/webapps/"+id+"/editEntry?title='+title+'&author='+author+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude+'&id='+editPutId;"
			    + "}"
			+ "}"			    
			+ "function resetmDB() {"
				+ "location.href ='/feedsync/rest/webapps/2/reset';"
			+ "}"
		    + "function editDataForm(editId,editTitle,editAuthor,editSummary,editContent,editLatitude,editLongitude) {"
		    	+ "showEdit = 1;"
		    	+ "editPutId = editId;"
			    + "console.log(editId,editTitle,editAuthor,editSummary);"
			    + "if (infowindow) infowindow.close();"
			    + "content = editContent;"
			    + "latitude = editLatitude;"
			    + "longitude = editLongitude;"
			    + "if(showValidate == 0){"
			    	+ "document.getElementById('addItem').innerHTML = 'Edit Item';"
			    	+ "document.getElementById('inputTitle3').value = editTitle;"
				    + "document.getElementById('inputAuthor3').value = editAuthor;"
				    + "document.getElementById('inputSummary3').value= editSummary;"
			    	+ "showValidate = 1;"
		    	+ "}else if(showValidate == 1){"
		    		+ "showValidate = 0;"
			    	+ "var titleEdit = document.getElementById('inputTitle3').value;"
			    	+ "var authorEdit = document.getElementById('inputAuthor3').value;"
			    	+ "var summary = document.getElementById('inputSummary3').value;"
			    	+ "console.log(titleEdit);"
			    	+ "document.getElementById('addItem').innerHTML = 'Add Item';"
			    	+ "location.href ='/feedsync/rest/webapps/"+id+ "/editEntry?title='+titleEdit+'&author='+authorEdit+'&content='+content+'&summary='+summary+'&latitude='+latitude+'&longitude='+longitude+'&id='+editId;"
			    + "}"
		    + "}"
		    + "function addPoylygon(polyCoordinates) {"
		      + "myCoordinates = polyCoordinates.split('///');"
		      + "var allLatLng = [];"
		      + "for(var i=myCoordinates.length;i>1;i-=2){"
	      		+ "var myLatlng = new google.maps.LatLng(myCoordinates[i-2],myCoordinates[i-1]);"
	      		+ "allLatLng.push(myLatlng);"
			  + "}"
			  + "allPolygones.push(createPolygones(allLatLng));"
			+ "}"
			+ "function addMarker(latLocation,lngLocation,name) {"
				+ "var myLatlng = new google.maps.LatLng(latLocation,lngLocation);"
				+ "var contentString = name;"
				+ "var myName = name;"
				+ "console.log(myName);"
				+ "markers.push(createMarker(myName,myLatlng));"
			+ "}"
			+ "function showAll() {"
				+ "if(showHide == 0){"
					+ "showHide = 1;"
					+ "document.getElementById('showHide').innerHTML = 'Hide markers';"
					+ "showPoly();"
					+ "showMarkers();"
				+ "}else if(showHide == 1){"
					+ "showHide = 0;"
					+ "document.getElementById('showHide').innerHTML = 'Show markers';"
					+ "hideAll();"
				+ "}"
			+ "}"
		    + "function hideAll() {"
		  		+ "setAllPolyMap(null);"
		  		+ "setAllMarkersMap(null);"
			+ "}"	
			+ "function showPoly() {"
			  	+ "setAllPolyMap(map);"
			+ "}"
			+ "function showMarkers() {"
	  		  	+ "setAllMarkersMap(map);"
		    + "}"
			+ "function setAllMarkersMap(map) {"
			  	+ "for (var i = 0; i < markers.length; i++) {"
			      	+ "markers[i].setMap(map);"
		      	+ "}"
			+ "}"
			+ "function setAllPolyMap(map) {"
			  	+ "for (var i = 0; i < allPolygones.length; i++) {"
			      	+ "allPolygones[i].setMap(map);"
		      	+ "}"
			+ "}"
			+ "function createPolygones(latlng) {"
				+ "console.log(latlng);"
				+ "var myPolygon = new google.maps.Polygon({map: map,paths: latlng});"
				+ "google.maps.event.addListener(myPolygon, 'click', function(event) {"
					+ "if(showEdit == 1){"
						+ "console.log(editDraggable);"
						+ "if(editDraggable == 0){"
							+ "this.setDraggable(true);"
							+ "this.setEditable(true);"
							+ "editDraggable = 1;"
						+ "}"
						+ "else if(editDraggable == 1){"
							+ "this.setDraggable(false);"
							+ "this.setEditable(false);"
							+ "editDraggable = 0;"
					    	+ "var polygonBounds = this.getPath();"
							+ "coordinates = [];"
							+ "for(var i = 0 ; i < polygonBounds.length ; i++){"
					    		+ "coordinates.push(polygonBounds.getAt(i).lat(),polygonBounds.getAt(i).lng());"
				    		+ "}"
				    		+ "changeFieldGeofence(coordinates.join('///'));"
						+ "}"
					+ "}"
	    		+ "});"
		    	+ "return myPolygon;"
		    + "}"  
			+ "function createMarker(name, latlng) {"
				+ "var image;"
				+ "console.log(name.indexOf('Fire'));"
		    	+ "if(name.indexOf('Fire') != -1){"
		    		+ "image = '../../img/fire.png';"
		    	+ "}else if(name.indexOf('Bomb') != -1 ){"
		    		+ "image = '../../img/bomb.png';"
	    		+ "}else if(name.indexOf('Flood') != -1 ){"
		    		+ "image = '../../img/flood.png';"
	    		+ "}else if(name.indexOf('Radiation') != -1 ){"
		    		+ "image = '../../img/radiation.png';"
	    		+ "}else if(name.indexOf('Treedown') != -1 ){"
		    		+ "image = '../../img/treedown.png';"
		    	+ "}else if(name.indexOf('Group') != -1 ){"
	    			+ "image = '../../img/group.png';"
    			+ "}else if(name.indexOf('Offline') != -1 ){"
	    			+ "image = '../../img/male-red.png';"
    			+ "}else if(name.indexOf('Available') != -1 ){"
	    			+ "image = '../../img/male-2.png';"
    			+ "}else if(name.indexOf('Missing') != -1 ){"
	    			+ "image = '../../img/male-yellow.png';"
    			+ "}else if(name.indexOf('tetra') != -1 ){"
	    			+ "image = '../../img/flaticon.png';"
    			+ "}else{"
	    			+ "image = '../../img/male-2.png';"
	    		+ "}"
			    + "var myMarker = new google.maps.Marker({position: latlng, map: map,icon : image, draggable:true});"
			    + "google.maps.event.addListener(myMarker, 'click', function() {"
			      + "if (infowindow) infowindow.close();"
			      + "infowindow = new google.maps.InfoWindow({content: name});"
			      + "infowindow.open(map, myMarker);"
			    + "});"
			    + "google.maps.event.addListener(myMarker, 'dragend', function() {"
					+ "if(showEdit == 1){"
						+ "var markerBounds = myMarker.getPosition();"
						+ "latitude = markerBounds.lat();"
						+ "longitude = markerBounds.lng();"
					+ "}"
				+ "});"
			    + "return myMarker;"
		    + "}"  
			+ "function initialize(){"
			    + "map = new google.maps.Map(document.getElementById('map'), {zoom: 14, center: new google.maps.LatLng(48.941361,2.30689)});"
			    + "drawingManager = new google.maps.drawing.DrawingManager("
			    + "{drawingMode: null,drawingControlOptions: {drawingModes: ["
	    		+ "google.maps.drawing.OverlayType.MARKER,google.maps.drawing.OverlayType.POLYGON]}});"
	    		+ "drawingManager.setMap(map);"
	    		+ "drawingManager.setOptions({drawingControl: true});"
			    + "polygons = [];"
				+ "google.maps.event.addDomListener(drawingManager, 'polygoncomplete', function(polygon) {"
			    	+ "polygons.push(polygon);"
			    	+ "console.log(polygon.getPath().getArray());"
			    	+ "var polygonBounds = polygon.getPath();"
					+ "coordinates = [];"
					+ "for(var i = 0 ; i < polygonBounds.length ; i++){"
			    		+ "coordinates.push(polygonBounds.getAt(i).lat(),polygonBounds.getAt(i).lng());"
		    		+ "}"
		    		+ "changeFieldGeofence(coordinates.join('///'));"
				+ "});"
				+ "google.maps.event.addDomListener(drawingManager, 'markercomplete', function(marker) {"
			    	+ "console.log(marker.getPosition());"
			    	+ "var markerBounds = marker.getPosition();"
		    		+ "latitude = markerBounds.lat();"
		    		+ "longitude = markerBounds.lng();"
	    		+ "});"
	    		+ "google.maps.event.addListenerOnce(map, 'tilesloaded', showAll);"	
			+ "}"
				+ "google.maps.event.addDomListener(window, 'load', initialize);"
				+ "function refreshTable() {"
					+ "setInterval(function () {"
						+ "var pathtopage = window.location.href;"
						+ "var myDiv = document.getElementById('rowTab');"
						+ "var arr = myDiv.getElementsByTagName('script');"
						+ "refreshScript = arr;"
						+ "$('#rowTab').load(pathtopage + ' #rowTab');"
					+ "}, 2000);"
				+ "}"
				+ "function refreshMap() {"
				+ "setInterval(function () {"
					+ "console.log('refreshMap');"
					+ "console.log(refreshScript);"
				    + "eval(refreshScript[0].innerHTML);" 
				+ "}, 2000);"
			+ "}"
		+ "</script>");
		
		buffer.append(
		"<div class='container'>"
			+ "<div class='row'>"
				+ "<div class='col-md-2' style='height:40px;'>"
					+ "<button type='button' class='btn btn-info' id='showHide' onclick='showAll()' style='margin-bottom:20px;'>Show markers</button>"
				+ "</div>"
			+ "</div>"
			+ "<div class='row'>"
			    + "<div class='col-md-8'>"
					+ "<div style='width:900px; height:680px;' id='map'></div>"
				+ "</div>"
				+ "<div class='col-md-4'>"
					+ "<div class='row-fluid' style='margin-top: 50px;'>"
						+ "<div class='col-md-12' style='margin-left:30%;'>"
							+ "<form role='post' class='form-horizontal'>");
							if(type==1){
								buffer.append("<div class='form-group'>"
									+ "<label for='inputTitle3' class='col-sm-4 control-label'>Title</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' class='form-control' id='inputTitle3' placeholder='Title'>"
									+ "</div>"
								+ "</div>");
							}
							if(type==2){
								buffer.append(
								  "<div class='form-group'>"
									+ "<label for='inputTitle3' class='col-sm-4 control-label'>Title</label>"
										+ "<div class='col-md-8'>"
											+ "<select class='form-control' id='inputTitle3'>"
												  + "<option>Intervention Group 410</option>"
												  + "<option>Intervention Group 401</option>"
												  + "<option>Intervention Group 402</option>"
												  + "<option>Intervention Group 403</option>"
												  + "<option>Fire</option>"
												  + "<option>Bomb Alert</option>"
												  + "<option>Flood</option>"
												  + "<option>Radiation</option>"
												  + "<option>Treedown</option>"
											+ "</select>"
										+ "</div>"
								+ "</div>");
							}
							if(type==2 || type==1){
									buffer.append(
								  "<div class='form-group'>"
									+ "<label for='inputAuthor3' class='col-sm-4 control-label'>Author</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' value='PMR' class='form-control' id='inputAuthor3' placeholder='Author'>"
									+ "</div>"
								+ "</div>"
								+ "<div class='form-group'>"
									+ "<label for='inputSummary3' class='col-sm-4 control-label'>Summary</label>"
									+ "<div class='col-md-8'>"
										+ "<input type='text' value='Alert incident' class='form-control' id='inputSummary3' placeholder='Summary'>"
									+ "</div>"
								+ "</div>"
								);}
							    buffer.append("<div class='form-group'>");
									//+ "<a href='/feedsync/rest/admin/"+id+ "/add_item'><input type='submit' id='button' class='btn btn-success' value='Add item'></input></a>"
									//+ "<a role='button' class='btn btn-success' href='/feedsync/rest/admin/"+id+ "/add_item?title='>Add Item</a>"
							    if(type == 2 || type==1){
							    	buffer.append("<button type='button' class='btn btn-success' id ='addItem' style='margin-left:200px' onclick='postDataForm()'>Add Item</button>");
							    	//buffer.append("<button type='button' class='btn btn-success' onclick='resetmDB'>Reset DB</button>");
							    }
							    buffer.append("</div>"
							+ "</form>"
						+ "</div>"
					+ "</div>"
				+ "</div>"
		+ "</div>");
		buffer.append(""
				+ "<style type='text/css'>"
				+ "td { text-align:center;}"
				+ "th { text-align:center;}"
				+ "</style>"
				+ "<div class='row' id='rowTab' style='margin-top:20px;'>"
					+ "<table class='table table-striped table-hover table-condensed' id='tabDB'>"
					+ "<tr>"
						+ "<th>ID</th><th>Title</th><th>Author</th><th>Created</th>");
		buffer.append("<th colspan='2'>Action</th><tr>");
		DBEntry item = new DBEntry();
		while (!cursor.eof()) {
			item.read(cursor);
			if(id.equals(item.getFeed_id()) && item.getComment_to_entry()==null){
				buffer.append("<tr>");
				printValue(buffer, item.getId());
				printValue(buffer, "<a href='/feedsync/rest/webapps/"+item.getFeed_id()+"/"+item.getId()+ "'>"+item.getTitle()+ "</a>");
				printValue(buffer, item.getAuthor().getName());
				printValue(buffer, item.getUpdated());
				if(id.equals(idBFT)){
					String mColor ="";
					mColor = returnTimeDiffStatus(item.getUpdated().getTime());
					
					String mTmp = "<h3>"+item.getTitle()+ "</h3>"
										+ "<div><p>"+mColor+ "</p></div>"
												+ "<div>"
//												+ "<button type=\"button\" class=\"btn btn-primary\" id=\"showEdit\" onclick=\"editDataForm("+item.getId()+ ",\\'"+item.getTitle()+ "\\',\\'"+item.getAuthor().getName()+ "\\',\\'"+item.getSummary()+ "\\',\\'"+item.getContent()+ "\\',\\'"+item.getLatitude()+ "\\',\\'"+item.getLongitude()+ "\\')\">Edit</button>"
												//+ "<a role=\"button\" class=\"btn btn-primary\" href=\"/feedsync/rest/admin/"+id+ "/edit_item?id="+item.id+ "\">Edit</a>"
												+ "<a role=\"button\"  style=\"margin-left:10px;\" class=\"btn btn-danger\" href=\"/feedsync/rest/webapps/"+id+ "/removeEntry?id="+item.getId()+ "\" onclick=\"return confirm(&#x27 Are you sure ?&#x27);\">Remove</a><div>"
												
										+ "</div>";
					
					buffer.append("<script type='text/javascript'>"
							+ "addMarker("+item.getLatitude()+ ","+item.getLongitude()+ ",'"+mTmp+ "');"
							+ "addPoylygon('"+item.getTitle()+ "');"
						+ "</script>");
				}else{
//					System.out.println("item.getContent() "+item.getContent());
					String mTmp = "<h3>"+item.getTitle()+ "</h3>"
//									+ "<div><p>"+item.getContent()+ "</p></div>"
											+ "<div>"
											//+ "<button type=\"button\" class=\"btn btn-primary\" id=\"showEdit\" onclick=\"editDataForm("+item.id+ ",\\'"+item.title+ "\\',\\'"+item.author.getName()+ "\\',\\'"+item.summary+ "\\',\\'"+item.content+ "\\',\\'"+item.latitude+ "\\',\\'"+item.longitude+ "\\')\">Edit</button>"
											//+ "<a role=\"button\" class=\"btn btn-primary\" href=\"/feedsync/rest/admin/"+id+ "/edit_item?id="+item.id+ "\">Edit</a>"
											+ "<a role=\"button\"  style=\"margin-left:10px;\" class=\"btn btn-danger\" href=\"/feedsync/rest/webapps/"+id+ "/removeEntry?id="+item.getId()+ "\" onclick=\"return confirm(&#x27 Are you sure ?&#x27);\">Remove</a><div>"
											
									+ "</div>";
					buffer.append("<script type='text/javascript'>"
							+ "addMarker("+item.getLatitude()+ ","+item.getLongitude()+ ",'"+mTmp+ "');");
							System.out.println("item.getContent() "+item.getContent());
							if(item.getContent().contains("///")){
								buffer.append("addPoylygon('"+item.getContent()+ "');");
							}
							
							buffer.append("</script>");
				}
				
				buffer.append("<td><a role='button' class='btn btn-primary' href='/feedsync/rest/webapps/"+id+ "/editEntry?id=");
				buffer.append(item.getId());
				buffer.append("'>Edit</a></td>");
				buffer.append("<td><a role='button' class='btn btn-danger' href='/feedsync/rest/webapps/"+id+ "/removeEntry?id=");
				buffer.append(item.getId());
				buffer.append("' onclick='return confirm(");
				buffer.append("&#x27 Are you sure ?&#x27");
				buffer.append(");");
				buffer.append("'>Remove</a></td>");
				buffer.append("</tr>");
			}else if(idBFT.equals(item.getFeed_id()) && item.getComment_to_entry()==null){
				String mColor ="";
				mColor = returnTimeDiffStatus(item.getUpdated().getTime());
				
				String mTmp = "<h3>"+item.getTitle()+ "</h3>"
									+ "<div><p>"+mColor+ "</p></div>"
											+ "<div>"
//											+ "<button type=\"button\" class=\"btn btn-primary\" id=\"showEdit\" onclick=\"editDataForm("+item.getId()+ ",\\'"+item.getTitle()+ "\\',\\'"+item.getAuthor().getName()+ "\\',\\'"+item.getSummary()+ "\\',\\'"+item.getContent()+ "\\',\\'"+item.getLatitude()+ "\\',\\'"+item.getLongitude()+ "\\')\">Edit</button>"
											//+ "<a role=\"button\" class=\"btn btn-primary\" href=\"/feedsync/rest/admin/"+id+ "/edit_item?id="+item.id+ "\">Edit</a>"
											+ "<a role=\"button\"  style=\"margin-left:10px;\" class=\"btn btn-danger\" href=\"/feedsync/rest/webapps/"+id+ "/removeEntry?id="+item.getId()+ "\" onclick=\"return confirm(&#x27 Are you sure ?&#x27);\">Remove</a><div>"
											
									+ "</div>";
				
				buffer.append("<script type='text/javascript'>"
						+ "addMarker("+item.getLatitude()+ ","+item.getLongitude()+ ",'"+mTmp+ "');");
//						if(item.getContent()==null){
//						}else{
//							buffer.append("addPoylygon('"+item.getContent()+ "');");
//						}
//						
						buffer.append("</script>");
			}
			cursor.next();
		}
		buffer.append("</table></div>");
		printFooterEntries(buffer);
	}
	
	/**
	 * This function shows a form in a HTML page, this form is to add a feed
	 * 
	 * @param buffer web page
	 */
	public void showAddFormFeed(StringBuffer buffer) {
		buffer.append("<h2>Add Feed</h2><p>");
		buffer.append("<form><table>");
		buffer.append("<tr><td>Title:</td><td><input type='text' name='title'/></td></tr>");
		buffer.append("</table><input type='submit' value='Add'></form>");
		printReturnToAdminPage(buffer);
	}
	
	/**
	 * This function processes of the add of the feed with the params received in arguments
	 * 
	 * @param buffer web page for the form
	 * @param params parameters for the execution of the form
	 * @throws SqlJetException
	 */
	@SuppressWarnings("deprecation")
	public void processAddFormFeed(StringBuffer buffer, Map<String, String> params) throws SqlJetException {
		try {
			DBFeed item = new DBFeed();
			item.title = params.get("title");
			if (item.title == null) {
				throw new IllegalArgumentException("title is not specified.");
			}

			Date mDate = new Date();
			item.created = mDate.toGMTString();
			Long id;
			DB db = new DB();
			try {
				id = db.addFeed(item);
				db.updateFeedURL(id);
			} finally {
				db.close();
			}
			
			buffer.append("Item was added.");
		} catch (IllegalArgumentException e) {
			buffer.append("Invalid input! " + e.getMessage());
		}
		printReturnToAdminPage(buffer);
	}
	
	/**
	 * <p>
	 * This function lets the user to edit the feed <br>
	 * Depends on the number of the params in the call,
	 * if there is only one params it calls the function showEditFormFeed to show the form to edit the feed
	 * else it calls the function processEditFormFeed to process of the edition, it is when the user submit the edition
	 * </p>
	 * 
	 * @param buffer
	 * @param params
	 * @throws SqlJetException
	 */
	public void editFeed(StringBuffer buffer, Map<String, String> params) throws SqlJetException {
		DBFeed item = findFeed(buffer, params);
		if (item != null) {
			if (params.size() == 1) {
				showEditFormFeed(buffer, item);
			} else {
				processEditFormFeed(buffer, item, params);
			}
			printReturnToAdminPage(buffer);
		}
	}
	
	/**
	 * <p>
	 * This function shows a form in a HTML page, this form is to edit a feed </br>
	 * The param "item" is the feed to edit
	 * </p>
	 * 
	 * @param buffer web page
	 * @param item feed to edit
	 */
	public void showEditFormFeed(StringBuffer buffer,DBFeed item){
		buffer.append("<h2>Edit Feed</h2><p>");
		buffer.append("<form><table>");
		buffer.append("<tr><td>Title:</td><td><input type='text' name='title'");
		if (item.title != null) {
			buffer.append(" value='");
			buffer.append(item.title);
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		buffer.append("</table><input type='submit' value='Update'/>");
		buffer.append("<input type='hidden' name='id' value='");
		buffer.append(item.id);
		buffer.append("'/></form>");
	}
	
	/**
	 * This function processes of the edition of the feed with the params received in arguments
	 * 
	 * @param buffer web page
	 * @param item feed to edit
	 * @param params parameters for the edition, the new data
	 * @throws SqlJetException
	 */
	@SuppressWarnings("deprecation")
	public void processEditFormFeed(StringBuffer buffer,DBFeed item, Map<String, String> params) throws SqlJetException {
		try {
			DBFeed itemFeed = item;
			itemFeed.title = params.get("title");
			if (item.title == null) {
				throw new IllegalArgumentException("title is not specified.");
			}
			
			itemFeed.author = "1";

			Date mDate = new Date();
			item.created = mDate.toGMTString();
			DB db = new DB();
			try {
				db.updateFeeditem(item);
			} finally {
				db.close();
			}
			
			buffer.append("Item was updated.");
		} catch (IllegalArgumentException e) {
			buffer.append("Invalid input! " + e.getMessage());
		}
		//printReturnToInventory(buffer);
	}
	
	/**
	 * This function remove a feed, it calls functions to remove feed in database
	 * 
	 * @param buffer web page
	 * @param params 
	 * @throws SqlJetException
	 */
	public void removeFeed(StringBuffer buffer, Map<String, String> params) throws SqlJetException {
		DBFeed item = findFeed(buffer, params);
		if (item != null) {
			DB db = new DB();
				db.removeFeed(item.id);
				try {
					db.beginTransaction(false);
					try {
						ISqlJetCursor cursorEntry;
						cursorEntry = db.getAllItemsEntry();
						try {
							while (!cursorEntry.eof()) {
								DBEntry itemEntry = new DBEntry();
								itemEntry.read(cursorEntry);
								if(itemEntry.getFeed_id().equals(String.valueOf(item.id))){
									db.removeEntry(itemEntry.getId());
								}
								cursorEntry.next();
							}
						} finally {
							cursorEntry.close();
						}
				} finally {
					db.commit();
				}
			} finally {
				db.close();
			}
			buffer.append("Item was removed.");
			printReturnToAdminPage(buffer);
		}
	}
	
	/**
	 * This function remove a feed, it calls functions to remove feed in database
	 * 
	 * @param buffer web page
	 * @param params
	 * @throws SqlJetException
	 */
	public void removeFeedUsedAPI(StringBuffer buffer, Map<String, String> params) throws SqlJetException {
		DBFeed item = findFeed(buffer, params);
		if (item != null) {
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			abderaClient.delete(httpAddress+"/feedsync/rest/myfeeds/"+item.id+"/deleteFeed");
			buffer.append("Item was removed.");
			printReturnToAdminPage(buffer);
		}
	}
	
	/**
	 * This function remove an entry, it calls the API to delete the entry
	 * 
	 * @param buffer web page
	 * @param id id of the entry to delete
	 * @param params
	 * @throws SqlJetException
	 */
	public void removeEntry(StringBuffer buffer, String id, Map<String, String> params) throws SqlJetException {
		DBEntry item = findEntry(buffer, id, params);
		if (item != null) {
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			abderaClient.delete(httpAddress+"/feedsync/rest/myfeeds/"+item.getFeed_id()+"/"+item.getId()+"/deleteEntry");
			buffer.append("Item was removed.");
			printReturnToEntries(buffer, id);
		}
	}
	
	/**
	 * This function shows a form in a HTML page, this form is to add an entry in a specific feed with the "id"
	 * 
	 * @param buffer web page
	 * @param id id of te feed where the entry will be added
	 */
	public void showAddFormEntry(StringBuffer buffer, String id) {
		buffer.append("<h2>Add Entry</h2><p>");
		buffer.append("<form><table>");
		buffer.append("<tr><td>Title:</td><td><input type='text' name='title'/></td></tr>");
		buffer.append("<tr><td>Author:</td><td><input type='text' name='author'/></td></tr>");
		buffer.append("<tr><td>Content/Geofence for Alert:</td><td><input type='text' name='content'/></td></tr>");
		buffer.append("<tr><td>Summary:</td><td><input type='text' name='summary'/></td></tr>");
		buffer.append("<tr><td>Latitude:</td><td><input type='text' name='latitude'/></td></tr>");
		buffer.append("<tr><td>Longitude:</td><td><input type='text' name='longitude'/></td></tr>");
		buffer.append("</table><input type='submit' value='Add'></form>");
		printReturnToEntries(buffer,id);
	}

	/**
	 * This function processes of the add of the entry with the params received in arguments in a specific feed with the "id"
	 * 
	 * @param buffer web page
	 * @param id id id of te feed where the entry will be added
	 * @param params parameters of the entry
	 * @throws SqlJetException
	 */
	public void processAddFormEntry(StringBuffer buffer, String id, Map<String, String> params) throws SqlJetException {
		try {
			DBEntry item = new DBEntry();
			item.setTitle(params.get("title"));
			if (item.getTitle() == null) {
				throw new IllegalArgumentException("title is not specified.");
			}
			item.setId(Long.parseLong(id));
			item.setContent(params.get("content"));
			item.setCreated(new Date());
			item.setSummary(params.get("summary"));
			item.setDescription(params.get("summary"));
			
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			Entry entry = AbderaSupport.getAbdera().getFactory().newEntry();
	        entry.setTitle(item.getTitle());
	        entry.setContent(item.getContent());
	        entry.setSummary(item.getSummary());
	        entry.setUpdated(new Date());
	        entry.addAuthor(params.get("author"));
	        if(params.get("latitude")!=null && params.get("longitude")!=null){
    			GeoHelper.addPosition(entry,new Point(Double.parseDouble(params.get("latitude")),Double.parseDouble(params.get("longitude"))));
	        }

	        abderaClient.post(httpAddress+"/feedsync/rest/myfeeds/"+id+"/addEntry", entry);
	        
	        if(item.getTitle().contains("Group")){
	        	
	        	Entry mEntry = AbderaSupport.getAbdera().getFactory().newEntry();
	        	mEntry.setTitle(item.getTitle());
	        	mEntry.setContent(item.getContent());
	        	mEntry.setSummary(item.getSummary());
	        	mEntry.setUpdated(new Date());
	        	mEntry.addAuthor("1,2,3");
	        	if(params.get("latitude")!=null && params.get("longitude")!=null){
	    			GeoHelper.addPosition(mEntry,new Point(Double.parseDouble(params.get("latitude")),Double.parseDouble(params.get("longitude"))));
		        }
	        	abderaClient.post(httpAddress+"/feedsync/rest/myfeeds/3/addEntry", mEntry);	        	
	        }
//	        
//	        /feedsync/rest/admin/3/add_item?title='+title+'&author='+'Farid,Florian,Jeremie'+'&content='+'Admin'+'&summary='+'Admin'+'&latitude='+latitude+'&longitude='+longitude;"

			buffer.append("Item was added.");
		} catch (IllegalArgumentException e) {
			buffer.append("Invalid input! " + e.getMessage());
		}
		printReturnToEntries(buffer,id);
	}
	
	/**
	 * <p>
	 * This function lets the user to edit an entry </br>
	 * Depends on the number of the params in the call,
	 * if there is only one params it calls the function showEditFormEntry to show the form to edit the entry
	 * else it calls the function processEditFormEntry to process of the edition, it is when the user submit the edition
	 * </p>
	 * 
	 * @param buffer web page
	 * @param feed_id id of the feed where the entry is
	 * @param params parameters of the edited entry
	 * @throws SqlJetException
	 */
	public void editEntry(StringBuffer buffer,String feed_id, Map<String, String> params) throws SqlJetException {
		DBEntry item = findEntry(buffer, feed_id, params);
		if (item != null) {
			if (params.size() == 1) {
				showEditFormEntry(buffer, item);
			} else {
				processEditFormEntry(buffer, item,feed_id, params);
			}
			printReturnEditToEntrie(buffer,feed_id);
		}
	}
	
	/**
	 * <p>
	 * This function shows a form in a HTML page, this form is to edit an entry </br>
	 * The param "item" is the entry to edit
	 * </p>
	 * 
	 * @param buffer web page
	 * @param item entry
	 */
	public void showEditFormEntry(StringBuffer buffer,DBEntry item){
		buffer.append("<h2>Edit Entry</h2><p>");
		buffer.append("<form><table>");
		buffer.append("<tr><td>Title:</td><td><input type='text' name='title'");
		if (item.getTitle() != null) {
			buffer.append(" value='");
			buffer.append(item.getTitle());
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		
		buffer.append("<tr><td>Author:</td><td><input type='text' name='author'");
		if (item.getAuthor().getName() != null) {
			buffer.append(" value='");
			buffer.append(item.getAuthor().getName());
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		
		buffer.append("<tr><td>Content:</td><td><input type='text' name='content'");
		if (item.getContent() != null) {
			buffer.append(" value='");
			buffer.append(item.getContent());
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		
		buffer.append("<tr><td>Summary:</td><td><input type='text' name='summary'");
		if (item.getSummary() != null) {
			buffer.append(" value='");
			buffer.append(item.getSummary());
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		
		buffer.append("<tr><td>Latitude:</td><td><input type='text' name='latitude'");
		if (item.getLatitude() != 0.0) {
			buffer.append(" value='");
			buffer.append(item.getLatitude());
			buffer.append("'");
		}
		buffer.append("/></td></tr>");
		
		buffer.append("<tr><td>Longitude:</td><td><input type='text' name='longitude'");
		if (item.getLongitude() != 0.0) {
			buffer.append(" value='");
			buffer.append(item.getLongitude());
			buffer.append("'");
		}
		
		buffer.append("/></td></tr>");
		buffer.append("</table><input type='submit' value='Update'/>");
		buffer.append("<input type='hidden' name='id' value='");
		buffer.append(item.getId());
		buffer.append("'/></form>");
	}
	
	/**
	 * This function processes of the edition of the feed with the params received in arguments
	 * 
	 * @param buffer web page
	 * @param item entry
	 * @param id id of the feed where the entry is
	 * @param params parameters of the edited entry
	 * @throws SqlJetException
	 */
	public void processEditFormEntry(StringBuffer buffer,DBEntry item, String id, Map<String, String> params) throws SqlJetException {
		try {
			DBEntry itemEntry = item;
			itemEntry.setTitle(params.get("title"));
			if (item.getTitle() == null) {
				throw new IllegalArgumentException("title is not specified.");
			}
		
			itemEntry.setId(item.getId());
			itemEntry.setFeed_id(id);
			itemEntry.setContent(params.get("content"));
			itemEntry.setCreated(item.getCreated());
			itemEntry.setSummary(params.get("summary"));
//			itemEntry.setDescription(params.get("description"));
			itemEntry.setUpdated(new Date());
			
			Entry entry = AbderaSupport.getAbdera().getFactory().newEntry();
			entry.addAuthor(params.get("author"));
			itemEntry.setAuthor(entry.getAuthor());
			
			itemEntry.setLatitude(Double.parseDouble(params.get("latitude")));
			itemEntry.setLongitude(Double.parseDouble(params.get("longitude")));
			
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			entry.setId(String.valueOf(itemEntry.getId()));
	        entry.setTitle(itemEntry.getTitle());
	        entry.setContent(itemEntry.getContent());
	        entry.setSummary(itemEntry.getSummary());
	        entry.setUpdated(new Date());
	        entry.addAuthor(params.get("author"));
	        if(params.get("latitude")!=null && params.get("longitude")!=null){
    			GeoHelper.addPosition(entry,new Point(itemEntry.getLatitude(),Double.parseDouble(params.get("longitude"))));
	        }
	       
	        abderaClient.put(httpAddress+"/feedsync/rest/myfeeds/"+id+"/editEntry", entry);

			DB db = new DB();
			try {
				db.updateEntry(itemEntry);
			} finally {
				db.close();
			}
			
			buffer.append("Item was updated.");
		} catch (IllegalArgumentException e) {
			buffer.append("Invalid input! " + e.getMessage());
		}
	}
	
	/**
	 * This function shows a form in a HTML page, this form is to add a comment in a specific feed with the "id"
	 * 
	 * @param buffer web page
	 * @param id id of the feed where the comment is
	 * @param idEntry id of the comment
	 */
	public void showAddFormComment(StringBuffer buffer, String id, String idEntry) {
		buffer.append("<h2>Add Comment</h2><p>");
		buffer.append("<form><table>");
		buffer.append("<tr><td>Author:</td><td><input type='text' name='author'/></td></tr>");
		buffer.append("<tr><td>Content:</td><td><input type='text' name='content'/></td></tr>");
		buffer.append("</table><input type='submit' value='Add'></form>");
		printReturnToComment(buffer,id,idEntry);
	}
	
	/**
	 * <p>
	 * This function lets the user to edit a comment </br>
	 * Depends on the number of the params in the call,
	 * if there is only one params it calls the function showEditFormEntry to show the form to edit the comment
	 * else it calls the function processEditFormEntry to process of the edition, it is when the user submit the edition
	 * </p>
	 * 
	 * @param buffer web page
	 * @param feed_id id of the feed where the comment is
	 * @param id id of the comment to edit
	 * @param params parameters of the edited comment
	 * @throws SqlJetException
	 */
	public void editComment(StringBuffer buffer,String feed_id,String id, Map<String, String> params) throws SqlJetException {
		DBEntry item = findEntry(buffer, feed_id, params);
		if (item != null) {
			if (params.size() == 1) {
				showEditFormEntry(buffer, item);
			} else {
				processEditFormEntry(buffer, item,feed_id, params);
			}
			printReturnEditToEntrie(buffer,feed_id);
		}
	}
	
	/**
	 * This function processes of the add of the comment with the params received in arguments in a specific feed with the "id"
	 * 
	 * @param buffer web page
	 * @param id id of the feed where the comment is
	 * @param idEntry id of the comment
	 * @param params parameters of the new comment
	 * @throws SqlJetException
	 */
	public void processAddFormComment(StringBuffer buffer, String feedId, String entryId, Map<String, String> params) throws SqlJetException {
		try {
			DBEntry item = new DBEntry();
			item.setTitle("title");
			item.setFeed_id(feedId);
			item.setContent(params.get("content"));
			item.setCreated(new Date());
			
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			Entry entry = AbderaSupport.getAbdera().getFactory().newEntry();
	        entry.setTitle(item.getTitle());
	        entry.setContent(item.getContent());
	        entry.setUpdated(new Date());
	        entry.addAuthor(params.get("author"));
	        Category cat = AbderaSupport.getAbdera().getFactory().newCategory();
		    cat.setText(new Date().toString());
	        entry.addCategory(cat);

	        abderaClient.post(httpAddress+"/feedsync/rest/myfeeds/"+feedId+"/"+entryId, entry);
	        
			buffer.append("Item was added.");
		} catch (IllegalArgumentException e) {
			buffer.append("Invalid input! " + e.getMessage());
		}
		printReturnToComment(buffer,feedId, entryId);
	}
	
	/**
	 * This function removes a comment in the database
	 * 
	 * @param buffer web page
	 * @param id id of the feed where the comment id
	 * @param idEntry id of the comment
	 * @param params
	 * @throws SqlJetException
	 */
	public void removeComment(StringBuffer buffer, String id, String idEntry, Map<String, String> params) throws SqlJetException {
		DBEntry item = findEntry(buffer, id, params);
		if (item != null) {
			AbderaClient abderaClient = new AbderaClient(AbderaSupport.getAbdera());
			abderaClient.delete(httpAddress+"/feedsync/rest/myfeeds/"+item.getFeed_id()+"/comments-"+item.getComment_to_entry()+"/"+item.getId());
			buffer.append("Item was removed.");
			printReturnToComment(buffer, item.getFeed_id(), idEntry);
		}
	}
	
	/**
	 * <p>
	 * This function gets a specific entry from the database, in arguments there are a map of params containing the id of the entry </br>
	 * With this id, it finds the entry in the database and returns it
	 * </p>
	 * 
	 * @param buffer web page
	 * @param feed_id id of the feed where the entry is
	 * @param params 
	 * @return item entry found in the database
	 * @throws SqlJetException
	 */
	private DBEntry findEntry(StringBuffer buffer,String feed_id, Map<String, String> params) throws SqlJetException {
		if (!params.containsKey("id")) {
			buffer.append("id is not specified.");
			printReturnToEntries(buffer,feed_id);
			return null;
		}
		long id;
		try {
			id = Long.parseLong(params.get("id"));
		} catch (Exception e) {
			buffer.append("Invalid id: '");
			buffer.append(params.get("id"));
			buffer.append("'");
			printReturnToEntries(buffer,feed_id);
			return null;
		}
		DBEntry item;
		DB db = new DB();
		try {
			item = db.getDBEntry(id);
		} finally {
			db.close();
		}
		if (item == null) {
			buffer.append("Item with id '");
			buffer.append(id);
			buffer.append("' is not found.");
			printReturnToEntries(buffer,feed_id);
			return null;
		}
		return item;
	}
	
	/**
	 * <p>
	 * This function gets a specific feed from the database, in arguments there are a map of params containing the id of the feed </br>
	 * With this id, it finds the feed in the database and returns it
	 * </p>
	 * 
	 * @param buffer web page 
	 * @param params
	 * @return DBFeed feed found in the database
	 * @throws SqlJetException
	 */
	private DBFeed findFeed(StringBuffer buffer, Map<String, String> params) throws SqlJetException {
		if (!params.containsKey("id")) {
			buffer.append("id is not specified.");
			printReturnToAdminPage(buffer);
			return null;
		}
		long id;
		try {
			id = Long.parseLong(params.get("id"));
		} catch (Exception e) {
			buffer.append("Invalid id: '");
			buffer.append(params.get("id"));
			buffer.append("'");
			printReturnToAdminPage(buffer);
			return null;
		}
		DBFeed item;
		DB db = new DB();
		try {
			item = db.getDBFeed(id);
		} finally {
			db.close();
		}
		if (item == null) {
			buffer.append("Item with id '");
			buffer.append(id);
			buffer.append("' is not found.");
			printReturnToAdminPage(buffer);
			return null;
		}
		return item;
	}
	
	private void printValue(StringBuffer buffer, Object value) {
		buffer.append("<td");
		if (value instanceof Number) {
			buffer.append(" style='text-align: right;'");
		}
		buffer.append(">");
		if (value != null) {
			buffer.append(value);
		} else {
			buffer.append("&nbsp;");
		}
		buffer.append("</td>");
	}
	
	public void printFooterEntries(StringBuffer buffer) {
		buffer.append(
		"<!-- FOOTER -->"
	  + "<footer style='margin-top:30px;'>"
	  	+ "<p class='pull-right'><a href='#'>Back to top</a></p>"
	  	+ "<p>&copy; Thales Communications & Security 2013-2014 &middot; <a href='#'>Privacy</a> &middot; <a href='#'>Terms</a></p>"
	  + "</footer>"
	  + "</div><!-- /.container -->"
	  + "<!-- Bootstrap core JavaScript "
	  + "================================================== -->"
	  + "<!-- Placed at the end of the document so the pages load faster -->"
	  + "<script src='https://code.jquery.com/jquery-1.10.2.min.js'></script>"
	  + "<script src='../../js/bootstrap.min.js'></script>");
	}
	
	public void printFooterComments(StringBuffer buffer) {
		buffer.append(
		"<!-- FOOTER -->"
	  + "<footer style='margin-top:30px;'>"
	  	+ "<p class='pull-right'><a href='#'>Back to top</a></p>"
	  	+ "<p>&copy; Thales Communications & Security 2013-2014 &middot; <a href='#'>Privacy</a> &middot; <a href='#'>Terms</a></p>"
	  + "</footer>"
	  + "</div><!-- /.container -->"
	  + "<!-- Bootstrap core JavaScript "
	  + "================================================== -->"
	  + "<!-- Placed at the end of the document so the pages load faster -->"
	  + "<script src='https://code.jquery.com/jquery-1.10.2.min.js'></script>"
	  + "<script src='../../../js/bootstrap.min.js'></script>");
	}
	
	private void printReturnToAdminPage(StringBuffer buffer) {
		buffer.append("<hr><a href='/feedsync/rest/webapps'>Return to Feeds List</a>");
	}
	
	private void printReturnToEntries(StringBuffer buffer, String id) {
		buffer.append(
				"<hr><a href='/feedsync/rest/webapps/"+id+"' id='backButton'>Back</a>"
				+ "<script type='text/javascript'>"
					+"document.getElementById('backButton').click();"
				+"</script>");
	}
	
	private void printReturnEditToEntrie(StringBuffer buffer, String id) {
		buffer.append("<hr><a href='/feedsync/rest/webapps/"+id+"' id='backButton'>Back</a>");
	}
	
	private void printReturnToComment(StringBuffer buffer, String id, String idEntry) {
		buffer.append("<hr><a href='/feedsync/rest/webapps/"+id+"/"+idEntry+"'>Return to Comments</a>");
	}
	
	private final String returnTimeDiffStatus(double time){
		Date actualTime= new Date();
		double diff=actualTime.getTime()-time;
		int diff1=(int) (diff/1000);
		if (diff1<MINUTE*5){
			return("Available");
		}
		else if(diff1<HOUR){
			return("Missing");
		}
		else if (diff1<DAY){
			return("Offline");
		}
		return("Missing");
	}

}
