package com.thales.feedsyncserver.gui;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;

import org.tmatesoft.sqljet.core.SqlJetException;

import com.thales.feedsyncserver.db.DB;
import com.thales.feedsyncserver.db.DBFeed;

/**
 * <p>
 * This class provides a REST API for the webapps</br></br>
 * It contains some HTML and Javascript functions to display the webapps
 * </p>
 * @author Florian COSNIER
 *
 */
@Path("/webapps")
public class WebAppAPI {
	
	@Context
	private UriInfo uriInfo;
	
	private GuiResponder mGuiResponder = new GuiResponder();
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String ShowAll() throws SqlJetException{
		StringBuffer buffer = new StringBuffer();
		buffer.append("<html><head><title>Admin Management</title></head><body>");
		mGuiResponder.showFeeds(buffer);
		buffer.append("</body></html>");

		return buffer.toString();
  }
	
	/**
	 * <p>
	 * This function shows the entries of a feed</br>
	 * This feed is the feed with the id (feedId)</br>
	 * The user gets the address then the API calls the GuiManager to show the data in a web page</br>
	 * </p>
	 * @param feedId the feed id
	 * @return a web page
	 * @throws SqlJetException
	 */
	 @GET
	 @Produces(MediaType.TEXT_HTML)
	 @Path("/{feedId}")
	public String ShowAllEntries(@PathParam("feedId")String feedId) throws SqlJetException{
		StringBuffer buffer = new StringBuffer();
		DB mDb = new DB();
		
		try {
			DBFeed item = mDb.getDBFeed(Long.parseLong(feedId));
//			if(item.title.contains("Comment")){
//				String split[] = item.title.split("-");
//				mEntryResponder.showInventoryComment(buffer,id, split[1]);
//			}else{
//				mEntryResponder.showInventory(buffer,id);
//			}
			mGuiResponder.showEntries(buffer, feedId);
		} finally {
			mDb.close();
		}
		return buffer.toString();
	}
	
	/**
	 * <p>
	 * This function creates a feed</br>
	 * The user gets the address then the API calls the GuiManager to create a feed from a form
	 * </p>
	 * 
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/addFeed")
	@Produces(MediaType.TEXT_HTML)
	public String addFeed() throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		if (uri.getQuery() == null) {
			mGuiResponder.showAddFormFeed(buffer);
		}else{
			mGuiResponder.processAddFormFeed(buffer, parseQuery(uri));
		}
		buffer.append("</body></html>");
		return buffer.toString();
	}
	
	/**
	 * <p>
	 * This function creates an entry<br>
	 * The user gets the address then the API calls the GuiManager to create an entry from a form</br>
	 * The param "feedId" is the id of the feed where the entry will be added
	 * </p>
	 * @param feedId the feed id
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/{feedId}/addEntry")
	@Produces(MediaType.TEXT_HTML)
	public String addEntry(@PathParam("feedId")String feedId) throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		
		if (uri.getQuery() == null) {
			DBFeed item;
			DB db = new DB();
			try {
				item = db.getDBFeed(Long.parseLong(feedId));
				if(item.title.contains("Comment")){
					String split[] = item.title.split("-");
					mGuiResponder.showAddFormComment(buffer,feedId,split[1]);
				}else{
					mGuiResponder.showAddFormEntry(buffer,feedId);
				}
			} finally {
				db.close();
			} 		
		}else{
			DBFeed item;
			DB db = new DB();
			try {
				item = db.getDBFeed(Long.parseLong(feedId));
				if(item.title.contains("Comment")){
					String split[] = item.title.split("-");
					mGuiResponder.processAddFormComment(buffer, feedId, split[1], parseQuery(uri));
				}else{
					mGuiResponder.processAddFormEntry(buffer, feedId, parseQuery(uri));
				}
			} finally {
				db.close();
			} 	

		}
		buffer.append("</body></html>");
		return buffer.toString();
	}
	
	/**
	 * <p>
	 * This function edits a feed</br>
	 * The user gets the address then the API calls the GuiManager to edit a feed via a form
	 * </p>
	 * 
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/editFeed")
	@Produces(MediaType.TEXT_HTML)
	public String editFeed() throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		mGuiResponder.editFeed(buffer, parseQuery(uri));
		buffer.append("</body></html>");
		return buffer.toString();
	}
	/**
	 * <p>
	 * This function edits an entry</br>
	 * The user gets the address then the API calls the GuiManager to edit an entry via a form</br>
	 * The param "id" is the id of the feed where the entry will be edited
	 * </p>
	 * 
	 * @param feedId the feed id
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/{feedId}/editEntry")
	@Produces(MediaType.TEXT_HTML)
	public String editEntry(@PathParam("feedId")String feedId) throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		DBFeed item;
		DB db = new DB();
		try {
			item = db.getDBFeed(Long.parseLong(feedId));
			if(item.title.contains("Comment")){
				String split[] = item.title.split("-");
				mGuiResponder.editComment(buffer,feedId,split[1], parseQuery(uri));
			}else{
				mGuiResponder.editEntry(buffer, feedId, parseQuery(uri));
			}
		} finally {
			db.close();
		}
		buffer.append("</body></html>");
		return buffer.toString();
	}
	
	/**
	 * <p>
	 * This function removes a feed</br>
	 * The user gets the address then the API calls the GuiManager to remove a feed in the database</br>
	 * </p>
	 * 
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/removeFeed")
	@Produces(MediaType.TEXT_HTML)
	public String removeFeed() throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		mGuiResponder.removeFeed(buffer, parseQuery(uri));
		buffer.append("</body></html>");
		return buffer.toString();
	}
	
	/**
	 * <p>
	 * This function removes an entry</br>
	 * The user gets the address then the API calls the GuiManager to remove an entry in the database</br>
	 * The param "id" is the id of the feed where the entry will be removed
	 * </p>
	 * 
	 * @param feedId the feed id
	 * @return a web page
	 * @throws SqlJetException
	 */
	@GET
	@Path("/{feedId}/removeEntry")
	@Produces(MediaType.TEXT_HTML)
	public String removeItemEntry(@PathParam("feedId")String feedId) throws SqlJetException{
		URI uri = uriInfo.getRequestUri();
		StringBuffer buffer = new StringBuffer();
		mGuiResponder.removeEntry(buffer,feedId, parseQuery(uri));
		buffer.append("</body></html>");
		return buffer.toString();
	}
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	@Path("/{feedId}/{entryId}")
	public String ShowAllComments(@PathParam("feedId")String feedId,@PathParam("entryId")String entryId) throws SqlJetException{
		StringBuffer buffer = new StringBuffer();
		DB mDb = new DB();
		
		try {
			mGuiResponder.showComments(buffer, entryId);
		} finally {
			mDb.close();
		}
		return buffer.toString();
	}
	
	@GET
  	@Path("/{feedId}/{entryId}/add_item")
  	@Produces(MediaType.TEXT_HTML)
  	public String addItemComment(@PathParam("feedId")String feedId, @PathParam("entryId")String entryId) throws SqlJetException{
	  URI uri = uriInfo.getRequestUri();
	  StringBuffer buffer = new StringBuffer();
	  
	  if (uri.getQuery() == null) {
		  mGuiResponder.showAddFormComment(buffer,feedId,entryId);
	  }else{
		  mGuiResponder.processAddFormComment(buffer, feedId, entryId, parseQuery(uri));
	  	}
	  return buffer.toString();
  	}
	
//	@GET
//	@Path("/{feedId}/{entryId}/remove_item")
//	@Produces(MediaType.TEXT_HTML)
//	public String removeItemEntryComment(@PathParam("feedId")String feedId, @PathParam("entryId")String entryId) throws SqlJetException{
//		URI uri = uriInfo.getRequestUri();
//		StringBuffer buffer = new StringBuffer();
//		mGuiResponder.removeItemComment(buffer,feedId, entryId, parseQuery(uri));
//		return buffer.toString();
//	}
	
	private static Map<String, String> parseQuery(URI uri) {
		String uriQuery = uri.getRawQuery();
		Map<String, String> result = new HashMap<String, String>();
		if (uriQuery != null) {
			for (String param : uriQuery.split("&")) {
				String[] parts = param.split("=");
				if (parts.length > 1) {
					try {
						result.put(parts[0], URLDecoder.decode(parts[1], "UTF-8"));
					} catch (UnsupportedEncodingException e) {
						throw new IllegalArgumentException(e);
					}
				}
			}
		}
		return result;
	}
}
